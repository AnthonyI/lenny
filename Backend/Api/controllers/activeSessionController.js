'use strict';
const FEED_LIMIT = 10;
var mongoose = require('mongoose');
var ActiveSessions = mongoose.model('ActiveSessions');


exports.get_all_users = function (req, res) {
    ActiveSessions.find({}, function (err, user) {
        if (err)
            res.send(err);
        res.json(user);
        res.end();
        console.log('get_all_users');
    });
};


exports.open_session = function (req, res) {

    console.log('add_session ' + req.body.userId);

    ActiveSessions.findOne({ userId: req.body.userId }, function (err, session) {
        if (session == null) {

            var newSession = new ActiveSessions(req.body.userId);
            newSession.start = Date.now();
            newSession.Active = 1;
            newSession.NextIndex = FEED_LIMIT;
            newSession.PrevIndex = 0;
            newSession.FeedBuffer = DataFeed(0); ///load feed here

            newSession.save(function (err, session) {
                if (!err) {
                    res.json(session);
                    res.end();
                }
            
                else {
                    res.status(400);
                    res.send(err);
                    res.end();
                }
            });
        }
        else 
        {
            res.status(400);
            res.json({ message: 'session with userId=' + req.body.userId + ' already open' });
            res.end();
        }
    })
};

var update_feed = function (_userId){
    ActiveSessions.findOne({ userId: _userId }, function (err, session){
        console.log('update_session for ' + _userId);
        var newFeed = new ActiveSessions(_userId);
        newFeed.start = session.start;
        newFeed.Active = 1;
        newFeed.FeedBuffer = DataFeed(session.Index); ///load feed here
        newFeed.PrFeedBuffer = session.FeedBuffer;
        newFeed.NextIndex = session.NextIndex + FEED_LIMIT;
        newFeed.PrevIndex = session.PrevIndex + FEED_LIMIT;

        // condtions herenewFeed.PrevIndex = (newFeed.PrevIndex <0) ? 0 : newFeed.PrevIndex;
        
        ActiveSessions.findOneAndUpdate({ userId: req.body.userId }, newFeed, { new: true }, function (err, session) {
            if (err) {
                console.log('Erreur updating feed for' + _userId);
                return false;
            }
            else return true;
        })
    })
}

var update_Pfeed = function (_userId){
    ActiveSessions.findOne({ userId: _userId }, function (err, session){
        console.log('update_session for ' + _userId);
        var newFeed = new ActiveSessions(_userId);
        newFeed.start = session.start;
        newFeed.Active = 1;
        newFeed.FeedBuffer = DataFeed(session.Index); ///load feed here
        newFeed.PrFeedBuffer = session.FeedBuffer;
        newFeed.NextIndex = session.NextIndex - FEED_LIMIT;
        newFeed.PrevIndex = session.PrevIndex - FEED_LIMIT;

        // condtions herenewFeed.PrevIndex = (newFeed.PrevIndex <0) ? 0 : newFeed.PrevIndex;
        
        ActiveSessions.findOneAndUpdate({ userId: req.body.userId }, newFeed, { new: true }, function (err, session) {
            if (err) {
                console.log('Erreur updating feed for' + _userId);
                return false;
            }
            else return true;
        })
    })
}
exports.Load_Feed = function (req, res) {

    console.log('Load_Feed for ' + req.params.userId);

    ActiveSessions.findOne({ userId: req.params.userId }, function (err, session) {
        if (err)
            res.send(err);
        res.on('finish',update_feed(req.params.userId));
        res.json(session.FeedBuffer);
        res.end();
    });
};

exports.Load_PreviousFeed = function (req, res) {

    console.log('Load_PreviousFeed for ' + req.params.userId);

    ActiveSessions.findOne({ userId: req.params.userId }, function (err, session) {
        if (err)
            res.send(err);
        res.on('finish',update_Pfeed(req.params.userId));
        res.json(session.PrFeedBuffer);
        res.end();
    });
};
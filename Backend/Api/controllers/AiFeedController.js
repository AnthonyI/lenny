'use strict';

var mongoose = require('mongoose');
var AiFeed = mongoose.model('AiFeed');
var User = mongoose.model('Users');

exports.get_feed = function (req, res) {

    console.log('get_feed ' + req.params._id);

    AiFeed.findOne({ userId: req.params.userId }, { __v: 0 }, function (err, feed) {
        if (err) {
            res.status(400);
            res.send(err);
            res.end();
        }
        else {
            res.json(feed);
            res.end();
        }
    });
};

exports.add_feed = function (req, res) {

    console.log('add_feed');

    User.findOne({ userId: req.body.userId }, function (err, feed) {
        if (feed != null) {
            var newFeed = new AiFeed(req.body);
            newFeed.save(function (err, feed) {
                if (err) {
                    res.status(400);
                    res.send(err);
                }
                else {
                    res.json(feed);
                }
                res.end();
            });
        }
        else {
            console.log('User with the given userId does not exist');
            res.status(400);
            res.json({ message: 'User with the given userId does not exist' });
            res.end();
        }
    });
};

exports.update_feed = function (req, res) {

    console.log('update_feed ' + req.body.userId);

    if (req.body.userId) {
        AiFeed.findOneAndUpdate({ userId: req.body.userId }, req.body, { new: true }, function (err, feed) {
            if (err) {
                res.status(400);
                res.send(err);
            }
            else if (feed == null) {
                res.status(400);
                res.json({ message: 'feed with userId=' + req.body.userId + ' does not exist' });
            }
            else res.json(feed);
            res.end();
        });
    }
    else {
        res.status(400);
        res.json({ message: 'userId field is required for update_feed' });
        res.end();
    };
};

exports.delete_feed = function (req, res) {

    console.log('delete_feed ' + req.params._id);
    var fileName = '';

    if (req.params._id) {

        AiFeed.deleteOne({ _id: req.params._id }, function (err, feed) {
            if (!err) {
                res.json({ message: 'Feed successfully deleted' });
                res.end();
            }
            else {
                res.status(400);
                res.send(err);
                res.end();
            }
        });
    }
    else {
        res.status(400);
        res.end();
    };
};


/* exports.add_AiFeed = function (data) {
    console.log('add_Into_AiFeed');
    console.log(data);
    var newAiFeed = new AiFeed(data);
    newAiFeed.save(function (err) {
                if (err) return false;
                else return true;
    })
};

exports.update_userAiFeed = function (data) {
    console.log('updating_user');
    console.log(data);
    if (data.userId) {
        AiFeed.findOneAndUpdate({ userId: data.userId }, data, { new: true }, function (err) {
            if (err) return false;
            else return true;
        });
    }
    else 
    {
        console.log('userId does not exist');
        return false;
    }
};

exports.delete_userAiFeed = function (Id) {
    console.log('delete_userId ' + Id);
    if (Id) {
        AiFeed.deleteOne({ userId: Id }, function (err) {
            if (err)
                return false;
        });
    }
    else return true;   
}; */

exports.Load_Feed = function (req, res) {
    AiFeed.
        find().
        where('userId').equals(req.params.userId).
        populate('mainFeed Items').
        select('mainFeed -_id').
        exec(function (err, feeds) {
            if (err) {
                res.status(400);
                res.send(err);
                res.end();
            }
            else {
                res.json(feeds[0].mainFeed);
                //res.json(feeds);
                res.end();
                console.log('Load_Feed');
            }
        });
};


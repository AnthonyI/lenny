'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('Trading');

exports.get_all_users = function (req, res) {
    User.find({}, function (err, user) {
        if (err)
            res.send(err);
        res.json(user);
        res.end();
        console.log('get_all_users');
    });
};


exports.add_user = function (req, res) {

    console.log('add_user');
    console.log(req.body);

    User.findOne({ userId: req.body.userId }, function (err, user) {
        if (user == null) {
            var newUser = new User(req.body);
            newUser.createdDate = Date.now();
            newUser.save(function (err, user) {
                if (err) {
                    res.status(400);
                    res.send(err);
                }
                else res.json(user);
                res.end();
            });
        }
        else {
            console.log('User with the given userId already exists');
            res.status(400);
            res.json({ message: 'User with the given userId already exists' });
            res.end();
        }
    })
};



exports.get_user = function (req, res) {
    console.log('get_user');
    console.log(req.params);
    User.findOne({ userId: req.params.userId }, { _id: 0, __v: 0 }, function (err, user) {
        if (err)
            res.send(err);
        res.json(user);
        res.end();        
    });
};


'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('Trading');

exports.get_all_users = function (req, res) {
    User.find({}, function (err, user) {
        if (err)
            res.send(err);
        res.json(user);
        res.end();
        console.log('get_all_users');
    });
};


exports.add_user = function (req, res) {

    console.log('add_user');
    console.log(req.body);

    User.findOne({ userId: req.body.userId }, function (err, user) {
        if (user == null) {
            var newUser = new User(req.body);
            newUser.createdDate = Date.now();
            newUser.save(function (err, user) {
                if (err) {
                    res.status(400);
                    res.send(err);
                }
                else res.json(user);
                res.end();
            });
        }
        else {
            console.log('User with the given userId already exists');
            res.status(400);
            res.json({ message: 'User with the given userId already exists' });
            res.end();
        }
    })
};


exports.get_user = function (req, res) {
    console.log('get_user');
    console.log(req.params);
    User.findOne({ userId: req.params.userId }, { _id: 0, __v: 0 }, function (err, user) {
        if (err)
            res.send(err);
        res.json(user);
        res.end();        
    });
};


// exports.update_user = function (req, res) {
//     console.log('update_user');
//     console.log(req.body);
//     if (req.body.userId) {
//         User.findOneAndUpdate({ userId: req.body.userId }, req.body, { new: true }, function (err, user) {
//             if (err) {
//                 res.status(400);
//                 res.send(err);
//             }
//             else if (user == null) {
//                 res.status(400);
//                 res.json({ message: 'User with userId=' + req.body.userId + ' does not exist' });
//             }
//             else res.json(user);
//             res.end();
//         });
//     }
//     else {
//         res.status(400);
//         res.json({ message: 'UserId is required for update user' });
//         res.end();
//     }
// };


// exports.delete_user = function (req, res) {
//     console.log('delete_user ' + req.params.userId);
//     if (req.params.userId) {
//         User.deleteOne({ userId: req.params.userId }, function (err, user) {
//             if (err)
//                 res.send(err);
//             res.json({ message: 'User successfully deleted' });
//             res.end();
//         });
//     }
//     else {
//         res.status(400);
//         res.end();
//     }    
// };
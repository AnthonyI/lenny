'use strict';

var mongoose = require('mongoose');
var User = mongoose.model('Users');

var fs = require('fs');
var pathPhoto = "./photos/usersPhotos/";

exports.get_all_users = function (req, res) {

    console.log('get_all_users');

    User.find({}, function (err, users) {
        if (!err) {
            res.status(200);
            res.json(users);
            res.end();
        }
        else {
            res.status(400);
            res.send(err);
            res.end();
            console.log('get_all_users error ' + err);
        }
    });
};

exports.add_user = function (req, res) {

    console.log('add_user ' + req.body.userId);

    User.findOne({ userId: req.body.userId }, function (err, user) {
        if (err) {
            res.status(400);
            res.send(err);
            res.end();
            console.log('add_user error findOne ' + err);
        }
        else if (user == null) {

            var newUser = new User(req.body);
            newUser.createdDate = Date.now();

            newUser.save(function (err, user) {
                if (!err) {
                    res.status(200);
                    res.json(user);
                    res.end();
                }
                else {
                    res.status(400);
                    res.send(err);
                    res.end();
                    console.log('add_user error save ' + err);
                }
            });
        }
        else {
            res.status(400);
            res.json({ message: 'User with the given userId already exists' });
            res.end();
            console.log('User with the given userId already exists');
        }
    })
};



exports.get_user = function (req, res) {

    console.log('get_user ' + req.params.userId);

    User.findOne({ userId: req.params.userId }, { _id: 0, __v: 0, photo: 0 }, function (err, user) {
        if (!err) {
            res.status(200);
            res.json(user);
            res.end();
        }
        else {
            res.status(400);
            res.send(err);
            res.end();
            console.log('get_user error findOne ' + err);
        }
    });
};


exports.update_user = function (req, res) {

    console.log('update_user ' + req.body.userId);

    if (req.body.userId) {
        User.findOneAndUpdate({ userId: req.body.userId }, req.body, { new: true }, function (err, user) {
            if (err) {
                res.status(400);
                res.send(err);
                res.end();
                console.log('update_user error findOneAndUpdate ' + err);
            }
            else if (user == null) {
                res.status(400);
                res.json({ message: 'User with userId=' + req.body.userId + ' does not exist' });
                res.end();
                console.log('User with userId=' + req.body.userId + ' does not exist');
            }
            else {
                res.status(200);
                res.json(user);
                res.end();
            }
        });
    }
    else {
        res.status(400);
        res.json({ message: 'UserId is required for update user' });
        res.end();
        console.log('UserId is required for update user');
    }
};


exports.delete_user = function (req, res) {

    console.log('delete_user ' + req.params.userId);

    if (req.params.userId) {
        User.deleteOne({ userId: req.params.userId }, function (err, user) {
            if (!err) {
                var fileName = req.params.userId;

                fs.exists(pathPhoto + fileName, (exists) => {
                    if (exists) {
                        fs.unlink(pathPhoto + fileName, function (err) {
                            if (!err) {
                                res.status(200);
                                res.json({ message: 'User successfully deleted' });
                                res.end();
                            }
                            else {
                                res.status(400);
                                res.send(err);
                                res.end();
                                console.log('delete_user error unlink ' + err);
                            }
                        });
                    }
                    else {
                        res.status(200);
                        res.json({ message: 'User successfully deleted' });
                        res.end();
                    }
                });
            }
            else {
                res.status(400);
                res.send(err);
                res.end();
                console.log('delete_user error deleteOne ' + err);
            }
        });
    }
    else {
        res.status(400);
        res.json({ message: 'UserId is required for delete user' });
        res.end();
        console.log('UserId is required for delete user');
    }
};

exports.update_photo = function (req, res) {

    console.log('update_photo ' + req.body.userId);

    var fileName = req.body.userId;
    var photo = req.body.photo;

    if (req.body.userId) {
        User.findOne({ userId: req.body.userId }, { _id: 0, __v: 0 }, function (err, user) {
            if (err) {
                res.status(400);
                res.send(err);
                res.end();
                console.log('update_photo error findOne ' + err);
            }
            else if (user == null) {
                res.status(400);
                res.json({ message: 'User with userId=' + req.body.userId + ' does not exist' });
                res.end();
                console.log('User with userId=' + req.body.userId + ' does not exist');
            }
            else {
                fs.writeFile(pathPhoto + fileName, photo, function (err) {
                    if (!err) {
                        res.status(200);
                        res.json({ message: 'Profile picture updated' });
                        res.end();
                    }
                    else {
                        res.status(400);
                        res.send(err);
                        res.end();
                        console.log('update_photo error writeFile ' + err);
                    }
                });
            }
        });
    }
    else {
        res.status(400);
        res.json({ message: 'UserId is required for update user' });
        res.end();
        console.log('UserId is required for update user');
    }
};

exports.get_photo = function (req, res) {

    console.log('get_photo ' + req.params.userId);

    var fileName = req.params.userId;

    fs.readFile(pathPhoto + fileName, function (err, data) {
        if (!err) {
            res.status(200);
            res.send(data);
            res.end();
        }
        else {
            res.status(400);
            res.json({ message: 'No such file = ' + fileName + ' or directory!' });
            res.end();
            console.log('No such file = ' + fileName + ' or directory!');
        }
    });
};
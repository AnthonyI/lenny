'use strict';


var mongoose = require('mongoose');
var Network = mongoose.model('RecommendationNetworks');
var User = mongoose.model('Users');

exports.get_all_networks = function (req, res) {
    Network.find({}, function (err, networks) {
        if (err)
            res.send(err);
        res.json(networks);
        res.end();
        console.log('get_all_networks');
    });
};

exports.add_network = function (req, res) {

    console.log('add_network ' + req.body.userId);

    if (req.body.userId) {
        if (req.body.network) {
            User.findOne({ userId: req.body.userId }, function (err, user) {
                if (user != null) {

                    Network.findOne({ userId: req.body.userId }, function (err, network) {
                        if (network == null) {

                            var newNetwork = new Network(req.body);
                            newNetwork.createdDate = Date.now();

                            newNetwork.save(function (err, network) {
                                if (!err) {
                                    res.status(200);
                                    res.json(network);
                                    res.end();
                                }
                                else {
                                    res.status(400);
                                    res.send(err);
                                    res.end();
                                    console.log('add_network error save ' + err);
                                }
                            });
                        }
                        else {
                            res.status(400);
                            res.json({ message: 'Network with the given userId already exists' });
                            res.end();
                            console.log('Network with the given userId already exists');
                        }
                    })
                }
                else {
                    res.status(400);
                    res.json({ message: 'User with the given userId does not exist' });
                    res.end();
                    console.log('User with the given userId does not exist');
                }
            });
        }
        else {
            res.status(400);
            res.json({ message: 'Network is required for add network' });
            res.end();
            console.log('Network is required for add network');
        }
    }
    else {
        res.status(400);
        res.json({ message: 'UserId is required for add network' });
        res.end();
        console.log('UserId is required for add network');
    }
};

exports.get_network = function (req, res) {

    console.log('get_network ' + req.params.userId);

    Network.findOne({ userId: req.params.userId }, { __v: 0 }, function (err, network) {
        if (err) {
            res.send(err);
            res.end();
        }
        else {
            res.json({ "network" : network, "userId" : req.params.userId });
            res.end();
        }
    });
};

exports.update_network = function (req, res) {

    console.log('update_network ' + req.body.userId);

    if (req.body.userId) {
        Network.findOneAndUpdate({ userId: req.body.userId }, req.body, { new: true }, function (err, network) {
            if (err) {
                res.status(400);
                res.send(err);
                res.end();
                console.log('update_network error findOneAndUpdate ' + err);
            }
            else if (network == null) {
                res.status(400);
                res.json({ message: 'Network with userId=' + req.body.userId + ' does not exist' });
                res.end();
                console.log('Network with userId=' + req.body.userId + ' does not exist');
            }
            else {
                res.json(network);
                res.end();
            }
        });
    }
    else {
        res.status(400);
        res.json({ message: 'userId is required for update_network' });
        res.end();
        console.log('userId is required for update_network');
    };
};

exports.delete_network = function (req, res) {

    console.log('delete_network ' + req.params.userId);

    if (req.params.userId) {
        Network.deleteOne({ userId: req.params.userId }, function (err, network) {
            if (!err) {
                res.json({ message: 'Network successfully deleted' });
                res.end();
            }
            else {
                res.status(400);
                res.send(err);
                res.end();
            }
        });
    }
    else {
        res.status(400);
        res.end();
    };
};
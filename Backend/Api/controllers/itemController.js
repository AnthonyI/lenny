'use strict';


var mongoose = require('mongoose');
var Item = mongoose.model('Items');
var User = mongoose.model('Users');

var fs = require('fs');
var pathPhoto = "./photos/itemsPhotos/";

exports.get_all_items = function (req, res) {

    console.log('get_all_items');

    Item.find({}, function (err, items) {
        if (!err) {
            res.status(200);
            res.json(items);
            res.end();
        }
        else {
            res.status(400);
            res.send(err);
            res.end();
            console.log('get_all_items error ' + err);
        }
    });
};

exports.add_item = function (req, res) {

    console.log('add_item');
    console.log(req.body);

    User.findOne({ userId: req.body.userId }, function (err, user) {
        if (err) {
            res.status(400);
            res.send(err);
            res.end();
            console.log('add_item error findOne ' + err);
        }
        else if (user != null) {
            var newItem = new Item(req.body);
            newItem.createdDate = Date.now();
            newItem.save(function (err, item) {
                if (!err) {
                    res.status(200);
                    res.json(item);
                    res.end();
                }
                else {
                    res.status(400);
                    res.send(err);
                    res.end();
                    console.log('add_item error save ' + err);
                }
            });
        }
        else {
            res.status(400);
            res.json({ message: 'User with the given userId does not exist' });
            res.end();
            console.log('User with the given userId does not exist');
        }
    });
};

exports.get_item = function (req, res) {

    console.log('get_item ' + req.params._id);

    Item.findOne({ _id: req.params._id }, { __v: 0 }, function (err, item) {
        if (!err) {
            res.status(200);
            res.json(item);
            res.end();
        }
        else {
            res.status(400);
            res.send(err);
            res.end();
            console.log('get_item error ' + err);
        }
    });
};

exports.update_item = function (req, res) {

    console.log('update_item ' + req.body._id);

    if (req.body._id) {
        Item.findOneAndUpdate({ _id: req.body._id }, req.body, { new: true }, function (err, item) {
            if (err) {
                res.status(400);
                res.send(err);
                res.end();
                console.log('update_item error findOneAndUpdate' + err);
            }
            else if (item == null) {
                res.status(400);
                res.json({ message: 'Item with _id=' + req.body._id + ' does not exist' });
                res.end();
                console.log('Item with _id=' + req.body._id + ' does not exist');
            }
            else {
                res.status(200);
                res.json(item);
                res.end();
            }
        });
    }
    else {
        res.status(400);
        res.json({ message: '_id is required for update item' });
        res.end();
        console.log('_id is required for update item');
    };
};

exports.delete_item = function (req, res) {

    console.log('delete_item ' + req.params._id);
    var fileName = '';
    var count = 0;

    if (req.params._id) {

        Item.findOne({ _id: req.params._id }, { __v: 0 }, function (err, item) {
            if (!err) {

                if (item != null) {
                    count = item.photos.length;
                    for (var i = 0; i < count; i++) {
                        fileName = item.photos[i];
                        fs.unlink(pathPhoto + fileName, function (err) {
                            if (err)
                                console.log('delete_item error unlink ' + err);
                        });
                    };
                };
            };
        });

        Item.deleteOne({ _id: req.params._id }, function (err) {
            if (!err) {
                res.status(200);
                res.json({ message: 'Item successfully deleted' });
                res.end();
            }
            else {
                res.status(400);
                res.send(err);
                res.end();
                console.log('delete_item error unlink ' + err);
            }
        });
    }
    else {
        res.status(400);
        res.end();
    };
};

exports.get_my_items = function (req, res) {

    console.log('get_my_items ' + req.params.userId);

    Item.find({ userId: req.params.userId }, { __v: 0 }, function (err, items) {
        if (!err) {
            res.status(200);
            res.json(items);
            res.end();
        }
        else {
            res.status(400);
            res.send(err);
            res.end();
            console.log('get_my_items error ' + err);
        }
    });
};

exports.add_photo = function (req, res) {

    console.log('add_photo ' + req.body._id);

    var fileName = req.body._id;
    var photo = req.body.photo;
    var lastFileName = new Date();

    Item.findOne({ _id: req.body._id }, { __v: 0 }, function (err, item) {
        if (err) {
            res.status(400);
            res.send(err);
            res.end();
            console.log('add_photo error findOne ' + err);
        }
        else {

            if (item != null) {
                if (item.photos.length < 5) {

                    // Perform a global replacement ":" to "-" 
                    fileName += "_" + lastFileName.toISOString().replace(/:/g, "-") + ".tw";

                    fs.writeFile(pathPhoto + fileName, photo, function (err) {
                        if (!err) {

                            item.photos.push(fileName);

                            Item.findOneAndUpdate({ _id: req.body._id }, item, { new: true }, function (err, item) {
                                if (!err) {
                                    res.status(200);
                                    res.json({ fileName: fileName });
                                    res.end();
                                }
                                else {
                                    res.status(400);
                                    res.send(err);
                                    res.end();
                                    console.log('add_photo error findOneAndUpdate ' + err);
                                };
                            });
                        }
                        else {
                            res.status(400);
                            res.send(err);
                            res.end();
                            console.log('add_photo error writeFile ' + err);
                        };
                    });
                }
                else {
                    res.status(400);
                    res.json({ message: 'The maximum number of photos is 5' });
                    res.end();
                    console.log('The maximum number of photos is 5');
                }
            }
            else {
                res.status(400);
                res.json({ message: 'Item with the given _id does not exist' });
                res.end();
                console.log('Item with the given _id does not exist');
            }
        }
    });
};

exports.get_photo = function (req, res) {

    console.log('get_photo ' + req.params.fileName);

    var fileName = req.params.fileName;
    fs.readFile(pathPhoto + fileName, function (err, data) {
        if (!err) {
            res.status(200);
            res.send(data);
            res.end();
        }
        else {
            res.status(400);
            res.json({ message: 'No such file = ' + fileName + ' or directory!' });
            res.end();
            console.log('No such file = ' + fileName + ' or directory!')
        }
    });
};

exports.delete_photo = function (req, res) {

    console.log('delete_photo ' + req.params.fileName);

    var fileName = req.params.fileName;
    var length = 0;
    var fileExist = false;
    var _id = fileName.substring(0, 24);

    Item.findOne({ _id: _id }, { __v: 0 }, function (err, item) {
        if (!err) {
            if (item != null) {
                length = item.photos.length;

                for (var i = 0; i < length; i++) {
                    if (item.photos[i] == fileName) {
                        // to remove item i from array
                        item.photos.splice(i, 1);
                        fileExist = true;
                    }
                };
            };

            if (fileExist) {
                Item.findOneAndUpdate({ _id: _id }, item, { new: true }, function (err, item) {
                    if (!err) {
                        fs.unlink(pathPhoto + fileName, function (err) {
                            if (!err) {
                                res.status(200);
                                res.json({ message: 'Photo ' + fileName + ' deleted!' });
                                res.end();
                            }
                            else {
                                res.status(400);
                                res.send(err);
                                res.end();
                                console.log('delete_photo error unlink ' + err);
                            }
                        });
                    }
                    else {
                        res.status(400);
                        res.send(err);
                        res.end();
                        console.log('delete_photo error findOneAndUpdate ' + err);
                    }
                });
            }
            else {
                res.status(400);
                res.json({ message: 'No such file = ' + fileName + ' or directory!' });
                res.end();
                console.log('No such file = ' + fileName + ' or directory!');
            }
        }
        else {
            res.status(400);
            res.send(err);
            res.end();
            console.log('delete_photo error findOne ' + err);
        }
    });
};

exports.make_first = function (req, res) {

    console.log('make_first ' + req.params.fileName);

    var fileName = req.params.fileName;
    var length = 0;
    var fileExist = false;
    var _id = fileName.substring(0, 24);

    Item.findOne({ _id: _id }, { __v: 0 }, function (err, item) {
        if (!err) {

            if (item != null) {
                length = item.photos.length;

                for (var i = 0; i < length; i++) {
                    if (item.photos[i] == fileName) {
                        // to remove item i from array
                        item.photos.splice(i, 1);
                        fileExist = true;
                    }
                };
            };

            if (fileExist) {

                // The unshift() method adds new items to the beginning of an array
                item.photos.unshift(fileName);

                Item.findOneAndUpdate({ _id: _id }, item, { new: true }, function (err, item) {
                    if (!err) {
                        res.status(200);
                        res.json(item);
                        res.end();
                    }
                    else {
                        res.status(400);
                        res.send(err);
                        res.end();
                        console.log('make_first error findOneAndUpdate ' + err);
                    }
                });
            }
            else {
                res.status(400);
                res.json({ message: 'No such file = ' + fileName + ' or directory!' });
                res.end();
                console.log('No such file = ' + fileName + ' or directory!');
            }
        }
        else {
            res.status(400);
            res.send(err);
            res.end();
            console.log('make_first error findOne ' + err);
        }
    });
};
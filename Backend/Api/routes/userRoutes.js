'use strict';
module.exports = function (app) {
  var User = require('../controllers/userController');

  // User Routes
  app.route('/users/get_all_users')
    .get(User.get_all_users);

  app.route('/users/add_user')
    .post(User.add_user);

  app.route('/users/get_user/:userId')
    .get(User.get_user);

  app.route('/users/update_user')
    .put(User.update_user);

  app.route('/users/delete_user/:userId')
    .delete(User.delete_user);

  app.route('/users/update_photo')
    .put(User.update_photo);

  app.route('/users/get_photo/:userId')
    .get(User.get_photo);
};
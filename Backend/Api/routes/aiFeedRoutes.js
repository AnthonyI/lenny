'use strict';
module.exports = function (app) {
  var AiFeed = require('../controllers/AiFeedController');

  // User Routes
  app.route('/ai_feed/add_feed')
    .post(AiFeed.add_feed);

  app.route('/ai_feed/get_feed/:userId')
    .get(AiFeed.get_feed);

  app.route('/ai_feed/update_feed')
    .put(AiFeed.update_feed);

  app.route('/ai_feed/delete_feed/:userId')
    .delete(AiFeed.delete_feed);

  // AiFeed Routes
  app.route('/aifeeds/load_feed/:userId')
    .get(AiFeed.Load_Feed);

};
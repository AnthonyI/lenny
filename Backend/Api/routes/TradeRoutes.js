'use strict';
module.exports = function (app) {
  var Item = require('../controllers/itemController');

  // Item Routes
  app.route('/items/get_all_items')
    .get(Item.get_all_items);

  app.route('/items/add_item')
    .post(Item.add_item);

  app.route('/items/get_item/:_id')
    .get(Item.get_item);

  app.route('/items/update_item')
    .put(Item.update_item);

  app.route('/items/delete_item/:_id')
    .delete(Item.delete_item);

  app.route('/items/get_my_items/:userId')
    .get(Item.get_my_items);

  app.route('/items/add_photo')
    .post(Item.add_photo);

  app.route('/items/get_photo/:fileName')
    .get(Item.get_photo);

  app.route('/items/delete_photo/:fileName')
    .delete(Item.delete_photo);

  app.route('/items/first_photo/:fileName')
    .put(Item.first_photo);
};
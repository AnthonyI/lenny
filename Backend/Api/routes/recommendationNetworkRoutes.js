'use strict';
module.exports = function (app) {
  var Network = require('../controllers/recommendationNetworkController');

  // User Routes
  app.route('/networks/get_all_networks')
    .get(Network.get_all_networks);

  app.route('/networks/add_network')
    .post(Network.add_network);

  app.route('/networks/get_network/:userId')
    .get(Network.get_network);

  app.route('/networks/update_network')
    .put(Network.update_network);

  app.route('/networks/delete_network/:userId')
    .delete(Network.delete_network);
};
var expect = require('chai').expect;
var request = require('request');
var uri = 'http://localhost:8080';

var user = '{"userId":"88888","name":"PhotoTest","surname":"surname1"}';
var userJson = JSON.parse(user);

var item = '{"userId":"88888","title":"PhotoTest"}';
var itemJson = JSON.parse(item);

const fs = require('fs');
const dir = './photos/itemsPhotos/';

var _id = '';
var fileName1 = '';
var fileName2 = '';
var fileName3 = '';
var fileName4 = '';
var fileName5 = '';

// for skip test: xit 

describe('Delete item tests', function () {

    it('Should post the new user', function (done) {

        request.post(
            {
                url: `${uri}/users/add_user`,
                form: userJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.userId).to.equal(userJson.userId);
                expect(bodyJson.name).to.equal(userJson.name);
                expect(bodyJson.surname).to.equal(userJson.surname);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should post the new item', function (done) {

        request.post(
            {
                url: `${uri}/items/add_item`,
                form: itemJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                _id = bodyJson._id;
                expect(bodyJson.userId).to.equal(itemJson.userId);
                expect(bodyJson.title).to.equal(itemJson.title);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should add photo1', function (done) {

        var photo = '{"photo":"PhotoTest photo1 in base64"}';
        var photoJson = JSON.parse(photo);
        photoJson._id = _id;

        request.post(
            {
                url: `${uri}/items/add_photo`,
                form: photoJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.fileName.length).to.equal(52);

                expect(bodyJson.fileName.substring(0, 27)).to.equal(_id + '_20');
                expect(bodyJson.fileName.substring(49, 52)).to.equal('.tw');

                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should add photo2', function (done) {

        var photo = '{"photo":"PhotoTest photo2 in base64"}';
        var photoJson = JSON.parse(photo);
        photoJson._id = _id;

        request.post(
            {
                url: `${uri}/items/add_photo`,
                form: photoJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.fileName.length).to.equal(52);

                expect(bodyJson.fileName.substring(0, 27)).to.equal(_id + '_20');
                expect(bodyJson.fileName.substring(49, 52)).to.equal('.tw');

                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should add photo3', function (done) {

        var photo = '{"photo":"PhotoTest photo3 in base64"}';
        var photoJson = JSON.parse(photo);
        photoJson._id = _id;

        request.post(
            {
                url: `${uri}/items/add_photo`,
                form: photoJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.fileName.length).to.equal(52);

                expect(bodyJson.fileName.substring(0, 27)).to.equal(_id + '_20');
                expect(bodyJson.fileName.substring(49, 52)).to.equal('.tw');

                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should add photo4', function (done) {

        var photo = '{"photo":"PhotoTest photo4 in base64"}';
        var photoJson = JSON.parse(photo);
        photoJson._id = _id;

        request.post(
            {
                url: `${uri}/items/add_photo`,
                form: photoJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.fileName.length).to.equal(52);

                expect(bodyJson.fileName.substring(0, 27)).to.equal(_id + '_20');
                expect(bodyJson.fileName.substring(49, 52)).to.equal('.tw');

                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should add photo5', function (done) {

        var photo = '{"photo":"PhotoTest photo5 in base64"}';
        var photoJson = JSON.parse(photo);
        photoJson._id = _id;

        request.post(
            {
                url: `${uri}/items/add_photo`,
                form: photoJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.fileName.length).to.equal(52);

                expect(bodyJson.fileName.substring(0, 27)).to.equal(_id + '_20');
                expect(bodyJson.fileName.substring(49, 52)).to.equal('.tw');

                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    xit('Should have 5 files in the folder "itemsPhotos"', function (done) {

        fs.readdir(dir, (err, files) => {
            expect(files.length).to.equal(5);
            done();
        });
    });

    it('Should get item by _id', function (done) {
        request.get(
            {
                url: `${uri}/items/get_item/${_id}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                fileName1 = bodyJson.photos[0];
                fileName2 = bodyJson.photos[1];
                fileName3 = bodyJson.photos[2];
                fileName4 = bodyJson.photos[3];
                fileName5 = bodyJson.photos[4];
                expect(bodyJson._id).to.equal(_id);
                expect(bodyJson.userId).to.equal(itemJson.userId);
                expect(bodyJson.title).to.equal(itemJson.title);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should find file1 in folder the "itemsPhotos"', function (done) {

        fs.exists(dir + fileName1, (exists) => {
            expect(exists).to.equal(true);
            done();
        });
    });

    it('Should find file2 in folder the "itemsPhotos"', function (done) {

        fs.exists(dir + fileName2, (exists) => {
            expect(exists).to.equal(true);
            done();
        });
    });

    it('Should find file3 in folder the "itemsPhotos"', function (done) {

        fs.exists(dir + fileName3, (exists) => {
            expect(exists).to.equal(true);
            done();
        });
    });

    it('Should find file4 in folder the "itemsPhotos"', function (done) {

        fs.exists(dir + fileName4, (exists) => {
            expect(exists).to.equal(true);
            done();
        });
    });

    it('Should find file5 in folder the "itemsPhotos"', function (done) {

        fs.exists(dir + fileName5, (exists) => {
            expect(exists).to.equal(true);
            done();
        });
    });

    it('Should delete item', function (done) {

        request.delete(
            {
                url: `${uri}/items/delete_item/${_id}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Item successfully deleted');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should not find deleted items', function (done) {
        request.get(
            {
                url: `${uri}/items/get_item/${_id}`
            }
            , function (error, response, body) {
                expect(body).to.equal('null');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    xit('Should not have any files in the folder "itemsPhotos"', function (done) {

        // setTimeout because it must wait when 5 files will be deleted
        setTimeout(function () {
            fs.readdir(dir, (err, files) => {
                expect(files.length).to.equal(0);
                done();
            });
        }, 100);
    });

    it('Should not find file1 in folder the "itemsPhotos"', function (done) {
        // setTimeout because it must wait when 5 files will be deleted
        setTimeout(function () {
            fs.exists(dir + fileName1, (exists) => {
                expect(exists).to.equal(false);
                done();
            });
        }, 100);
    });

    it('Should not find file2 in folder the "itemsPhotos"', function (done) {

        fs.exists(dir + fileName2, (exists) => {
            expect(exists).to.equal(false);
            done();
        });
    });

    it('Should not find file3 in folder the "itemsPhotos"', function (done) {

        fs.exists(dir + fileName3, (exists) => {
            expect(exists).to.equal(false);
            done();
        });
    });

    it('Should not find file4 in folder the "itemsPhotos"', function (done) {

        fs.exists(dir + fileName4, (exists) => {
            expect(exists).to.equal(false);
            done();
        });
    });

    it('Should not find file5 in folder the "itemsPhotos"', function (done) {

        fs.exists(dir + fileName5, (exists) => {
            expect(exists).to.equal(false);
            done();
        });
    });

    it('Should delete user', function (done) {

        request.delete(
            {
                url: `${uri}/users/delete_user/${userJson.userId}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('User successfully deleted');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });
});


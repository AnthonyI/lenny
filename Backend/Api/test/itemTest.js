var expect = require('chai').expect;
var request = require('request');
var uri = 'http://localhost:8080';

var user = '{"userId":"77777","name":"ItemTest","surname":"surname1"}';
var userJson = JSON.parse(user);

var user2 = '{"userId":"77776","name":"ItemTest","surname":"surname1"}';
var user2Json = JSON.parse(user2);

// item with userId wich does not exist
var item1 = '{"userId":"11111","type":"Shirt","title":"title1-1"}';
var item1Json = JSON.parse(item1);

// item with error in quantity
var item2 = '{"userId":"77777","type":"Shoes","title":"title1-1","quantity":"a"}';
var item2Json = JSON.parse(item2);

var item3 = '{"userId":"77777","type":"Shoes","title":"title3","quantity":1}';
var item3Json = JSON.parse(item3);
var _id3 = "";

var item4 = '{"userId":"77777","type":"Shoes","title":"title4","quantity":1}';
var item4Json = JSON.parse(item4);
var _id4 = "";

var item5 = '{"userId":"77776","type":"Shoes","title":"title5","quantity":1}';
var item5Json = JSON.parse(item5);
var _id5 = "";

var item6 = '{"userId":"77776","type":"Shoes","title":"title6","quantity":1}';
var item6Json = JSON.parse(item6);
var _id6 = "";


// for skip test: xit 

describe('Items tests', function () {

    before(function () {
        request.post(
            {
                url: `${uri}/users/add_user`,
                form: userJson
            }
        );

        request.post(
            {
                url: `${uri}/users/add_user`,
                form: user2Json
            }
        );
    });

    after(function () {
        request.delete(
            {
                url: `${uri}/users/delete_user/${userJson.userId}`
            }
        );

        request.delete(
            {
                url: `${uri}/users/delete_user/${user2Json.userId}`
            }
        );
    });

    it('Should not post the new item if user does not exist', function (done) {
        request.post(
            {
                url: `${uri}/items/add_item`,
                form: item1Json
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('User with the given userId does not exist');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should not post the new item if "quantity" is not a number', function (done) {
        request.post(
            {
                url: `${uri}/items/add_item`,
                form: item2Json
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should get all (0) items', function (done) {
        request.get(
            {
                url: `${uri}/items/get_all_items`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.length).to.equal(0);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should post the new item if user exists', function (done) {
        request.post(
            {
                url: `${uri}/items/add_item`,
                form: item3Json
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                _id3 = bodyJson._id;
                expect(bodyJson.userId).to.equal(item3Json.userId);
                expect(bodyJson.type).to.equal(item3Json.type);
                expect(bodyJson.title).to.equal(item3Json.title);
                expect(bodyJson.quantity).to.equal(item3Json.quantity);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should get item by _id', function (done) {
        request.get(
            {
                url: `${uri}/items/get_item/${_id3}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson._id).to.equal(_id3);
                expect(bodyJson.userId).to.equal(item3Json.userId);
                expect(bodyJson.type).to.equal(item3Json.type);
                expect(bodyJson.title).to.equal(item3Json.title);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should return Bad Request "400" "CastError" ObjectId format is incorrect in get_item', function (done) {
        request.get(
            {
                url: `${uri}/items/get_item/01234567890123456789012`
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should return Bad Request "404 Not Found" parameter is absent', function (done) {
        request.get(
            {
                url: `${uri}/items/get_item/`
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(404);
                done();
            });
    });

    it('Should not update item "_id is required for update item"', function (done) {

        item3Json.quantity = 5;

        request.put(
            {
                url: `${uri}/items/update_item`,
                form: item3Json
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('_id is required for update item');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should update item', function (done) {
        item3Json._id = _id3;
        item3Json.type = "Shirt";
        item3Json.quantity = 3;
        item3Json.deleted = "true";
        request.put(
            {
                url: `${uri}/items/update_item`,
                form: item3Json
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson._id).to.equal(_id3);
                expect(bodyJson.userId).to.equal(item3Json.userId);
                expect(bodyJson.type).to.equal(item3Json.type);
                expect(bodyJson.title).to.equal(item3Json.title);
                expect(bodyJson.quantity).to.equal(item3Json.quantity);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should not update item due to "quantity" data format', function (done) {

        item3Json._id = _id3;
        item3Json.quantity = "a";

        request.put(
            {
                url: `${uri}/items/update_item`,
                form: item3Json
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should not update item due to "deleted" data format', function (done) {

        item3Json._id = _id3;
        item3Json.quantity = 5;
        item3Json.deleted = "tr";

        request.put(
            {
                url: `${uri}/items/update_item`,
                form: item3Json
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should not update item due to "Item with _id=  does not exist"', function (done) {

        item3Json._id = '012345678901234567890123';
        item3Json.quantity = 5;
        item3Json.deleted = "true";

        request.put(
            {
                url: `${uri}/items/update_item`,
                form: item3Json
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Item with _id=012345678901234567890123 does not exist');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should return Bad Request "400" "CastError" ObjectId format is incorrect in update_item', function (done) {

        item3Json._id = '01234567890123456789012';
        item3Json.quantity = 5;
        item3Json.deleted = "true";

        request.put(
            {
                url: `${uri}/items/update_item`,
                form: item3Json
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should post second item', function (done) {
        request.post(
            {
                url: `${uri}/items/add_item`,
                form: item4Json
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                _id4 = bodyJson._id;
                expect(bodyJson.userId).to.equal(item4Json.userId);
                expect(bodyJson.type).to.equal(item4Json.type);
                expect(bodyJson.title).to.equal(item4Json.title);
                expect(bodyJson.quantity).to.equal(item4Json.quantity);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should get all (2) items by userId', function (done) {
        request.get(
            {
                url: `${uri}/items/get_my_items/${userJson.userId}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.length).to.equal(2);
                expect(bodyJson[0]._id).to.equal(_id3);
                expect(bodyJson[1]._id).to.equal(_id4);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should get all (2) items', function (done) {
        request.get(
            {
                url: `${uri}/items/get_all_items`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.length).to.equal(2);
                expect(bodyJson[0]._id).to.equal(_id3);
                expect(bodyJson[1]._id).to.equal(_id4);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should post the new item for second user', function (done) {
        request.post(
            {
                url: `${uri}/items/add_item`,
                form: item5Json
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                _id5 = bodyJson._id;
                expect(bodyJson.userId).to.equal(item5Json.userId);
                expect(bodyJson.type).to.equal(item5Json.type);
                expect(bodyJson.title).to.equal(item5Json.title);
                expect(bodyJson.quantity).to.equal(item5Json.quantity);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should get all (3) items', function (done) {
        request.get(
            {
                url: `${uri}/items/get_all_items`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.length).to.equal(3);
                expect(bodyJson[0]._id).to.equal(_id3);
                expect(bodyJson[1]._id).to.equal(_id4);
                expect(bodyJson[2]._id).to.equal(_id5);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should post the new item for second user', function (done) {
        request.post(
            {
                url: `${uri}/items/add_item`,
                form: item6Json
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                _id6 = bodyJson._id;
                expect(bodyJson.userId).to.equal(item6Json.userId);
                expect(bodyJson.type).to.equal(item6Json.type);
                expect(bodyJson.title).to.equal(item6Json.title);
                expect(bodyJson.quantity).to.equal(item6Json.quantity);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should get all (4) items', function (done) {
        request.get(
            {
                url: `${uri}/items/get_all_items`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.length).to.equal(4);
                expect(bodyJson[0]._id).to.equal(_id3);
                expect(bodyJson[1]._id).to.equal(_id4);
                expect(bodyJson[2]._id).to.equal(_id5);
                expect(bodyJson[3]._id).to.equal(_id6);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should return Bad Request "404 Not Found" parameter is absent', function (done) {
        request.delete(
            {
                url: `${uri}/items/delete_item/`
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(404);
                done();
            });
    });

    it('Should return Bad Request "400" "CastError" ObjectId format is incorrect in delete_item', function (done) {
        request.delete(
            {
                url: `${uri}/items/delete_item/01234567890123456789012`
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should delete first item', function (done) {
        request.delete(
            {
                url: `${uri}/items/delete_item/${_id3}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Item successfully deleted');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should get all (1) items by userId', function (done) {
        request.get(
            {
                url: `${uri}/items/get_my_items/${userJson.userId}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.length).to.equal(1);
                expect(bodyJson[0]._id).to.equal(_id4);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should delete second item', function (done) {
        request.delete(
            {
                url: `${uri}/items/delete_item/${_id4}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Item successfully deleted');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should not find deleted items', function (done) {
        request.get(
            {
                url: `${uri}/items/get_my_items/${userJson.userId}`
            }
            , function (error, response, body) {
                expect(body).to.equal('[]');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should delete first item from second user', function (done) {
        request.delete(
            {
                url: `${uri}/items/delete_item/${_id5}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Item successfully deleted');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should delete second item from second user', function (done) {
        request.delete(
            {
                url: `${uri}/items/delete_item/${_id6}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Item successfully deleted');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should get all (0) items', function (done) {
        request.get(
            {
                url: `${uri}/items/get_all_items`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.length).to.equal(0);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

});

var expect = require('chai').expect;
var request = require('request');
var uri = 'http://localhost:8080';

var user = '{"userId":"66666","name":"RecommendationNetworkTest","surname":"surname1"}';
var userJson = JSON.parse(user);


// for skip test: xit 

describe('RecommendationNetwork tests', function () {

    before(function () {
        request.post(
            {
                url: `${uri}/users/add_user`,
                form: userJson
            }
        );
    });

    after(function () {
        request.delete(
            {
                url: `${uri}/users/delete_user/${userJson.userId}`
            }
        );
    });

    it('Should not post the new network without userId', function (done) {

        var netWork = '{"network":{"color": "red","category": "category1"}}';
        var networkJson = JSON.parse(netWork);

        request.post(
            {
                url: `${uri}/networks/add_network`,
                form: networkJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('UserId is required for add network');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should not post the new network without netWork', function (done) {

        var netWork = '{"userId":"66666"}';
        var networkJson = JSON.parse(netWork);

        request.post(
            {
                url: `${uri}/networks/add_network`,
                form: networkJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Network is required for add network');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should not post the new network if user does not exist', function (done) {

        var netWork = '{"userId":"55555","network":{"color": "red","category": "category1"}}';
        var networkJson = JSON.parse(netWork);

        request.post(
            {
                url: `${uri}/networks/add_network`,
                form: networkJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('User with the given userId does not exist');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should post the new network', function (done) {

        var netWork = '{"userId":"66666","network":{"color": "red","category": "category1"}}';
        var networkJson = JSON.parse(netWork);

        request.post(
            {
                url: `${uri}/networks/add_network`,
                form: networkJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.userId).to.equal(networkJson.userId);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should return Bad Request the new network exists', function (done) {

        var netWork = '{"userId":"66666","network":{"color": "red","category": "category1"}}';
        var networkJson = JSON.parse(netWork);

        request.post(
            {
                url: `${uri}/networks/add_network`,
                form: networkJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Network with the given userId already exists');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should update network', function (done) {

        var netWork = '{"userId":"66666","network":{"color": "blue","category": "category2"}}';
        var networkJson = JSON.parse(netWork);

        request.put(
            {
                url: `${uri}/networks/update_network`,
                form: networkJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.userId).to.equal(networkJson.userId);
                expect(bodyJson.network.color).to.equal(networkJson.network.color);
                expect(bodyJson.network.category).to.equal(networkJson.network.category);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should not update network due to "Network with _id=  does not exist"', function (done) {

        var netWork = '{"userId":"55555","network":{"color": "blue","category": "category2"}}';
        var networkJson = JSON.parse(netWork);

        request.put(
            {
                url: `${uri}/networks/update_network`,
                form: networkJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Network with userId=55555 does not exist');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should get network by userId', function (done) {

        var netWork = '{"userId":"66666","network":{"color": "blue","category": "category2"}}';
        var networkJson = JSON.parse(netWork);

        request.get(
            {
                url: `${uri}/networks/get_network/${networkJson.userId}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.userId).to.equal(networkJson.userId);
                //expect(bodyJson.network.color).to.equal(networkJson.network.color);
                //expect(bodyJson.network.category).to.equal(networkJson.network.category);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should delete the network', function (done) {

        request.delete(
            {
                url: `${uri}/networks/delete_network/66666`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Network successfully deleted');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

});

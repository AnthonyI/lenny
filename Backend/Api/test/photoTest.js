var expect = require('chai').expect;
var request = require('request');
var uri = 'http://localhost:8080';

var user = '{"userId":"88888","name":"PhotoTest","surname":"surname1"}';
var userJson = JSON.parse(user);

var item = '{"userId":"88888","title":"PhotoTest"}';
var itemJson = JSON.parse(item);


var _id = "";
var fileName1 = "";
var fileName2 = "";
var fileName3 = "";
var fileName4 = "";
var fileName5 = "";

// for skip test: xit 

describe('photo tests', function () {

    it('Should post the new user', function (done) {

        request.post(
            {
                url: `${uri}/users/add_user`,
                form: userJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.userId).to.equal(userJson.userId);
                expect(bodyJson.name).to.equal(userJson.name);
                expect(bodyJson.surname).to.equal(userJson.surname);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should post the new item', function (done) {

        request.post(
            {
                url: `${uri}/items/add_item`,
                form: itemJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                _id = bodyJson._id;
                expect(bodyJson.userId).to.equal(itemJson.userId);
                expect(bodyJson.title).to.equal(itemJson.title);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should not have photos in the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_item/${_id}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.photos.length).to.equal(0);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should add photo1', function (done) {

        var photo = '{"photo":"PhotoTest photo1 in base64"}';
        var photoJson = JSON.parse(photo);
        photoJson._id = _id;

        request.post(
            {
                url: `${uri}/items/add_photo`,
                form: photoJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.fileName.length).to.equal(52);

                expect(bodyJson.fileName.substring(0, 27)).to.equal(_id + '_20');
                expect(bodyJson.fileName.substring(49, 52)).to.equal('.tw');

                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should have 1 photo in the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_item/${_id}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                fileName1 = bodyJson.photos[0];
                expect(bodyJson.photos.length).to.equal(1);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should get photo1 of the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_photo/${fileName1}`
            }
            , function (error, response, body) {
                expect(body).to.equal('PhotoTest photo1 in base64');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should add photo2', function (done) {

        var photo = '{"photo":"PhotoTest photo2 in base64"}';
        var photoJson = JSON.parse(photo);
        photoJson._id = _id;

        request.post(
            {
                url: `${uri}/items/add_photo`,
                form: photoJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.fileName.length).to.equal(52);

                expect(bodyJson.fileName.substring(0, 27)).to.equal(_id + '_20');
                expect(bodyJson.fileName.substring(49, 52)).to.equal('.tw');

                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should have 2 photos in the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_item/${_id}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                fileName2 = bodyJson.photos[1];
                expect(bodyJson.photos.length).to.equal(2);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should get photo2 of the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_photo/${fileName2}`
            }
            , function (error, response, body) {
                expect(body).to.equal('PhotoTest photo2 in base64');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should add photo3', function (done) {

        var photo = '{"photo":"PhotoTest photo3 in base64"}';
        var photoJson = JSON.parse(photo);
        photoJson._id = _id;

        request.post(
            {
                url: `${uri}/items/add_photo`,
                form: photoJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.fileName.length).to.equal(52);

                expect(bodyJson.fileName.substring(0, 27)).to.equal(_id + '_20');
                expect(bodyJson.fileName.substring(49, 52)).to.equal('.tw');

                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should have 3 photos in the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_item/${_id}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                fileName3 = bodyJson.photos[2];
                expect(bodyJson.photos.length).to.equal(3);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should get photo3 of the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_photo/${fileName3}`
            }
            , function (error, response, body) {
                expect(body).to.equal('PhotoTest photo3 in base64');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should add photo4', function (done) {

        var photo = '{"photo":"PhotoTest photo4 in base64"}';
        var photoJson = JSON.parse(photo);
        photoJson._id = _id;

        request.post(
            {
                url: `${uri}/items/add_photo`,
                form: photoJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.fileName.length).to.equal(52);

                expect(bodyJson.fileName.substring(0, 27)).to.equal(_id + '_20');
                expect(bodyJson.fileName.substring(49, 52)).to.equal('.tw');

                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should have 4 photos in the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_item/${_id}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                fileName4 = bodyJson.photos[3];
                expect(bodyJson.photos.length).to.equal(4);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should get photo4 of the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_photo/${fileName4}`
            }
            , function (error, response, body) {
                expect(body).to.equal('PhotoTest photo4 in base64');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should add photo5', function (done) {

        var photo = '{"photo":"PhotoTest photo5 in base64"}';
        var photoJson = JSON.parse(photo);
        photoJson._id = _id;

        request.post(
            {
                url: `${uri}/items/add_photo`,
                form: photoJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.fileName.length).to.equal(52);

                expect(bodyJson.fileName.substring(0, 27)).to.equal(_id + '_20');
                expect(bodyJson.fileName.substring(49, 52)).to.equal('.tw');

                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should not add photo6', function (done) {

        var photo = '{"photo":"PhotoTest photo6 in base64"}';
        var photoJson = JSON.parse(photo);
        photoJson._id = _id;

        request.post(
            {
                url: `${uri}/items/add_photo`,
                form: photoJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('The maximum number of photos is 5');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should have 5 photos in the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_item/${_id}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                fileName5 = bodyJson.photos[4];
                expect(bodyJson.photos.length).to.equal(5);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should get photo5 of the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_photo/${fileName5}`
            }
            , function (error, response, body) {
                expect(body).to.equal('PhotoTest photo5 in base64');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should make the first photo5', function (done) {

        request.put(
            {
                url: `${uri}/items/make_first/${fileName5}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.photos[0]).to.equal(fileName5);
                expect(bodyJson.photos[1]).to.equal(fileName1);
                expect(bodyJson.photos[2]).to.equal(fileName2);
                expect(bodyJson.photos[3]).to.equal(fileName3);
                expect(bodyJson.photos[4]).to.equal(fileName4);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should return Bad Request "404 Not Found" parameter is absent in make_first', function (done) {

        request.put(
            {
                url: `${uri}/items/make_first/`
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(404);
                done();
            });
    });

    it('Should return Bad Request "404 Not Found" parameter is absent in delete_photo', function (done) {

        request.delete(
            {
                url: `${uri}/items/delete_photo/`
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(404);
                done();
            });
    });

    it('Should return Bad Request "404 Not Found" parameter is absent in get_photo', function (done) {

        request.get(
            {
                url: `${uri}/items/get_photo/`
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(404);
                done();
            });
    });

    it('Should return Bad Request "400" "CastError" ObjectId format is incorrect in delete_photo', function (done) {

        request.delete(
            {
                url: `${uri}/items/delete_photo/test`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should return Bad Request "400" "CastError" ObjectId format is incorrect in make_first', function (done) {

        request.put(
            {
                url: `${uri}/items/make_first/test`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should return Bad Request "400" "CastError" ObjectId format is incorrect in add_photo', function (done) {

        var photo = '{"photo":"PhotoTest photo1 in base64"}';
        var photoJson = JSON.parse(photo);
        photoJson._id = '01234567890123456789012';

        request.post(
            {
                url: `${uri}/items/add_photo`,
                form: photoJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should not add photo without item', function (done) {

        var photo = '{"photo":"PhotoTest photo1 in base64"}';
        var photoJson = JSON.parse(photo);
        photoJson._id = '012345678901234567890123';

        request.post(
            {
                url: `${uri}/items/add_photo`,
                form: photoJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Item with the given _id does not exist');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should return Bad Request "400" "No such file or directory!" in delete_photo', function (done) {

        request.delete(
            {
                url: `${uri}/items/delete_photo/012345678901234567890123`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('No such file = 012345678901234567890123 or directory!');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should return Bad Request "400" "No such file or directory!" in get_photo', function (done) {

        request.get(
            {
                url: `${uri}/items/get_photo/test`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('No such file = test or directory!');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should return Bad Request "400" "No such file or directory!" in make_first', function (done) {

        request.put(
            {
                url: `${uri}/items/make_first/012345678901234567890123`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('No such file = 012345678901234567890123 or directory!');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should delete photo5', function (done) {

        request.delete(
            {
                url: `${uri}/items/delete_photo/${fileName5}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Photo ' + fileName5 + ' deleted!');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should have 4 photo in the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_item/${_id}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.photos.length).to.equal(4);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should delete photo4', function (done) {

        request.delete(
            {
                url: `${uri}/items/delete_photo/${fileName4}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Photo ' + fileName4 + ' deleted!');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should have 3 photo in the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_item/${_id}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.photos.length).to.equal(3);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should delete photo3', function (done) {

        request.delete(
            {
                url: `${uri}/items/delete_photo/${fileName3}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Photo ' + fileName3 + ' deleted!');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should have 2 photo in the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_item/${_id}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.photos.length).to.equal(2);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should delete photo2', function (done) {

        request.delete(
            {
                url: `${uri}/items/delete_photo/${fileName2}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Photo ' + fileName2 + ' deleted!');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should have 1 photo in the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_item/${_id}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.photos.length).to.equal(1);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should delete photo1', function (done) {

        request.delete(
            {
                url: `${uri}/items/delete_photo/${fileName1}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Photo ' + fileName1 + ' deleted!');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should not have photos in the item', function (done) {

        request.get(
            {
                url: `${uri}/items/get_item/${_id}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.photos.length).to.equal(0);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should delete item', function (done) {

        request.delete(
            {
                url: `${uri}/items/delete_item/${_id}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Item successfully deleted');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should delete user', function (done) {

        request.delete(
            {
                url: `${uri}/users/delete_user/${userJson.userId}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('User successfully deleted');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });
});


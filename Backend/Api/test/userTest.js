
var expect = require('chai').expect;
var request = require('request');
var uri = 'http://localhost:8080/users';
const fs = require('fs');

// for skip test: xit 

describe('Users tests', function () {

    it('Should not post the new user without userId', function (done) {

        var user = '{"name":"name1","surname":"surname1"}';
        var userJson = JSON.parse(user);

        request.post(
            {
                url: `${uri}/add_user`,
                form: userJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.errors.userId.message).to.equal('Path `userId` is required.');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should not post the new user due to birthDate date format', function (done) {

        var user = '{"userId":"11111","name":"name1","surname":"surname1","birthDate":"13/13/2019"}';
        var userJson = JSON.parse(user);

        request.post(
            {
                url: `${uri}/add_user`,
                form: userJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should post the new user', function (done) {

        var user = '{"userId":"11111","name":"name1","surname":"surname1"}';
        var userJson = JSON.parse(user);

        request.post(
            {
                url: `${uri}/add_user`,
                form: userJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.userId).to.equal(userJson.userId);
                expect(bodyJson.name).to.equal(userJson.name);
                expect(bodyJson.surname).to.equal(userJson.surname);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should return Bad Request the new user exists', function (done) {

        var user = '{"userId":"11111","name":"name1","surname":"surname1"}';
        var userJson = JSON.parse(user);

        request.post(
            {
                url: `${uri}/add_user`,
                form: userJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('User with the given userId already exists');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should get user by userId', function (done) {

        var user = '{"userId":"11111","name":"name1","surname":"surname1"}';
        var userJson = JSON.parse(user);

        request.get(
            {
                url: `${uri}/get_user/${userJson.userId}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.userId).to.equal(userJson.userId);
                expect(bodyJson.name).to.equal(userJson.name);
                expect(bodyJson.surname).to.equal(userJson.surname);
                expect(bodyJson.photo).to.equal(undefined);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should return Bad Request "404 Not Found" parameter is absent in get_user', function (done) {

        request.get(
            {
                url: `${uri}/get_user/`
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(404);
                done();
            });
    });

    it('Should return Bad Request "404 Not Found" parameter is absent in get_photo', function (done) {

        request.get(
            {
                url: `${uri}/get_photo/`
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(404);
                done();
            });
    });

    it('Should return Bad Request "400" "No such file or directory!" in get_photo', function (done) {

        var user = '{"userId":"00000"}';
        var userJson = JSON.parse(user);

        request.get(
            {
                url: `${uri}/get_photo/${userJson.userId}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('No such file = ' + userJson.userId + ' or directory!');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should update user', function (done) {

        var user = '{"userId":"11111","name":"name1-1","surname":"surname1-1","inboardingDone":true,"birthDate":"12/12/2019","profile":{"color": "red","category": "category1"}}';
        var userJson = JSON.parse(user);

        request.put(
            {
                url: `${uri}/update_user`,
                form: userJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.userId).to.equal(userJson.userId);
                expect(bodyJson.name).to.equal(userJson.name);
                expect(bodyJson.surname).to.equal(userJson.surname);
                expect(bodyJson.inboardingDone).to.equal(userJson.inboardingDone);
                expect(bodyJson.profile.color).to.equal(userJson.profile.color);
                expect(bodyJson.profile.category).to.equal(userJson.profile.category);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should update photo in "users/update_photo"', function (done) {

        var user = '{"userId":"11111","photo":"Photo2 in base64"}';
        var userJson = JSON.parse(user);

        request.put(
            {
                url: `${uri}/update_photo`,
                form: userJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('Profile picture updated');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should not update photo in "users/update_photo the user does not exist"', function (done) {

        var user = '{"userId":"11112","photo":"Photo2 in base64"}';
        var userJson = JSON.parse(user);

        request.put(
            {
                url: `${uri}/update_photo`,
                form: userJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('User with userId=11112 does not exist');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should get users updated photo', function (done) {

        var user = '{"userId":"11111"}';
        var userJson = JSON.parse(user);

        request.get(
            {
                url: `${uri}/get_photo/${userJson.userId}`
            }
            , function (error, response, body) {
                expect(body).to.equal('Photo2 in base64');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should not update user due to birthDate date format', function (done) {

        var user = '{"userId":"11111","birthDate":"13/13/2019"}';
        var userJson = JSON.parse(user);

        request.put(
            {
                url: `${uri}/update_user`,
                form: userJson
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should not update user due to inboardingDone format', function (done) {

        var user = '{"userId":"11111", "inboardingDone":"a"}';
        var userJson = JSON.parse(user);

        request.put(
            {
                url: `${uri}/update_user`,
                form: userJson
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should return "400 Bad Request" and message "UserId is required for update user"', function (done) {

        var user = '{"name":"name1","surname":"surname1"}';
        var userJson = JSON.parse(user);

        request.put(
            {
                url: `${uri}/update_user`,
                form: userJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('UserId is required for update user');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should return "400 Bad Request" and message "User with userId=00000 does not exist"', function (done) {

        var user = '{"userId":"00000","name":"name1","surname":"surname1"}';
        var userJson = JSON.parse(user);

        request.put(
            {
                url: `${uri}/update_user`,
                form: userJson
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('User with userId=' + userJson.userId + ' does not exist');
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should return Bad Request "404 Not Found" parameter is absent', function (done) {

        request.get(
            {
                url: `${uri}/delete_user/`
            }
            , function (error, response, body) {
                expect(response.statusCode).to.equal(404);
                done();
            });
    });

    it('Should have file 11111 in the folder "usersPhotos"', function (done) {

        const dir = './photos/usersPhotos/11111';

        fs.exists(dir, (exists) => {
            expect(exists).to.equal(true);
            done();
        });
    })

    it('Should delete user by userId', function (done) {

        var user = '{"userId":"11111","name":"name1","surname":"surname1"}';
        var userJson = JSON.parse(user);

        request.delete(
            {
                url: `${uri}/delete_user/${userJson.userId}`
            }
            , function (error, response, body) {
                var bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal('User successfully deleted');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should not find deleted user', function (done) {

        var user = '{"userId":"11111"}';
        var userJson = JSON.parse(user);

        request.get(
            {
                url: `${uri}/get_user/${userJson.userId}`
            }
            , function (error, response, body) {
                expect(body).to.equal('null');
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should not have file 11111 in the folder "usersPhotos"', function (done) {

        const dir = './photos/usersPhotos/11111';        

            fs.exists(dir, (exists) => {
                expect(exists).to.equal(false);
                done();
            });        
    })

});


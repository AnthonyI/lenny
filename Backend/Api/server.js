const MONGO_CONNECTION_STRING_LOCAL = 'mongodb://localhost/TradeAndWearDB';
const MONGO_CONNECTION_STRING_DEVELOP = 'mongodb+srv://akram_TW:' + process.env.TW_MONGO_SECRET + '@clustertw-ioece.gcp.mongodb.net/TradeAndWearDB?retryWrites=true';

/************************************************************************************
  The server can be started by running the following command in any terminal:
  npm start
  You must have specified the database connection string in the TW_MONGO_SECRET environment variable.

  If you wish to debug using a local database, use the command "node server.js --local"

  Important: for remote database, go to DB cloud and add your IPadresse in whitelist
  our account : 	tradeandwear2019@gmail.com
************************************************************************************/

var process_args = process.argv.slice(2);


var express = require('express');
var cors = require('cors'),
  app = express(),
  port = 4300,
  mongoose = require('mongoose'),

  User = require('./models/Users'),
  Item = require('./models/Items'), //created model loading here
  Network = require('./models/RecommendationNetworks'),
  AiFeed = require('./models/Aifeed'),
  bodyParser = require('body-parser');


// mongoose instance connection url connection
mongoose.Promise = global.Promise;
switch (process_args[0]) {
  case '--local':
    mongoose.connect(MONGO_CONNECTION_STRING_LOCAL, { useNewUrlParser: true });
    break;
  default:
    mongoose.connect(MONGO_CONNECTION_STRING_DEVELOP, { useNewUrlParser: true, dbName: 'TradeAndWearDB' });
    break;
}

mongoose.connection.on('connected', function(){
  console.log('Connected to DB');
});
mongoose.connection.on('error', function(){
  console.log('Error Connection to DB');
});
mongoose.connection.on('disconnected', function(){
  console.log('Disconnection DB');
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({limit: '50mb', type: 'application/json'}));
app.use(cors());

//importing routes
var routesUser = require('./routes/userRoutes');
var routesItem = require('./routes/itemRoutes');
var routesRecommendationEngine = require('./routes/recommendationNetworkRoutes');
var routesAiFeed = require('./routes/aiFeedRoutes');


//register the routes
routesUser(app); 
routesItem(app);
routesRecommendationEngine(app);
routesAiFeed(app);


app.listen(port, '0.0.0.0');

console.log('TradeAndWear RESTful API server started on: ' + port);

var mongoose = require('mongoose')
  , Schema = mongoose.Schema

var FeedSchema = Schema({
    userId: {type: Schema.Types.ObjectId, ref: 'Users'},
    idItem: [{type: Schema.Types.ObjectId, ref: 'Items'}],
  }, {collection: 'Feed'});

  module.exports = mongoose.model('Feed', FeedSchema);
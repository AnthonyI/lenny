var mongoose = require('mongoose')
  , Schema = mongoose.Schema

var ItemCategorySchema = Schema({
    category:
    {
      type:String,
      enum: ['Shoe', 'Tshirt',]
    },
    color:
    {
      type:String,
      enum: ['White', 'Red', 'Blue',]
    }
  });

  module.exports = mongoose.model('ItemCategory', ItemCategorySchema);
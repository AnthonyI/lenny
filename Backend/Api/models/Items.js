var mongoose = require('mongoose')
  , Schema = mongoose.Schema

var ItemsSchema = Schema({
  userId: { type: String, ref: 'Users' },
  title: { type: String, required: false },
  price: { type: String, required: false },
  quantity: { type: Number, required: false },
  description: { type: String },
  type: { type: String, required: false },
  style: { type: String, required: false },
  characteristic: { type: Object, required: false },
  photos: [String],
  status: { type: String, required: false },
  createdDate: { type: Date, default: Date.now() },
  deleted: { type: Boolean, required: false },
}, { collection: 'Items' });

module.exports = mongoose.model('Items', ItemsSchema);
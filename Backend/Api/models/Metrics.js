var mongoose = require('mongoose')
  , Schema = mongoose.Schema

var MetricsSchema = Schema({
  sessionId: { type: Schema.Types.ObjectId, ref: 'ActiveSession' },
  userId:    {type: Schema.Types.ObjectId, ref: 'Users'},
  idItem: [{type: Schema.Types.ObjectId, ref: 'Items'}],
  dateTime:  { type: Date }
  }, {collection: 'Metrics'});

module.exports = mongoose.model('Metrics', MetricsSchema);
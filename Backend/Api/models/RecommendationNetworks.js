'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var NetworkSchema = Schema({
  userId: { type: Object, required: true },
  network: { type: Object, required: true },
  createdDate: { type: Date, default: Date.now() }
}, { collection: 'RecommendationNetworks' });

module.exports = mongoose.model('RecommendationNetworks', NetworkSchema);
var mongoose = require('mongoose')
  , Schema = mongoose.Schema

var ActiveSessionSchema = Schema({
  userId: { type: Schema.Types.ObjectId, ref: 'Users', unique: true},
  start : {type: Date},
  end : Date,
  Active : {type : Boolean, default: 0},
  NextIndex : {type: Number, default: 0},
  PrevIndex : {type: Number, default: 0},
  FeedBuffer: [{type: Schema.Types.ObjectId, ref: 'Items'}],
  PrFeedBuffer: [{type: Schema.Types.ObjectId, ref: 'Items'}]
  }, {collection: 'ActiveSessions'});

module.exports = mongoose.model('ActiveSession', ActiveSessionSchema);
'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var AiFeedSchema = Schema({
  userId: {type: String, required: true},
  mainFeed: [{type: Schema.Types.ObjectId, required: true, ref: 'Items'}],
}, {collection: 'AiFeed'});

module.exports = mongoose.model('AiFeed', AiFeedSchema);
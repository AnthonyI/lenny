'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var UserSchema = Schema({
  userId: { type: String, required: true },
  name: { type: String, required: false },
  surname: { type: String, required: false },
  email: { type: String, required: false },  
  sexe: { type: String, required: false },
  birthDate: { type: Date, required: false },
  methodConnection: { type: String, required: false  },
  createdDate: { type: Date, default: Date.now() },
  inboardingDone: { type: Boolean, required: false  },
  profile: { type: Object, required: false  }
}, { collection: 'Users' });

module.exports = mongoose.model('Users', UserSchema);
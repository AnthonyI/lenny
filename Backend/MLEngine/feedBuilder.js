const MONGO_CONNECTION_STRING_LOCAL = 'mongodb://localhost/TradeAndWearDB';
const MONGO_CONNECTION_STRING_DEVELOP = 'mongodb+srv://akram_TW:' + process.env.TW_MONGO_SECRET + '@clustertw-ioece.gcp.mongodb.net/TradeAndWearDB?retryWrites=true';

var process_args = process.argv.slice(2);

const request = require('then-request');
const fs = require('fs');
const mongoose = require('mongoose');
const brain = require('brain.js');

var User = require('./../Api/models/Users');

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
switch (process_args[0]) {
  case '--local':
    mongoose.connect(MONGO_CONNECTION_STRING_LOCAL, { useNewUrlParser: true });
    break;
  default:
    mongoose.connect(MONGO_CONNECTION_STRING_DEVELOP, { useNewUrlParser: true, dbName: 'TradeAndWearDB' });
    break;
}


/*************************************************************************************************************
Determine items most relevant to each user and build the feeds.

We determine a feed by listing the itemIds of the most relevant items for a user, and storing this information.
*************************************************************************************************************/
console.log("Fetching the neural networks from remote database...")
var start = new Date().getTime();
request('GET', 'http://34.70.8.24:8080/networks/get_all_networks').getBody('utf8').then(JSON.parse).done(function (get_all_networks_response) {

    request('GET', 'http://34.70.8.24:8080/items/get_all_items').getBody('utf8').then(JSON.parse).done(function (get_all_items_response) {
        
        var elapsed = new Date().getTime() - start;
        console.log("Fetching took " + elapsed + " milliseconds to complete.")
        console.log("The neural networks have been fetched, beginning feed building...")

        for (var i = 0; i < get_all_networks_response.length; i++){

            BuildFeed(get_all_networks_response[i], get_all_items_response);            
        }
    });
});

setTimeout((function() {  
    return process.exit(22);
}), 10000);

function BuildFeed(network, items){
    var mainFeedScores = [];
    const net = new brain.NeuralNetwork();
    net.fromJSON(JSON.parse(network.network));

    for (var j = 0; j < items.length; j++){
        if (items[j].userId != network.userId){

            var score = net.run({[items[j].style]: 1});
            mainFeedScores.push({ id: [items[j]._id], score: [score] });

            //console.log("Item " + items[j].title + " has affinity of " + score + " with user " +  network.userId);
        }
    }
    mainFeedScores.sort((a, b) => (a.score > b.score) ? -1 : 1);
    var mainFeed = [];
    for (var idx = 0; idx < mainFeedScores.length; idx++){
        mainFeed.push(mainFeedScores[idx].id);
    }
    request('GET', 'http://34.70.8.24:8080/ai_feed/get_feed/' + network.userId).getBody('utf8').then(JSON.parse).done(function (get_feed_response) {
        if (get_feed_response != null && get_feed_response.mainFeed != null){
            request('PUT', 'http://34.70.8.24:8080/ai_feed/update_feed', {json: {mainFeed: mainFeed, userId: network.userId}}).getBody('utf8').then(JSON.parse).done(function (update_feed_response) {
            });
        }
        else{
            request('POST', 'http://34.70.8.24:8080/ai_feed/add_feed', {json: {mainFeed: mainFeed, userId: network.userId}}).getBody('utf8').then(JSON.parse).done(function (add_feed_response) {
            });
        }
    });
}
const MONGO_CONNECTION_STRING_LOCAL = 'mongodb://localhost/TradeAndWearDB';
const MONGO_CONNECTION_STRING_DEVELOP = 'mongodb+srv://akram_TW:' + process.env.TW_MONGO_SECRET + '@clustertw-ioece.gcp.mongodb.net/TradeAndWearDB?retryWrites=true';

var process_args = process.argv.slice(2);

const request = require('then-request');
const fs = require('fs');
const mongoose = require('mongoose');
const brain = require('brain.js');

var User = require('./../Api/models/Users');

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
switch (process_args[0]) {
  case '--local':
    mongoose.connect(MONGO_CONNECTION_STRING_LOCAL, { useNewUrlParser: true });
    break;
  default:
    mongoose.connect(MONGO_CONNECTION_STRING_DEVELOP, { useNewUrlParser: true, dbName: 'TradeAndWearDB' });
    break;
}

/*************************************************************************************************************
First time neural network building for every user.

This will have to change in future updates. We want this job to become modular in order to allow a network
rebuild or a network update for a particular user. For now, this will be run a single time and train networks
for each and every user.
*************************************************************************************************************/
console.log("Starting recommendation networks training...");
var start = new Date().getTime();
request('GET', 'http://34.70.8.24:8080/users/get_all_users').getBody('utf8').then(JSON.parse).done(function (get_all_users_response) {

    for (var i = 0; i < get_all_users_response.length; i++){

        BuildNetwork(get_all_users_response[i]);
    }
    var elapsed = new Date().getTime() - start;
    console.log("Training lasted for " + elapsed + " milliseconds...");
});

setTimeout((function() {  
  return process.exit(22);
}), 10000);

function BuildNetwork(user){
  const style = user.profile.style;
        const net = new brain.NeuralNetwork();
    
        const trainingData = [
            {input: {Tendance: 1}, output: [style.includes("Tendance") ? 1 : 0]},
            {input: {Streetwear: 1}, output: [style.includes("Streetwear") ? 1 : 0]},
            {input: {Classy: 1}, output: [style.includes("Classy") ? 1 : 0]},
            {input: {Casual: 1}, output: [style.includes("Casual") ? 1 : 0]},
            {input: {Vintage: 1}, output: [style.includes("Vintage") ? 1 : 0]},
            {input: {Sporty: 1}, output: [style.includes("Sporty") ? 1 : 0]},
            {input: {Rock: 1}, output: [style.includes("Rock") ? 1 : 0]},
            {input: {Punk: 1}, output: [style.includes("Punk") ? 1 : 0]},
        ];
    
        net.train(trainingData);
        var jsonNet = JSON.stringify(net.toJSON());
        request('GET', 'http://34.70.8.24:8080/networks/get_network/' + user.userId).getBody('utf8').then(JSON.parse).done(function (get_network_response) {
          if (get_network_response != null && get_network_response.network != null){
            request('PUT', 'http://34.70.8.24:8080/networks/update_network', {json: {network: jsonNet, userId: get_network_response.userId}}).getBody('utf8').then(JSON.parse).done(function (update_network_response) {
            });
          }
          else{
            request('POST', 'http://34.70.8.24:8080/networks/add_network', {json: {network: jsonNet, userId: user.userId}}).getBody('utf8').then(JSON.parse).done(function (add_network_response) {
            });
          }
        });
}
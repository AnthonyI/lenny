import app from "./app";
const PORT = 8080;

app.listen(PORT, () => {
  // tslint:disable-next-line:no-console
  console.log("listening on port " + PORT);
});

/************************************************************************************
  The server can be started by running the following command in any terminal:
  npm start
  You must have specified the database connection string in the TW_MONGO_SECRET environment variable.

  If you wish to debug using a local database, use the command "npm start local"

  Important: for remote database, go to DB cloud and add your IPadresse in whitelist
  our account : 	tradeandwear2019@gmail.com
************************************************************************************/

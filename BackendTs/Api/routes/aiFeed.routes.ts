import { AiFeedController } from "../controllers/aiFeed.controller";

export class AiFeedRoutes {
    public aiFeedController: AiFeedController = new AiFeedController();

    public routes(app: any): void {

        app.route("/ai_feed/add_feed").post(this.aiFeedController.add_feed);

        app.route("/ai_feed/get_feed/:userId").get(this.aiFeedController.get_feed);

        app.route("/ai_feed/update_feed").put(this.aiFeedController.update_feed);

        app.route("/ai_feed/delete_feed/:userId").delete(this.aiFeedController.delete_feed);
    }
}

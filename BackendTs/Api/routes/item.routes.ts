import { ItemController } from "../controllers/item.controller";

export class ItemRoutes {
    public itemController: ItemController = new ItemController();

    public routes(app: any): void {

        app.route("/items/get_all_items").get(this.itemController.get_all_items);

        app.route("/items/add_item").post(this.itemController.add_item);

        app.route("/items/get_item/:_id").get(this.itemController.get_item);

        app.route("/items/update_item").put(this.itemController.update_item);

        app.route("/items/delete_item/:_id").delete(this.itemController.delete_item);

        app.route("/items/get_my_items/:userId").get(this.itemController.get_my_items);

        app.route("/items/add_photo").post(this.itemController.add_photo);

        app.route("/items/get_photo/:fileName").get(this.itemController.get_photo);

        app.route("/items/delete_photo/:fileName").delete(this.itemController.delete_photo);

        app.route("/items/make_first/:fileName").put(this.itemController.make_first);

        app.route("/items/search_item/:title").get(this.itemController.search_item);
    }
}

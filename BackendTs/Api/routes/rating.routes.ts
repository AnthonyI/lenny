import { RatingController } from "../controllers/rating.controller";

export class RatingRoutes {
    public ratingController: RatingController = new RatingController();

    public routes(app: any): void {

        app.route("/ratings/get_all_ratings").get(this.ratingController.get_all_ratings);

        app.route("/ratings/add_rating").post(this.ratingController.add_rating);

        app.route("/ratings/get_rating/:userId").get(this.ratingController.get_rating);

        app.route("/ratings/delete_rating/:userId").delete(this.ratingController.delete_rating);
    }
}

import { RecommendationNetworkController } from "../controllers/recommendationNetwork.controller";

export class RecommendationNetworkRoutes {
    public RecommendationNetworkController: RecommendationNetworkController = new RecommendationNetworkController();

    public routes(app: any): void {

        app.route("/networks/get_all_networks").get(this.RecommendationNetworkController.get_all_networks);

        app.route("/networks/add_network").post(this.RecommendationNetworkController.add_network);

        app.route("/networks/get_network/:userId").get(this.RecommendationNetworkController.get_network);

        app.route("/networks/update_network").put(this.RecommendationNetworkController.update_network);

        app.route("/networks/delete_network/:userId").delete(this.RecommendationNetworkController.delete_network);
    }
}

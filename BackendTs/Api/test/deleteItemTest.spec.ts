import { expect } from "chai";
import * as fs from "fs";
import "mocha";
import request from "request";

const uri = "http://localhost:8080";

const user = '{"userId":"88888","name":"PhotoTest","surname":"surname1"}';
const userJson = JSON.parse(user);

const item = '{"userId":"88888","title":"PhotoTest"}';
const itemJson = JSON.parse(item);

const dir = "./Api/photos/itemsPhotos/";

let id = "";
let fileName1 = "";
let fileName2 = "";
let fileName3 = "";
let fileName4 = "";
let fileName5 = "";

// for skip test: xit

describe("Delete item tests", () => {

    it("Should post the new user", (done) => {

        request.post(
            {
                form: userJson,
                url: `${uri}/users/add_user`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.userId).to.equal(userJson.userId);
                expect(bodyJson.name).to.equal(userJson.name);
                expect(bodyJson.surname).to.equal(userJson.surname);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should post the new item", (done) => {

        request.post(
            {
                form: itemJson,
                url: `${uri}/items/add_item`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                id = bodyJson._id;
                expect(bodyJson.userId).to.equal(itemJson.userId);
                expect(bodyJson.title).to.equal(itemJson.title);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should add photo1", (done) => {

        const photo = '{"photo":"PhotoTest photo1 in base64"}';
        const photoJson = JSON.parse(photo);
        photoJson._id = id;

        request.post(
            {
                form: photoJson,
                url: `${uri}/items/add_photo`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.fileName.length).to.equal(52);

                expect(bodyJson.fileName.substring(0, 27)).to.equal(id + "_20");
                expect(bodyJson.fileName.substring(49, 52)).to.equal(".tw");

                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should add photo2", (done) => {

        const photo = '{"photo":"PhotoTest photo2 in base64"}';
        const photoJson = JSON.parse(photo);
        photoJson._id = id;

        request.post(
            {
                form: photoJson,
                url: `${uri}/items/add_photo`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.fileName.length).to.equal(52);

                expect(bodyJson.fileName.substring(0, 27)).to.equal(id + "_20");
                expect(bodyJson.fileName.substring(49, 52)).to.equal(".tw");

                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should add photo3", (done) => {

        const photo = '{"photo":"PhotoTest photo3 in base64"}';
        const photoJson = JSON.parse(photo);
        photoJson._id = id;

        request.post(
            {
                form: photoJson,
                url: `${uri}/items/add_photo`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.fileName.length).to.equal(52);

                expect(bodyJson.fileName.substring(0, 27)).to.equal(id + "_20");
                expect(bodyJson.fileName.substring(49, 52)).to.equal(".tw");

                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should add photo4", (done) => {

        const photo = '{"photo":"PhotoTest photo4 in base64"}';
        const photoJson = JSON.parse(photo);
        photoJson._id = id;

        request.post(
            {
                form: photoJson,
                url: `${uri}/items/add_photo`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.fileName.length).to.equal(52);

                expect(bodyJson.fileName.substring(0, 27)).to.equal(id + "_20");
                expect(bodyJson.fileName.substring(49, 52)).to.equal(".tw");

                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should add photo5", (done) => {

        const photo = '{"photo":"PhotoTest photo5 in base64"}';
        const photoJson = JSON.parse(photo);
        photoJson._id = id;

        request.post(
            {
                form: photoJson,
                url: `${uri}/items/add_photo`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.fileName.length).to.equal(52);

                expect(bodyJson.fileName.substring(0, 27)).to.equal(id + "_20");
                expect(bodyJson.fileName.substring(49, 52)).to.equal(".tw");

                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should have 5 files in the folder "itemsPhotos"', (done) => {

        fs.readdir(dir, (err, files) => {
            expect(files.length).to.equal(5);
            done();
        });
    });

    it("Should get item by _id", (done) => {
        request.get(
            {
                url: `${uri}/items/get_item/${id}`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                fileName1 = bodyJson.photos[0];
                fileName2 = bodyJson.photos[1];
                fileName3 = bodyJson.photos[2];
                fileName4 = bodyJson.photos[3];
                fileName5 = bodyJson.photos[4];
                expect(bodyJson._id).to.equal(id);
                expect(bodyJson.userId).to.equal(itemJson.userId);
                expect(bodyJson.title).to.equal(itemJson.title);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should find file1 in folder the "itemsPhotos"', (done) => {

        fs.exists(dir + fileName1, (exists) => {
            expect(exists).to.equal(true);
            done();
        });
    });

    it('Should find file2 in folder the "itemsPhotos"', (done) => {

        fs.exists(dir + fileName2, (exists) => {
            expect(exists).to.equal(true);
            done();
        });
    });

    it('Should find file3 in folder the "itemsPhotos"', (done) => {

        fs.exists(dir + fileName3, (exists) => {
            expect(exists).to.equal(true);
            done();
        });
    });

    it('Should find file4 in folder the "itemsPhotos"', (done) => {

        fs.exists(dir + fileName4, (exists) => {
            expect(exists).to.equal(true);
            done();
        });
    });

    it('Should find file5 in folder the "itemsPhotos"', (done) => {

        fs.exists(dir + fileName5, (exists) => {
            expect(exists).to.equal(true);
            done();
        });
    });

    it("Should delete item", (done) => {

        request.delete(
            {
                url: `${uri}/items/delete_item/${id}`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("Item successfully deleted");
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should not find deleted items", (done) => {
        request.get(
            {
                url: `${uri}/items/get_item/${id}`,
            }
            , (error, response, body) => {
                expect(body).to.equal("null");
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should not have any files in the folder "itemsPhotos"', (done) => {

        // setTimeout because it must wait when 5 files will be deleted
        setTimeout(() => {
            fs.readdir(dir, (err, files) => {
                expect(files.length).to.equal(0);
                done();
            });
        }, 100);
    });

    it('Should not find file1 in folder the "itemsPhotos"', (done) => {
        // setTimeout because it must wait when 5 files will be deleted
        setTimeout(() => {
            fs.exists(dir + fileName1, (exists) => {
                expect(exists).to.equal(false);
                done();
            });
        }, 100);
    });

    it('Should not find file2 in folder the "itemsPhotos"', (done) => {

        fs.exists(dir + fileName2, (exists) => {
            expect(exists).to.equal(false);
            done();
        });
    });

    it('Should not find file3 in folder the "itemsPhotos"', (done) => {

        fs.exists(dir + fileName3, (exists) => {
            expect(exists).to.equal(false);
            done();
        });
    });

    it('Should not find file4 in folder the "itemsPhotos"', (done) => {

        fs.exists(dir + fileName4, (exists) => {
            expect(exists).to.equal(false);
            done();
        });
    });

    it('Should not find file5 in folder the "itemsPhotos"', (done) => {

        fs.exists(dir + fileName5, (exists) => {
            expect(exists).to.equal(false);
            done();
        });
    });

    it("Should delete user", (done) => {

        request.delete(
            {
                url: `${uri}/users/delete_user/${userJson.userId}`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("User successfully deleted");
                expect(response.statusCode).to.equal(200);
                done();
            });
    });
});

import { expect } from "chai";
import * as fs from "fs";
import "mocha";
import request from "request";

interface IUser {
    userId: string;
    name: string;
    surname: string;
    inboardingDone: boolean;
    birthDate: string;
    profile: IProfile;
}
interface IProfile {
    color: string;
    category: string;
}

const uri = "http://localhost:8080/users";
const dir = "./Api/photos/usersPhotos/11111";

describe("Users tests", () => {

    it("Should not post the new user without userId", (done) => {

        const user = '{"name":"name1","surname":"surname1"}';
        const userJson = JSON.parse(user);

        /* const result = 5;
            expect(result).equal(5); */
        request.post(
            {
                form: userJson,
                url: `${uri}/add_user`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("User must have userId!");
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    xit("Should not post the new user due to birthDate date format", (done) => {

        const user = '{"userId":"11111","name":"name1","surname":"surname1","birthDate":"13/13/2019"}';
        const userJson = JSON.parse(user);

        request.post(
            {
                form: userJson,
                url: `${uri}/add_user`,
            }
            , (error, response, body) => {
                // var bodyJson = JSON.parse(body);
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it("Should post the new user", (done) => {

        const user = '{"userId":"11111","name":"name1","surname":"surname1"}';
        const userJson = JSON.parse(user);

        request.post(
            {
                form: userJson,
                url: `${uri}/add_user`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.userId).to.equal(userJson.userId);
                expect(bodyJson.name).to.equal(userJson.name);
                expect(bodyJson.surname).to.equal(userJson.surname);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should return Bad Request the new user exists", (done) => {

        const user = '{"userId":"11111","name":"name1","surname":"surname1"}';
        const userJson = JSON.parse(user);

        request.post(
            {
                form: userJson,
                url: `${uri}/add_user`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("User with the given userId already exists");
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it("Should get user by userId", (done) => {

        const user = '{"userId":"11111","name":"name1","surname":"surname1"}';
        const userJson = JSON.parse(user);

        request.get(
            {
                url: `${uri}/get_user/${userJson.userId}`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.userId).to.equal(userJson.userId);
                expect(bodyJson.name).to.equal(userJson.name);
                expect(bodyJson.surname).to.equal(userJson.surname);
                expect(bodyJson.photo).to.equal(undefined);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should return Bad Request '404 Not Found' parameter is absent in get_user", (done) => {

        request.get(
            {
                url: `${uri}/get_user/`,
            }
            , (error, response, body) => {
                expect(response.statusCode).to.equal(404);
                done();
            });
    });

    it('Should return Bad Request "404 Not Found" parameter is absent in get_photo', (done) => {

        request.get(
            {
                url: `${uri}/get_photo/`,
            }
            , (error, response, body) => {
                expect(response.statusCode).to.equal(404);
                done();
            });
    });

    it('Should return Bad Request "400" "No such file or directory!" in get_photo', (done) => {

        const user = '{"userId":"00000"}';
        const userJson = JSON.parse(user);

        request.get(
            {
                url: `${uri}/get_photo/${userJson.userId}`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("No such file = " + userJson.userId + " or directory!");
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it("Should update user", (done) => {

        const user = '{"userId":"11111","name":"name1-1","surname":"surname1-1","inboardingDone":true,"birthDate":"12/12/2019","profile":{"color": "red","category": "category1"}}';
        const userJson = JSON.parse(user);

        request.put(
            {
                form: userJson,
                url: `${uri}/update_user`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.userId).to.equal(userJson.userId);
                expect(bodyJson.name).to.equal(userJson.name);
                expect(bodyJson.surname).to.equal(userJson.surname);
                expect(bodyJson.inboardingDone).to.equal(userJson.inboardingDone);
                // expect(body).to.equal(userJson);
                // expect(bodyJson.profile.color).to.equal(userJson.profile.color);
                // expect(bodyJson.profile.category).to.equal(userJson.profile.category);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should update photo in "users/update_photo"', (done) => {

        const user = '{"userId":"11111","photo":"Photo2 in base64"}';
        const userJson = JSON.parse(user);

        request.put(
            {
                form: userJson,
                url: `${uri}/update_photo`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("Profile picture updated");
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should not update photo in "users/update_photo the user does not exist"', (done) => {

        const user = '{"userId":"11112","photo":"Photo2 in base64"}';
        const userJson = JSON.parse(user);

        request.put(
            {
                form: userJson,
                url: `${uri}/update_photo`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("User with userId=11112 does not exist");
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it("Should get users updated photo", (done) => {

        const user = '{"userId":"11111"}';
        const userJson = JSON.parse(user);

        request.get(
            {
                url: `${uri}/get_photo/${userJson.userId}`,
            }
            , (error, response, body) => {
                expect(body).to.equal("Photo2 in base64");
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should not update user due to birthDate date format", (done) => {

        const user = '{"userId":"11111","birthDate":"13/13/2019"}';
        const userJson = JSON.parse(user);

        request.put(
            {
                form: userJson,
                url: `${uri}/update_user`,
            }
            , (error, response, body) => {
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it("Should not update user due to inboardingDone format", (done) => {

        const user = '{"userId":"11111", "inboardingDone":"a"}';
        const userJson = JSON.parse(user);

        request.put(
            {
                form: userJson,
                url: `${uri}/update_user`,
            }
            , (error, response, body) => {
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should return "400 Bad Request" and message "UserId is required for update user"', (done) => {

        const user = '{"name":"name1","surname":"surname1"}';
        const userJson = JSON.parse(user);

        request.put(
            {
                form: userJson,
                url: `${uri}/update_user`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("UserId is required for update user");
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should return "400 Bad Request" and message "User with userId=00000 does not exist"', (done) => {

        const user = '{"userId":"00000","name":"name1","surname":"surname1"}';
        const userJson = JSON.parse(user);

        request.put(
            {
                form: userJson,
                url: `${uri}/update_user`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("User with userId=" + userJson.userId + " does not exist");
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should return Bad Request "404 Not Found" parameter is absent', (done) => {

        request.get(
            {
                url: `${uri}/delete_user/`,
            }
            , (error, response, body) => {
                expect(response.statusCode).to.equal(404);
                done();
            });
    });

    it('Should have file 11111 in the folder "usersPhotos"', (done) => {

        fs.exists(dir, (exists) => {
            expect(exists).to.equal(true);
            done();
        });
    });

    it("Should delete user by userId", (done) => {

        const user = '{"userId":"11111","name":"name1","surname":"surname1"}';
        const userJson = JSON.parse(user);

        request.delete(
            {
                url: `${uri}/delete_user/${userJson.userId}`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("User successfully deleted");
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should not find deleted user", (done) => {

        const user = '{"userId":"11111"}';
        const userJson = JSON.parse(user);

        request.get(
            {
                url: `${uri}/get_user/${userJson.userId}`,
            }
            , (error, response, body) => {
                expect(body).to.equal("null");
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should not have file 11111 in the folder "usersPhotos"', (done) => {

        fs.exists(dir, (exists) => {
            expect(exists).to.equal(false);
            done();
        });
    });

});

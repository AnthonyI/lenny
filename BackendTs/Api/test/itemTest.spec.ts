import { expect } from "chai";
import "mocha";
import request from "request";

const uri = "http://localhost:8080";

const user = '{"userId":"77777","name":"ItemTest","surname":"surname1"}';
const userJson = JSON.parse(user);

const user2 = '{"userId":"77776","name":"ItemTest","surname":"surname1"}';
const user2Json = JSON.parse(user2);

// item with userId wich does not exist
const item1 = '{"userId":"11111","type":"Shirt","title":"title1-1"}';
const item1Json = JSON.parse(item1);

// item with error in quantity
const item2 = '{"userId":"77777","type":"Shoes","title":"title1-1","quantity":"a"}';
const item2Json = JSON.parse(item2);

const item3 = '{"userId":"77777","type":"Shoes","title":"title3","quantity":1}';
const item3Json = JSON.parse(item3);
let id3 = "";

const item4 = '{"userId":"77777","type":"Shoes","title":"title4","quantity":1}';
const item4Json = JSON.parse(item4);
let id4 = "";

const item5 = '{"userId":"77776","type":"Shoes","title":"title5","quantity":1}';
const item5Json = JSON.parse(item5);
let id5 = "";

const item6 = '{"userId":"77776","type":"Shoes","title":"title6","quantity":1}';
const item6Json = JSON.parse(item6);
let id6 = "";

// for skip test: xit

describe("Items tests", () => {

    before(() => {
        request.post(
            {
                form: userJson,
                url: `${uri}/users/add_user`,
            },
        );

        request.post(
            {
                form: user2Json,
                url: `${uri}/users/add_user`,
            },
        );
    });

    after(() => {
        request.delete(
            {
                url: `${uri}/users/delete_user/${userJson.userId}`,
            },
        );

        request.delete(
            {
                url: `${uri}/users/delete_user/${user2Json.userId}`,
            },
        );
    });

    it("Should not post the new item if user does not exist", (done) => {
        request.post(
            {
                form: item1Json,
                url: `${uri}/items/add_item`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("User with the given userId does not exist");
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should not post the new item if "quantity" is not a number', (done) => {
        request.post(
            {
                form: item2Json,
                url: `${uri}/items/add_item`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it("Should get all (0) items", (done) => {
        request.get(
            {
                url: `${uri}/items/get_all_items`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.length).to.equal(0);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should post the new item if user exists", (done) => {
        request.post(
            {
                form: item3Json,
                url: `${uri}/items/add_item`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                id3 = bodyJson._id;
                expect(bodyJson.userId).to.equal(item3Json.userId);
                expect(bodyJson.type).to.equal(item3Json.type);
                expect(bodyJson.title).to.equal(item3Json.title);
                expect(bodyJson.quantity).to.equal(item3Json.quantity);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should get item by _id", (done) => {
        request.get(
            {
                url: `${uri}/items/get_item/${id3}`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson._id).to.equal(id3);
                expect(bodyJson.userId).to.equal(item3Json.userId);
                expect(bodyJson.type).to.equal(item3Json.type);
                expect(bodyJson.title).to.equal(item3Json.title);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should return Bad Request "400" "CastError" ObjectId format is incorrect in get_item', (done) => {
        request.get(
            {
                url: `${uri}/items/get_item/01234567890123456789012`,
            }
            , (error, response, body) => {
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should return Bad Request "404 Not Found" parameter is absent', (done) => {
        request.get(
            {
                url: `${uri}/items/get_item/`,
            }
            , (error, response, body) => {
                expect(response.statusCode).to.equal(404);
                done();
            });
    });

    it('Should not update item "_id is required for update item"', (done) => {

        item3Json.quantity = 5;

        request.put(
            {
                form: item3Json,
                url: `${uri}/items/update_item`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("_id is required for update item");
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it("Should update item", (done) => {
        item3Json._id = id3;
        item3Json.type = "Shirt";
        item3Json.quantity = 3;
        item3Json.deleted = "true";
        request.put(
            {
                form: item3Json,
                url: `${uri}/items/update_item`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson._id).to.equal(id3);
                expect(bodyJson.userId).to.equal(item3Json.userId);
                expect(bodyJson.type).to.equal(item3Json.type);
                expect(bodyJson.title).to.equal(item3Json.title);
                expect(bodyJson.quantity).to.equal(item3Json.quantity);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should not update item due to "quantity" data format', (done) => {

        item3Json._id = id3;
        item3Json.quantity = "a";

        request.put(
            {
                form: item3Json,
                url: `${uri}/items/update_item`,
            }
            , (error, response, body) => {
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should not update item due to "deleted" data format', (done) => {

        item3Json._id = id3;
        item3Json.quantity = 5;
        item3Json.deleted = "tr";

        request.put(
            {
                form: item3Json,
                url: `${uri}/items/update_item`,
            }
            , (error, response, body) => {
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should not update item due to "Item with _id=  does not exist"', (done) => {

        item3Json._id = "012345678901234567890123";
        item3Json.quantity = 5;
        item3Json.deleted = "true";

        request.put(
            {
                form: item3Json,
                url: `${uri}/items/update_item`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("Item with _id=012345678901234567890123 does not exist");
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it('Should return Bad Request "400" "CastError" ObjectId format is incorrect in update_item', (done) => {

        item3Json._id = "01234567890123456789012";
        item3Json.quantity = 5;
        item3Json.deleted = "true";

        request.put(
            {
                form: item3Json,
                url: `${uri}/items/update_item`,
            }
            , (error, response, body) => {
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it("Should post second item", (done) => {
        request.post(
            {
                form: item4Json,
                url: `${uri}/items/add_item`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                id4 = bodyJson._id;
                expect(bodyJson.userId).to.equal(item4Json.userId);
                expect(bodyJson.type).to.equal(item4Json.type);
                expect(bodyJson.title).to.equal(item4Json.title);
                expect(bodyJson.quantity).to.equal(item4Json.quantity);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should get all (2) items by userId", (done) => {
        request.get(
            {
                url: `${uri}/items/get_my_items/${userJson.userId}`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.length).to.equal(2);
                expect(bodyJson[0]._id).to.equal(id3);
                expect(bodyJson[1]._id).to.equal(id4);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should get all (2) items", (done) => {
        request.get(
            {
                url: `${uri}/items/get_all_items`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.length).to.equal(2);
                expect(bodyJson[0]._id).to.equal(id3);
                expect(bodyJson[1]._id).to.equal(id4);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should post the new item for second user", (done) => {
        request.post(
            {
                form: item5Json,
                url: `${uri}/items/add_item`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                id5 = bodyJson._id;
                expect(bodyJson.userId).to.equal(item5Json.userId);
                expect(bodyJson.type).to.equal(item5Json.type);
                expect(bodyJson.title).to.equal(item5Json.title);
                expect(bodyJson.quantity).to.equal(item5Json.quantity);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should get all (3) items", (done) => {
        request.get(
            {
                url: `${uri}/items/get_all_items`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.length).to.equal(3);
                expect(bodyJson[0]._id).to.equal(id3);
                expect(bodyJson[1]._id).to.equal(id4);
                expect(bodyJson[2]._id).to.equal(id5);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should post the new item for second user", (done) => {
        request.post(
            {
                form: item6Json,
                url: `${uri}/items/add_item`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                id6 = bodyJson._id;
                expect(bodyJson.userId).to.equal(item6Json.userId);
                expect(bodyJson.type).to.equal(item6Json.type);
                expect(bodyJson.title).to.equal(item6Json.title);
                expect(bodyJson.quantity).to.equal(item6Json.quantity);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should get all (4) items", (done) => {
        request.get(
            {
                url: `${uri}/items/get_all_items`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.length).to.equal(4);
                expect(bodyJson[0]._id).to.equal(id3);
                expect(bodyJson[1]._id).to.equal(id4);
                expect(bodyJson[2]._id).to.equal(id5);
                expect(bodyJson[3]._id).to.equal(id6);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it('Should return Bad Request "404 Not Found" parameter is absent', (done) => {
        request.delete(
            {
                url: `${uri}/items/delete_item/`,
            }
            , (error, response, body) => {
                expect(response.statusCode).to.equal(404);
                done();
            });
    });

    it('Should return Bad Request "400" "CastError" ObjectId format is incorrect in delete_item', (done) => {
        request.delete(
            {
                url: `${uri}/items/delete_item/01234567890123456789012`,
            }
            , (error, response, body) => {
                expect(response.statusCode).to.equal(400);
                done();
            });
    });

    it("Should delete first item", (done) => {
        request.delete(
            {
                url: `${uri}/items/delete_item/${id3}`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("Item successfully deleted");
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should get all (1) items by userId", (done) => {
        request.get(
            {
                url: `${uri}/items/get_my_items/${userJson.userId}`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.length).to.equal(1);
                expect(bodyJson[0]._id).to.equal(id4);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should delete second item", (done) => {
        request.delete(
            {
                url: `${uri}/items/delete_item/${id4}`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("Item successfully deleted");
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should not find deleted items", (done) => {
        request.get(
            {
                url: `${uri}/items/get_my_items/${userJson.userId}`,
            }
            , (error, response, body) => {
                expect(body).to.equal("[]");
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should delete first item from second user", (done) => {
        request.delete(
            {
                url: `${uri}/items/delete_item/${id5}`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("Item successfully deleted");
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should delete second item from second user", (done) => {
        request.delete(
            {
                url: `${uri}/items/delete_item/${id6}`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("Item successfully deleted");
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

    it("Should get all (0) items", (done) => {
        request.get(
            {
                url: `${uri}/items/get_all_items`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.length).to.equal(0);
                expect(response.statusCode).to.equal(200);
                done();
            });
    });

});

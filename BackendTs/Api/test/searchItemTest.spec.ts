/* import { expect } from "chai";
import "mocha";
import request from "request";

const uri = "http://localhost:8080";

const user = '{"userId":"77777","name":"SearchItemTest1","surname":"surname1"}';
const userJson = JSON.parse(user);

const user2 = '{"userId":"77776","name":"SearchItemTest2","surname":"surname2"}';
const user2Json = JSON.parse(user2);

const user3 = '{"userId":"77775","name":"SearchItemTest3","surname":"surname3"}';
const user3Json = JSON.parse(user3);

const item11 = '{"userId":"77777","type":"Shirt","title":"title1"}';
const item11Json = JSON.parse(item11);

const item12 = '{"userId":"77777","type":"Shirt","title":"title2"}';
const item12Json = JSON.parse(item12);

const item13 = '{"userId":"77777","type":"Shirt","title":"title3"}';
const item13Json = JSON.parse(item13);

// for skip test: xit

describe("Search items tests", () => {

    before(() => {
        request.post(
            {
                form: userJson,
                url: `${uri}/users/add_user`,
            },
        );

        request.post(
            {
                form: user2Json,
                url: `${uri}/users/add_user`,
            },
        );

        request.post(
            {
                form: user3Json,
                url: `${uri}/users/add_user`,
            },
        );
    });

    after(() => {
        request.delete(
            {
                url: `${uri}/users/delete_user/${userJson.userId}`,
            },
        );

        request.delete(
            {
                url: `${uri}/users/delete_user/${user2Json.userId}`,
            },
        );

        request.delete(
            {
                url: `${uri}/users/delete_user/${user3Json.userId}`,
            },
        );
    });

     it("Should not post the new item if user does not exist", (done) => {
        request.post(
            {
                form: item1Json,
                url: `${uri}/items/add_item`,
            }
            , (error, response, body) => {
                const bodyJson = JSON.parse(body);
                expect(bodyJson.message).to.equal("User with the given userId does not exist");
                expect(response.statusCode).to.equal(400);
                done();
            });
    });
}); */

import * as bodyParser from "body-parser";
import cors = require("cors");
import express = require("express");
import mongoose = require("mongoose");
import { AiFeedRoutes} from "./routes/aiFeed.routes";
import { ItemRoutes } from "./routes/item.routes";
import { RatingRoutes} from "./routes/rating.routes";
import { RecommendationNetworkRoutes } from "./routes/recommendationNetwork.routes";
import { UserRoutes } from "./routes/user.routes";

class App {
  public app: express.Application;
  private userRoute: UserRoutes = new UserRoutes();
  private ratingRoute: RatingRoutes = new RatingRoutes();
  private RecommendationNetworkRoutes: RecommendationNetworkRoutes = new RecommendationNetworkRoutes();
  private itemRoute: ItemRoutes = new ItemRoutes();
  private aiFeedRoute: AiFeedRoutes = new AiFeedRoutes();
  private MONGO_CONNECTION_STRING_LOCAL: string = "mongodb://localhost/TradeAndWearDB";
  private MONGO_CONNECTION_STRING_DEVELOP: string = "mongodb+srv://akram_TW:" + process.env.TW_MONGO_SECRET + "@clustertw-ioece.gcp.mongodb.net/TradeAndWearDB?retryWrites=true";

  constructor() {
    this.app = express();
    this.config();
    this.userRoute.routes(this.app);
    this.ratingRoute.routes(this.app);
    this.RecommendationNetworkRoutes.routes(this.app);
    this.itemRoute.routes(this.app);
    this.aiFeedRoute.routes(this.app);
    this.mongoSetup();
  }

  private config(): void {
    this.app.use(bodyParser.json({limit: "50mb"}));
    this.app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit: 1000000}));

    this.app.use(cors());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    // serving static files
    this.app.use(express.static("public"));
  }

  private mongoSetup(): void {
    mongoose.Promise = global.Promise;

    switch (process.argv[2]) {
      case "local":
        mongoose.connect(this.MONGO_CONNECTION_STRING_LOCAL, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
        }).then(() => {
          // tslint:disable-next-line:no-console
          console.log("connection to LOCAL database established");
        });
        break;
      default:
        mongoose.connect(this.MONGO_CONNECTION_STRING_DEVELOP, {
          dbName: "TradeAndWearDB",
          useNewUrlParser: true,
          useUnifiedTopology: true,
        }).then(() => {
          // tslint:disable-next-line:no-console
          console.log("connection to SERVER database established");
        });
        break;
    }

    mongoose.set("useFindAndModify", false);
  }
}

export default new App().app;

import * as mongoose from "mongoose";
const Schema = mongoose.Schema;

export const RatingSchema = new Schema({
    comment: { type: String, required: false },
    createdDate: { type: Date, default: Date.now },
    rating: {
        max: [5, "5 is maximum for the rating!"],
        min: [0, "0 is minimum for the rating!"],
        required: true,
        type: Number,
    },
    userId: { type: String, ref: "Users", required: true },
    userRated: { type: String, ref: "Users", required: true },
}, { collection: "Ratings" });

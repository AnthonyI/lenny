import * as mongoose from "mongoose";
const Schema = mongoose.Schema;

export const ItemsSchema = new Schema({
    characteristic: { type: Object, required: false },
    createdDate: { type: Date, default: Date.now },
    deleted: { type: Boolean, required: false },
    description: { type: String, required: false },
    photos: [String],
    price: { type: Number, required: false },
    quantity: { type: Number, required: false },
    status: { type: String, required: false },
    style: { type: String, required: false },
    title: { type: String, required: false },
    type: { type: String, required: false },
    userId: { type: String, ref: "Users" },
}, { collection: "Items" });

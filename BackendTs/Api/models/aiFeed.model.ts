import * as mongoose from "mongoose";
const Schema = mongoose.Schema;

export const AiFeedSchema = new Schema({
    mainFeed: [{type: Schema.Types.ObjectId, required: true, ref: "Items"}],
    userId: {type: String, required: true},
}, {collection: "AiFeed"});

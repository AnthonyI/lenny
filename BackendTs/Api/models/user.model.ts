import * as mongoose from "mongoose";

const Schema = mongoose.Schema;

export const UserSchema = new Schema({
    birthDate: { type: Date, required: false },
    chatUserIds: [String],
    createdDate: { type: Date, default: Date.now},
    email: { type: String, required: false },
    inboardingDone: { type: Boolean, required: false },
    methodConnection: { type: String, required: false },
    name: { type: String, required: false },
    profile: { type: Object, required: false },
    sexe: { type: String, required: false },
    surname: { type: String, required: false },
    userId: { type: String, required: true },
}, { collection: "Users" });

import * as mongoose from "mongoose";
const Schema = mongoose.Schema;

export const NetworkSchema = new Schema({
    createdDate: { type: Date, default: Date.now },
    network: { type: Object, required: true },
    userId: { type: String, required: true },
  }, { collection: "RecommendationNetworks" });

"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = __importStar(require("mongoose"));
const Schema = mongoose.Schema;
exports.RatingSchema = new Schema({
    comment: { type: String, required: false },
    createdDate: { type: Date, default: Date.now },
    rating: {
        max: [5, "5 is maximum for the rating!"],
        min: [0, "0 is minimum for the rating!"],
        required: true,
        type: Number,
    },
    userId: { type: String, ref: "Users", required: true },
    userRated: { type: String, ref: "Users", required: true },
}, { collection: "Ratings" });
//# sourceMappingURL=rating.model.js.map
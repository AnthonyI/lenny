"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = __importStar(require("mongoose"));
const Schema = mongoose.Schema;
exports.UserSchema = new Schema({
    birthDate: { type: Date, required: false },
    chatUserIds: [String],
    createdDate: { type: Date, default: Date.now },
    email: { type: String, required: false },
    inboardingDone: { type: Boolean, required: false },
    methodConnection: { type: String, required: false },
    name: { type: String, required: false },
    profile: { type: Object, required: false },
    sexe: { type: String, required: false },
    surname: { type: String, required: false },
    userId: { type: String, required: true },
}, { collection: "Users" });
//# sourceMappingURL=user.model.js.map
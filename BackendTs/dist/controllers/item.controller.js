"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs"));
const mongoose = __importStar(require("mongoose"));
const item_model_1 = require("../models/item.model");
const user_model_1 = require("../models/user.model");
const Item = mongoose.model("Item", item_model_1.ItemsSchema);
const User = mongoose.model("User", user_model_1.UserSchema);
const pathPhoto = "./Api/photos/itemsPhotos/";
class ItemController {
    get_all_items(req, res) {
        // tslint:disable-next-line:no-console
        console.log("get_all_items");
        Item.find({}, (err, items) => {
            if (!err) {
                res.status(200);
                res.json(items);
                res.end();
            }
            else {
                res.status(400);
                res.send(err);
                res.end();
                // tslint:disable-next-line:no-console
                console.log("get_all_items error " + err);
            }
        });
    }
    add_item(req, res) {
        // tslint:disable-next-line:no-console
        console.log("add_item");
        // tslint:disable-next-line:no-console
        console.log(req.body);
        User.findOne({ userId: req.body.userId }, (err, user) => {
            if (err) {
                res.status(400);
                res.send(err);
                res.end();
                // tslint:disable-next-line:no-console
                console.log("add_item error findOne " + err);
            }
            else if (user != null) {
                const newItem = new Item(req.body);
                // newItem.createdDate = Date.now();
                newItem.save((err1, item) => {
                    if (!err1) {
                        res.status(200);
                        res.json(item);
                        res.end();
                    }
                    else {
                        res.status(400);
                        res.send(err1);
                        res.end();
                        // tslint:disable-next-line:no-console
                        console.log("add_item error save " + err1);
                    }
                });
            }
            else {
                res.status(400);
                res.json({ message: "User with the given userId does not exist" });
                res.end();
                // tslint:disable-next-line:no-console
                console.log("User with the given userId does not exist");
            }
        });
    }
    get_item(req, res) {
        // tslint:disable-next-line:no-console
        console.log("get_item " + req.params._id);
        Item.findOne({ _id: req.params._id }, { __v: 0 }, (err, item) => {
            if (!err) {
                res.status(200);
                res.json(item);
                res.end();
            }
            else {
                res.status(400);
                res.send(err);
                res.end();
                // tslint:disable-next-line:no-console
                console.log("get_item error " + err);
            }
        });
    }
    update_item(req, res) {
        // tslint:disable-next-line:no-console
        console.log("update_item " + req.body._id);
        if (req.body._id) {
            Item.findOneAndUpdate({ _id: req.body._id }, req.body, { new: true }, (err, item) => {
                if (err) {
                    res.status(400);
                    res.send(err);
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("update_item error findOneAndUpdate" + err);
                }
                else if (item == null) {
                    res.status(400);
                    res.json({ message: "Item with _id=" + req.body._id + " does not exist" });
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("Item with _id=" + req.body._id + " does not exist");
                }
                else {
                    res.status(200);
                    res.json(item);
                    res.end();
                }
            });
        }
        else {
            res.status(400);
            res.json({ message: "_id is required for update item" });
            res.end();
            // tslint:disable-next-line:no-console
            console.log("_id is required for update item");
        }
    }
    delete_item(req, res) {
        // tslint:disable-next-line:no-console
        console.log("delete_item " + req.params._id);
        let fileName = "";
        let count = 0;
        if (req.params._id) {
            Item.findOne({ _id: req.params._id }, { __v: 0 }, (err, item) => {
                if (!err) {
                    if (item != null) {
                        count = item.photos.length;
                        for (let i = 0; i < count; i++) {
                            fileName = item.photos[i];
                            fs.unlink(pathPhoto + fileName, (err1) => {
                                if (err1) {
                                    // tslint:disable-next-line:no-console
                                    console.log("delete_item error unlink " + err1);
                                }
                            });
                        }
                    }
                }
            });
            Item.deleteOne({ _id: req.params._id }, (err) => {
                if (!err) {
                    res.status(200);
                    res.json({ message: "Item successfully deleted" });
                    res.end();
                }
                else {
                    res.status(400);
                    res.send(err);
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("delete_item error unlink " + err);
                }
            });
        }
        else {
            res.status(400);
            res.end();
        }
    }
    get_my_items(req, res) {
        // tslint:disable-next-line:no-console
        console.log("get_my_items " + req.params.userId);
        Item.find({ userId: req.params.userId }, { __v: 0 }, (err, items) => {
            if (!err) {
                res.status(200);
                res.json(items);
                res.end();
            }
            else {
                res.status(400);
                res.send(err);
                res.end();
                // tslint:disable-next-line:no-console
                console.log("get_my_items error " + err);
            }
        });
    }
    add_photo(req, res) {
        // tslint:disable-next-line:no-console
        console.log("add_photo " + req.body._id);
        let fileName = req.body._id;
        const photo = req.body.photo;
        const lastFileName = new Date();
        Item.findOne({ _id: req.body._id }, { __v: 0 }, (err, item) => {
            if (err) {
                res.status(400);
                res.send(err);
                res.end();
                // tslint:disable-next-line:no-console
                console.log("add_photo error findOne " + err);
            }
            else {
                if (item != null) {
                    if (item.photos.length < 5) {
                        // Perform a global replacement ":" to "-"
                        fileName += "_" + lastFileName.toISOString().replace(/:/g, "-") + ".tw";
                        fs.writeFile(pathPhoto + fileName, photo, (err1) => {
                            if (!err1) {
                                item.photos.push(fileName);
                                Item.findOneAndUpdate({ _id: req.body._id }, item, { new: true }, (err2) => {
                                    if (!err2) {
                                        res.status(200);
                                        // tslint:disable-next-line:object-literal-shorthand
                                        res.json({ fileName: fileName });
                                        res.end();
                                    }
                                    else {
                                        res.status(400);
                                        res.send(err2);
                                        res.end();
                                        // tslint:disable-next-line:no-console
                                        console.log("add_photo error findOneAndUpdate " + err2);
                                    }
                                });
                            }
                            else {
                                res.status(400);
                                res.send(err1);
                                res.end();
                                // tslint:disable-next-line:no-console
                                console.log("add_photo error writeFile " + err1);
                            }
                        });
                    }
                    else {
                        res.status(400);
                        res.json({ message: "The maximum number of photos is 5" });
                        res.end();
                        // tslint:disable-next-line:no-console
                        console.log("The maximum number of photos is 5");
                    }
                }
                else {
                    res.status(400);
                    res.json({ message: "Item with the given _id does not exist" });
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("Item with the given _id does not exist");
                }
            }
        });
    }
    get_photo(req, res) {
        // tslint:disable-next-line:no-console
        console.log("get_photo " + req.params.fileName);
        const fileName = req.params.fileName;
        fs.readFile(pathPhoto + fileName, (err, data) => {
            if (!err) {
                res.status(200);
                res.send(data);
                res.end();
            }
            else {
                res.status(400);
                res.json({ message: "No such file = " + fileName + " or directory!" });
                res.end();
                // tslint:disable-next-line:no-console
                console.log("No such file = " + fileName + " or directory!");
            }
        });
    }
    delete_photo(req, res) {
        // tslint:disable-next-line:no-console
        console.log("delete_photo " + req.params.fileName);
        const fileName = req.params.fileName;
        let length = 0;
        let fileExist = false;
        const id = fileName.substring(0, 24);
        Item.findOne({ _id: id }, { __v: 0 }, (err, item) => {
            if (!err) {
                if (item != null) {
                    length = item.photos.length;
                    for (let i = 0; i < length; i++) {
                        if (item.photos[i] === fileName) {
                            // to remove item i from array
                            item.photos.splice(i, 1);
                            fileExist = true;
                        }
                    }
                }
                if (fileExist) {
                    Item.findOneAndUpdate({ _id: id }, item, { new: true }, (err1) => {
                        if (!err1) {
                            fs.unlink(pathPhoto + fileName, (err2) => {
                                if (!err2) {
                                    res.status(200);
                                    res.json({ message: "Photo " + fileName + " deleted!" });
                                    res.end();
                                }
                                else {
                                    res.status(400);
                                    res.send(err2);
                                    res.end();
                                    // tslint:disable-next-line:no-console
                                    console.log("delete_photo error unlink " + err2);
                                }
                            });
                        }
                        else {
                            res.status(400);
                            res.send(err1);
                            res.end();
                            // tslint:disable-next-line:no-console
                            console.log("delete_photo error findOneAndUpdate " + err1);
                        }
                    });
                }
                else {
                    res.status(400);
                    res.json({ message: "No such file = " + fileName + " or directory!" });
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("No such file = " + fileName + " or directory!");
                }
            }
            else {
                res.status(400);
                res.send(err);
                res.end();
                // tslint:disable-next-line:no-console
                console.log("delete_photo error findOne " + err);
            }
        });
    }
    make_first(req, res) {
        // tslint:disable-next-line:no-console
        console.log("make_first " + req.params.fileName);
        const fileName = req.params.fileName;
        let length = 0;
        let fileExist = false;
        const id = fileName.substring(0, 24);
        Item.findOne({ _id: id }, { __v: 0 }, (err, item) => {
            if (!err) {
                if (item != null) {
                    length = item.photos.length;
                    for (let i = 0; i < length; i++) {
                        if (item.photos[i] === fileName) {
                            // to remove item i from array
                            item.photos.splice(i, 1);
                            fileExist = true;
                        }
                    }
                }
                if (fileExist) {
                    // The unshift() method adds new items to the beginning of an array
                    item.photos.unshift(fileName);
                    Item.findOneAndUpdate({ _id: id }, item, { new: true }, (err1, item1) => {
                        if (!err1) {
                            res.status(200);
                            res.json(item1);
                            res.end();
                        }
                        else {
                            res.status(400);
                            res.send(err1);
                            res.end();
                            // tslint:disable-next-line:no-console
                            console.log("make_first error findOneAndUpdate " + err1);
                        }
                    });
                }
                else {
                    res.status(400);
                    res.json({ message: "No such file = " + fileName + " or directory!" });
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("No such file = " + fileName + " or directory!");
                }
            }
            else {
                res.status(400);
                res.send(err);
                res.end();
                // tslint:disable-next-line:no-console
                console.log("make_first error findOne " + err);
            }
        });
    }
    search_item(req, res) {
        // tslint:disable-next-line:no-console
        console.log("search_item " + req.params.title);
        Item.find({ title: { $regex: ".*" + req.params.title + "*." } }, { __v: 0 }, (err, item) => {
            if (!err) {
                res.status(200);
                res.json(item);
                res.end();
            }
            else {
                res.status(400);
                res.send(err);
                res.end();
                // tslint:disable-next-line:no-console
                console.log("search_item error " + err);
            }
        });
    }
}
exports.ItemController = ItemController;
//# sourceMappingURL=item.controller.js.map
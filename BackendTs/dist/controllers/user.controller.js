"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs"));
const mongoose = __importStar(require("mongoose"));
const user_model_1 = require("../models/user.model");
const User = mongoose.model("User", user_model_1.UserSchema);
const pathPhoto = "./Api/photos/usersPhotos/";
class UserController {
    get_all_users(req, res) {
        // tslint:disable-next-line:no-console
        console.log("get_all_users");
        User.find({}, (err, users) => {
            if (!err) {
                res.status(200);
                res.json(users);
                res.end();
            }
            else {
                res.status(400);
                res.send(err);
                res.end();
                // tslint:disable-next-line:no-console
                console.log("get_all_users error " + err);
            }
        });
    }
    add_user(req, res) {
        // tslint:disable-next-line:no-console
        console.log("add_user " + req.body.userId);
        if (req.body.userId) {
            User.findOne({ userId: req.body.userId }, (err, user) => {
                if (err) {
                    res.status(400);
                    res.send(err);
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("add_user error findOne " + err);
                }
                else if (user == null) {
                    const newUser = new User(req.body);
                    // newUser.createdDate = Date.now();
                    newUser.save((err1, user1) => {
                        if (!err) {
                            res.status(200);
                            res.json(user1);
                            res.end();
                            // tslint:disable-next-line:no-console
                            console.log("add_user save " + user1);
                        }
                        else {
                            res.status(400);
                            res.send(err1);
                            res.end();
                            // tslint:disable-next-line:no-console
                            console.log("add_user error save " + err1);
                        }
                    });
                }
                else {
                    res.status(400);
                    res.json({ message: "User with the given userId already exists" });
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("User with the given userId already exists");
                }
            });
        }
        else {
            res.status(400);
            res.json({ message: "User must have userId!" });
            res.end();
            // tslint:disable-next-line:no-console
            console.log("User must have userId!");
        }
    }
    get_user(req, res) {
        // tslint:disable-next-line:no-console
        console.log("get_user " + req.params.userId);
        User.findOne({ userId: req.params.userId }, { _id: 0, __v: 0, photo: 0 }, (err, user) => {
            if (!err) {
                res.status(200);
                res.json(user);
                res.end();
            }
            else {
                res.status(400);
                res.send(err);
                res.end();
                // tslint:disable-next-line:no-console
                console.log("get_user error findOne " + err);
            }
        });
    }
    update_user(req, res) {
        // tslint:disable-next-line:no-console
        console.log("update_user " + req.body.userId);
        if (req.body.userId) {
            User.findOneAndUpdate({ userId: req.body.userId }, req.body, { new: true }, (err, user) => {
                if (err) {
                    res.status(400);
                    res.send(err);
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("update_user error findOneAndUpdate " + err);
                }
                else if (user == null) {
                    res.status(400);
                    res.json({ message: "User with userId=" + req.body.userId + " does not exist" });
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("User with userId=" + req.body.userId + " does not exist");
                }
                else {
                    res.status(200);
                    res.json(user);
                    res.end();
                }
            });
        }
        else {
            res.status(400);
            res.json({ message: "UserId is required for update user" });
            res.end();
            // tslint:disable-next-line:no-console
            console.log("UserId is required for update user");
        }
    }
    delete_user(req, res) {
        // tslint:disable-next-line:no-console
        console.log("delete_user " + req.params.userId);
        if (req.params.userId) {
            User.deleteOne({ userId: req.params.userId }, (err) => {
                if (!err) {
                    const fileName = req.params.userId;
                    fs.exists(pathPhoto + fileName, (exists) => {
                        if (exists) {
                            fs.unlink(pathPhoto + fileName, (err1) => {
                                if (!err1) {
                                    res.status(200);
                                    res.json({ message: "User successfully deleted" });
                                    res.end();
                                }
                                else {
                                    res.status(400);
                                    res.send(err1);
                                    res.end();
                                    // tslint:disable-next-line:no-console
                                    console.log("delete_user error unlink " + err1);
                                }
                            });
                        }
                        else {
                            res.status(200);
                            res.json({ message: "User successfully deleted" });
                            res.end();
                        }
                    });
                }
                else {
                    res.status(400);
                    res.send(err);
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("delete_user error deleteOne " + err);
                }
            });
        }
        else {
            res.status(400);
            res.json({ message: "UserId is required for delete user" });
            res.end();
            // tslint:disable-next-line:no-console
            console.log("UserId is required for delete user");
        }
    }
    update_photo(req, res) {
        const fileName = req.body.userId;
        const photo = req.body.photo;
        if (req.body.userId) {
            User.findOne({ userId: req.body.userId }, { _id: 0, __v: 0 }, (err, user) => {
                if (err) {
                    res.status(400);
                    res.send(err);
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("update_photo error findOne " + err);
                }
                else if (user == null) {
                    res.status(400);
                    res.json({ message: "User with userId=" + req.body.userId + " does not exist" });
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("User with userId=" + req.body.userId + " does not exist");
                }
                else {
                    fs.writeFile(pathPhoto + fileName, photo, (err2) => {
                        if (!err2) {
                            res.status(200);
                            res.json({ message: "Profile picture updated" });
                            res.end();
                            // tslint:disable-next-line:no-console
                            console.log("Profile picture updated");
                        }
                        else {
                            res.status(400);
                            res.send(err2);
                            res.end();
                            // tslint:disable-next-line:no-console
                            console.log("update_photo error writeFile " + err2);
                        }
                    });
                }
            });
        }
        else {
            res.status(400);
            res.json({ message: "UserId is required for update user" });
            res.end();
            // tslint:disable-next-line:no-console
            console.log("UserId is required for update user");
        }
    }
    get_photo(req, res) {
        // tslint:disable-next-line:no-console
        console.log("get_photo " + req.params.userId);
        const fileName = req.params.userId;
        fs.readFile(pathPhoto + fileName, (err, data) => {
            if (!err) {
                res.status(200);
                res.send(data);
                res.end();
            }
            else {
                res.status(400);
                res.json({ message: "No such file = " + fileName + " or directory!" });
                res.end();
                // tslint:disable-next-line:no-console
                console.log("No such file = " + fileName + " or directory!");
            }
        });
    }
}
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map
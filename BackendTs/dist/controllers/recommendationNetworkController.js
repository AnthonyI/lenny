"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = __importStar(require("mongoose"));
const RecommendationNetworks_1 = require("../models/RecommendationNetworks");
const Users_1 = require("../models/Users");
const Network = mongoose.model("RecommendationNetworks", RecommendationNetworks_1.NetworkSchema);
const User = mongoose.model("User", Users_1.UserSchema);
class RecommendationNetworkController {
    get_all_networks(req, res) {
        Network.find({}, (err, networks) => {
            if (err) {
                res.send(err);
            }
            res.json(networks);
            res.end();
            // tslint:disable-next-line:no-console
            console.log("get_all_networks");
        });
    }
    add_network(req, res) {
        // tslint:disable-next-line:no-console
        console.log("add_network " + req.body.userId);
        if (req.body.userId) {
            if (req.body.network) {
                User.findOne({ userId: req.body.userId }, (err, user) => {
                    if (user != null) {
                        Network.findOne({ userId: req.body.userId }, (err1, network) => {
                            if (network == null) {
                                const newNetwork = new Network(req.body);
                                newNetwork.save((err2, network1) => {
                                    if (!err2) {
                                        res.status(200);
                                        res.json(network1);
                                        res.end();
                                        // tslint:disable-next-line:no-console
                                        console.log("add_network");
                                    }
                                    else {
                                        res.status(400);
                                        res.send(err2);
                                        res.end();
                                        // tslint:disable-next-line:no-console
                                        console.log("add_network error save " + err2);
                                    }
                                });
                            }
                            else {
                                res.status(400);
                                res.json({ message: "Network with the given userId already exists" });
                                res.end();
                                // tslint:disable-next-line:no-console
                                console.log("Network with the given userId already exists");
                            }
                        });
                    }
                    else {
                        res.status(400);
                        res.json({ message: "User with the given userId does not exist" });
                        res.end();
                        // tslint:disable-next-line:no-console
                        console.log("User with the given userId does not exist");
                    }
                });
            }
            else {
                res.status(400);
                res.json({ message: "Network is required for add network" });
                res.end();
                // tslint:disable-next-line:no-console
                console.log("Network is required for add network");
            }
        }
        else {
            res.status(400);
            res.json({ message: "UserId is required for add network" });
            res.end();
            // tslint:disable-next-line:no-console
            console.log("UserId is required for add network");
        }
    }
    get_network(req, res) {
        // tslint:disable-next-line:no-console
        console.log("get_network " + req.params.userId);
        Network.findOne({ userId: req.params.userId }, { __v: 0 }, (err, network) => {
            if (err) {
                res.send(err);
                res.end();
            }
            else {
                res.json(network);
                res.end();
            }
        });
    }
    update_network(req, res) {
        // tslint:disable-next-line:no-console
        console.log("update_network " + req.body.userId);
        if (req.body.userId) {
            Network.findOneAndUpdate({ userId: req.body.userId }, req.body, { new: true }, (err, network) => {
                if (err) {
                    res.status(400);
                    res.send(err);
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("update_network error findOneAndUpdate " + err);
                }
                else if (network == null) {
                    res.status(400);
                    res.json({ message: "Network with userId=" + req.body.userId + " does not exist" });
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("Network with userId=" + req.body.userId + " does not exist");
                }
                else {
                    res.json(network);
                    res.end();
                }
            });
        }
        else {
            res.status(400);
            res.json({ message: "userId is required for update_network" });
            res.end();
            // tslint:disable-next-line:no-console
            console.log("userId is required for update_network");
        }
    }
    delete_network(req, res) {
        // tslint:disable-next-line:no-console
        console.log("delete_network " + req.params.userId);
        if (req.params.userId) {
            Network.deleteOne({ userId: req.params.userId }, (err) => {
                if (!err) {
                    res.json({ message: "Network successfully deleted" });
                    res.end();
                }
                else {
                    res.status(400);
                    res.send(err);
                    res.end();
                }
            });
        }
        else {
            res.status(400);
            res.end();
        }
    }
}
exports.RecommendationNetworkController = RecommendationNetworkController;
//# sourceMappingURL=recommendationNetworkController.js.map
"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = __importStar(require("mongoose"));
const rating_model_1 = require("../models/rating.model");
const user_model_1 = require("../models/user.model");
const Rating = mongoose.model("Rating", rating_model_1.RatingSchema);
const User = mongoose.model("User", user_model_1.UserSchema);
class RatingController {
    get_all_ratings(req, res) {
        // tslint:disable-next-line:no-console
        console.log("get_all_ratings");
        Rating.find({}, (err, ratings) => {
            if (!err) {
                res.status(200);
                res.json(ratings);
                res.end();
            }
            else {
                res.status(400);
                res.send(err);
                res.end();
                // tslint:disable-next-line:no-console
                console.log("get_all_ratings error " + err);
            }
        });
    }
    add_rating(req, res) {
        // tslint:disable-next-line:no-console
        console.log("add_rating");
        // tslint:disable-next-line:no-console
        console.log(req.body);
        if (req.body.userId) {
            if (req.body.userRated) {
                User.findOne({ userId: req.body.userId }, (err, user) => {
                    if (err) {
                        res.status(400);
                        res.send(err);
                        res.end();
                        // tslint:disable-next-line:no-console
                        console.log("add_rating error findOne " + err);
                    }
                    else if (user != null) {
                        User.findOne({ userId: req.body.userRated }, (err2, userRated) => {
                            if (err2) {
                                res.status(400);
                                res.send(err2);
                                res.end();
                                // tslint:disable-next-line:no-console
                                console.log("add_rating error findOne " + err2);
                            }
                            else if (userRated != null) {
                                const newRating = new Rating(req.body);
                                newRating.save((err1, rating) => {
                                    if (!err1) {
                                        res.status(200);
                                        res.json(rating);
                                        res.end();
                                    }
                                    else {
                                        res.status(400);
                                        res.send(err1);
                                        res.send();
                                        // tslint:disable-next-line:no-console
                                        console.log("add_rating error save " + err1);
                                    }
                                });
                            }
                            else {
                                res.status(400);
                                res.json({ message: "User with the given userRated does nor exist" });
                                res.end();
                                // tslint:disable-next-line:no-console
                                console.log("User with the given userRated does not exist");
                            }
                        });
                    }
                    else {
                        res.status(400);
                        res.json({ message: "User with the given userId does not exist" });
                        res.end();
                        // tslint:disable-next-line:no-console
                        console.log("User with the given userId does not exist");
                    }
                });
            }
            else {
                res.status(400);
                res.json({ message: "userRated is absent!" });
                res.end();
                // tslint:disable-next-line:no-console
                console.log("userRated is absent!");
            }
        }
        else {
            res.status(400);
            res.json({ message: "userId is absent!" });
            res.end();
            // tslint:disable-next-line:no-console
            console.log("userId is absent!");
        }
    }
    get_rating(req, res) {
        // tslint:disable-next-line:no-console
        console.log("get_rating " + req.params.userId);
        Rating.aggregate([
            {
                $match: {
                    userId: req.params.userId,
                },
            },
            {
                $group: {
                    _id: null,
                    averageRating: {
                        $avg: "$rating",
                    },
                },
            },
            {
                $project: {
                    _id: 0,
                },
            },
        ], (err, rating) => {
            if (!err) {
                res.status(200);
                res.json(rating);
                res.end();
            }
            else {
                res.status(400);
                res.send(err);
                res.end();
                // tslint:disable-next-line:no-console
                console.log("get_rating error " + err);
            }
        });
    }
    delete_rating(req, res) {
        // tslint:disable-next-line:no-console
        console.log("delete_rating " + req.params.userId);
        if (req.params.userId) {
            Rating.deleteMany({ userId: req.params.userId }, (err) => {
                if (!err) {
                    res.status(200);
                    res.json({ message: "All ratings successfully deleted" });
                    res.end();
                }
                else {
                    res.status(400);
                    res.send(err);
                    res.end();
                }
            });
        }
        else {
            res.status(400);
            res.json({ message: "UserId is required for delete rating" });
            res.end();
            // tslint:disable-next-line:no-console
            console.log("UserId is required for delete rating");
        }
    }
}
exports.RatingController = RatingController;
//# sourceMappingURL=rating.controller.js.map
"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = __importStar(require("mongoose"));
const aiFeed_model_1 = require("../models/aiFeed.model");
const user_model_1 = require("../models/user.model");
const AiFeed = mongoose.model("AiFeed", aiFeed_model_1.AiFeedSchema);
const User = mongoose.model("User", user_model_1.UserSchema);
class AiFeedController {
    get_feed(req, res) {
        // tslint:disable-next-line:no-console
        console.log("get_feed " + req.params._id);
        AiFeed.findOne({ userId: req.params.userId }, { __v: 0 }, (err, feed) => {
            if (!err) {
                res.status(200);
                res.json(feed);
                res.end();
            }
            else {
                res.status(400);
                res.send(err);
                res.end();
                // tslint:disable-next-line:no-console
                console.log("get_feed error " + err);
            }
        });
    }
    add_feed(req, res) {
        // tslint:disable-next-line:no-console
        console.log("add_feed");
        // tslint:disable-next-line:no-console
        console.log(req.body);
        User.findOne({ userId: req.body.userId }, (err, user) => {
            if (err) {
                res.status(400);
                res.send(err);
                res.end();
                // tslint:disable-next-line:no-console
                console.log("add_feed error findOne " + err);
            }
            else if (user != null) {
                const newFeed = new AiFeed(req.body);
                newFeed.save((err1, feed) => {
                    if (!err1) {
                        res.status(200);
                        res.json(feed);
                        res.end();
                    }
                    else {
                        res.status(400);
                        res.send(err1);
                        res.end();
                        // tslint:disable-next-line:no-console
                        console.log("add_feed error save " + err1);
                    }
                });
            }
            else {
                res.status(400);
                res.json({ message: "User with the given userId does not exist" });
                res.end();
                // tslint:disable-next-line:no-console
                console.log("User with the given userId does not exist");
            }
        });
    }
    update_feed(req, res) {
        // tslint:disable-next-line:no-console
        console.log("update_feed " + req.body.userId);
        if (req.body.userId) {
            AiFeed.findOneAndUpdate({ userId: req.body.userId }, req.body, { new: true }, (err, feed) => {
                if (err) {
                    res.status(400);
                    res.send(err);
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("update_feed error findOneAndUpdate" + err);
                }
                else if (feed == null) {
                    res.status(400);
                    res.json({ message: "Feed with userId=" + req.body.userId + " does not exist" });
                    res.end();
                    // tslint:disable-next-line:no-console
                    console.log("Feed with userId=" + req.body.userId + " does not exist");
                }
                else {
                    res.status(200);
                    res.json(feed);
                    res.end();
                }
            });
        }
        else {
            res.status(400);
            res.json({ message: "userId is required for update_feed" });
            res.end();
            // tslint:disable-next-line:no-console
            console.log("userId is required for update_feed");
        }
    }
    delete_feed(req, res) {
        // tslint:disable-next-line:no-console
        console.log("delete_feed " + req.params._id);
        if (req.params.userId) {
            AiFeed.deleteOne({ _id: req.params._id }, (err) => {
                if (!err) {
                    res.json({ message: "Feed successfully deleted" });
                    res.end();
                }
                else {
                    res.status(400);
                    res.send(err);
                    res.end();
                }
            });
        }
        else {
            res.status(400);
            res.end();
        }
    }
}
exports.AiFeedController = AiFeedController;
//# sourceMappingURL=aiFeed.controller.js.map
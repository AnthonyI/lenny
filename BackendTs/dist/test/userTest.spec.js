"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const fs = __importStar(require("fs"));
require("mocha");
const request_1 = __importDefault(require("request"));
const uri = "http://localhost:8080/users";
const dir = "./Api/photos/usersPhotos/11111";
describe("Users tests", () => {
    it("Should not post the new user without userId", (done) => {
        const user = '{"name":"name1","surname":"surname1"}';
        const userJson = JSON.parse(user);
        /* const result = 5;
            expect(result).equal(5); */
        request_1.default.post({
            form: userJson,
            url: `${uri}/add_user`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("User must have userId!");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    xit("Should not post the new user due to birthDate date format", (done) => {
        const user = '{"userId":"11111","name":"name1","surname":"surname1","birthDate":"13/13/2019"}';
        const userJson = JSON.parse(user);
        request_1.default.post({
            form: userJson,
            url: `${uri}/add_user`,
        }, (error, response, body) => {
            // var bodyJson = JSON.parse(body);
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it("Should post the new user", (done) => {
        const user = '{"userId":"11111","name":"name1","surname":"surname1"}';
        const userJson = JSON.parse(user);
        request_1.default.post({
            form: userJson,
            url: `${uri}/add_user`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.userId).to.equal(userJson.userId);
            chai_1.expect(bodyJson.name).to.equal(userJson.name);
            chai_1.expect(bodyJson.surname).to.equal(userJson.surname);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should return Bad Request the new user exists", (done) => {
        const user = '{"userId":"11111","name":"name1","surname":"surname1"}';
        const userJson = JSON.parse(user);
        request_1.default.post({
            form: userJson,
            url: `${uri}/add_user`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("User with the given userId already exists");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it("Should get user by userId", (done) => {
        const user = '{"userId":"11111","name":"name1","surname":"surname1"}';
        const userJson = JSON.parse(user);
        request_1.default.get({
            url: `${uri}/get_user/${userJson.userId}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.userId).to.equal(userJson.userId);
            chai_1.expect(bodyJson.name).to.equal(userJson.name);
            chai_1.expect(bodyJson.surname).to.equal(userJson.surname);
            chai_1.expect(bodyJson.photo).to.equal(undefined);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should return Bad Request '404 Not Found' parameter is absent in get_user", (done) => {
        request_1.default.get({
            url: `${uri}/get_user/`,
        }, (error, response, body) => {
            chai_1.expect(response.statusCode).to.equal(404);
            done();
        });
    });
    it('Should return Bad Request "404 Not Found" parameter is absent in get_photo', (done) => {
        request_1.default.get({
            url: `${uri}/get_photo/`,
        }, (error, response, body) => {
            chai_1.expect(response.statusCode).to.equal(404);
            done();
        });
    });
    it('Should return Bad Request "400" "No such file or directory!" in get_photo', (done) => {
        const user = '{"userId":"00000"}';
        const userJson = JSON.parse(user);
        request_1.default.get({
            url: `${uri}/get_photo/${userJson.userId}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("No such file = " + userJson.userId + " or directory!");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it("Should update user", (done) => {
        const user = '{"userId":"11111","name":"name1-1","surname":"surname1-1","inboardingDone":true,"birthDate":"12/12/2019","profile":{"color": "red","category": "category1"}}';
        const userJson = JSON.parse(user);
        request_1.default.put({
            form: userJson,
            url: `${uri}/update_user`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.userId).to.equal(userJson.userId);
            chai_1.expect(bodyJson.name).to.equal(userJson.name);
            chai_1.expect(bodyJson.surname).to.equal(userJson.surname);
            chai_1.expect(bodyJson.inboardingDone).to.equal(userJson.inboardingDone);
            // expect(body).to.equal(userJson);
            // expect(bodyJson.profile.color).to.equal(userJson.profile.color);
            // expect(bodyJson.profile.category).to.equal(userJson.profile.category);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it('Should update photo in "users/update_photo"', (done) => {
        const user = '{"userId":"11111","photo":"Photo2 in base64"}';
        const userJson = JSON.parse(user);
        request_1.default.put({
            form: userJson,
            url: `${uri}/update_photo`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("Profile picture updated");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it('Should not update photo in "users/update_photo the user does not exist"', (done) => {
        const user = '{"userId":"11112","photo":"Photo2 in base64"}';
        const userJson = JSON.parse(user);
        request_1.default.put({
            form: userJson,
            url: `${uri}/update_photo`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("User with userId=11112 does not exist");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it("Should get users updated photo", (done) => {
        const user = '{"userId":"11111"}';
        const userJson = JSON.parse(user);
        request_1.default.get({
            url: `${uri}/get_photo/${userJson.userId}`,
        }, (error, response, body) => {
            chai_1.expect(body).to.equal("Photo2 in base64");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should not update user due to birthDate date format", (done) => {
        const user = '{"userId":"11111","birthDate":"13/13/2019"}';
        const userJson = JSON.parse(user);
        request_1.default.put({
            form: userJson,
            url: `${uri}/update_user`,
        }, (error, response, body) => {
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it("Should not update user due to inboardingDone format", (done) => {
        const user = '{"userId":"11111", "inboardingDone":"a"}';
        const userJson = JSON.parse(user);
        request_1.default.put({
            form: userJson,
            url: `${uri}/update_user`,
        }, (error, response, body) => {
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it('Should return "400 Bad Request" and message "UserId is required for update user"', (done) => {
        const user = '{"name":"name1","surname":"surname1"}';
        const userJson = JSON.parse(user);
        request_1.default.put({
            form: userJson,
            url: `${uri}/update_user`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("UserId is required for update user");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it('Should return "400 Bad Request" and message "User with userId=00000 does not exist"', (done) => {
        const user = '{"userId":"00000","name":"name1","surname":"surname1"}';
        const userJson = JSON.parse(user);
        request_1.default.put({
            form: userJson,
            url: `${uri}/update_user`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("User with userId=" + userJson.userId + " does not exist");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it('Should return Bad Request "404 Not Found" parameter is absent', (done) => {
        request_1.default.get({
            url: `${uri}/delete_user/`,
        }, (error, response, body) => {
            chai_1.expect(response.statusCode).to.equal(404);
            done();
        });
    });
    it('Should have file 11111 in the folder "usersPhotos"', (done) => {
        fs.exists(dir, (exists) => {
            chai_1.expect(exists).to.equal(true);
            done();
        });
    });
    it("Should delete user by userId", (done) => {
        const user = '{"userId":"11111","name":"name1","surname":"surname1"}';
        const userJson = JSON.parse(user);
        request_1.default.delete({
            url: `${uri}/delete_user/${userJson.userId}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("User successfully deleted");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should not find deleted user", (done) => {
        const user = '{"userId":"11111"}';
        const userJson = JSON.parse(user);
        request_1.default.get({
            url: `${uri}/get_user/${userJson.userId}`,
        }, (error, response, body) => {
            chai_1.expect(body).to.equal("null");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it('Should not have file 11111 in the folder "usersPhotos"', (done) => {
        fs.exists(dir, (exists) => {
            chai_1.expect(exists).to.equal(false);
            done();
        });
    });
});
//# sourceMappingURL=userTest.spec.js.map
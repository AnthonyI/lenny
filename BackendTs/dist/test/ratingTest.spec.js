"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
require("mocha");
const request_1 = __importDefault(require("request"));
const uri = "http://localhost:8080";
const user = '{"userId":"99999","name":"RatingTest_1","surname":"surname1"}';
const userJson = JSON.parse(user);
const user2 = '{"userId":"99998","name":"RatingTest_2","surname":"surname2"}';
const user2Json = JSON.parse(user2);
describe("Rating tests", () => {
    before(() => {
        request_1.default.post({
            form: userJson,
            url: `${uri}/users/add_user`,
        });
        request_1.default.post({
            form: user2Json,
            url: `${uri}/users/add_user`,
        });
    });
    after(() => {
        request_1.default.delete({
            url: `${uri}/ratings/delete_rating/${userJson.userId}`,
        });
        request_1.default.delete({
            url: `${uri}/users/delete_user/${userJson.userId}`,
        });
        request_1.default.delete({
            url: `${uri}/users/delete_user/${user2Json.userId}`,
        });
    });
    it("Should not post the rating if user does not exist", (done) => {
        const rating = '{"userId": "99997", "comment": "comment 1", "rating": "5", "userRated": "99998" }';
        const ratingJson = JSON.parse(rating);
        request_1.default.post({
            form: ratingJson,
            url: `${uri}/ratings/add_rating`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("User with the given userId does not exist");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it("Should not post the rating if userRated does not exist", (done) => {
        const rating = '{"userId": "99999", "comment": "comment 1", "rating": "5", "userRated": "99997"}';
        const ratingJson = JSON.parse(rating);
        request_1.default.post({
            form: ratingJson,
            url: `${uri}/ratings/add_rating`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("User with the given userRated does nor exist");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it("Should not post the rating if userId is absent", (done) => {
        const rating = '{"comment": "comment 1", "rating": "5", "userRated": "99998"}';
        const ratingJson = JSON.parse(rating);
        request_1.default.post({
            form: ratingJson,
            url: `${uri}/ratings/add_rating`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("userId is absent!");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it("Should not post the rating if userRated is absent", (done) => {
        const rating = '{"userId": "99999", "comment": "comment 1", "rating": "5"}';
        const ratingJson = JSON.parse(rating);
        request_1.default.post({
            form: ratingJson,
            url: `${uri}/ratings/add_rating`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("userRated is absent!");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it("Should not post the rating if rating > 5", (done) => {
        const rating = '{"userId": "99999", "comment": "comment 1", "rating": "6", "userRated": "99998"}';
        const ratingJson = JSON.parse(rating);
        request_1.default.post({
            form: ratingJson,
            url: `${uri}/ratings/add_rating`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("Rating validation failed: rating: 5 is maximum for the rating!");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it("Should not post the rating if rating < 0", (done) => {
        const rating = '{"userId": "99999", "comment": "comment 1", "rating": "-1", "userRated": "99998" }';
        const ratingJson = JSON.parse(rating);
        request_1.default.post({
            form: ratingJson,
            url: `${uri}/ratings/add_rating`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("Rating validation failed: rating: 0 is minimum for the rating!");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it("Should post the rating = 1", (done) => {
        const rating = '{"userId": "99999", "comment": "comment 1", "rating": "1", "userRated": "99998" }';
        const ratingJson = JSON.parse(rating);
        request_1.default.post({
            form: ratingJson,
            url: `${uri}/ratings/add_rating`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.userId).to.equal(ratingJson.userId);
            chai_1.expect(bodyJson.comment).to.equal(ratingJson.comment);
            chai_1.expect(bodyJson.rating).to.equal(1);
            chai_1.expect(bodyJson.userRated).to.equal(ratingJson.userRated);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should get the average rating = 1", (done) => {
        request_1.default.get({
            url: `${uri}/ratings/get_rating/99999`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson[0].averageRating).to.equal(1);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should post the rating = 2", (done) => {
        const rating = '{"userId": "99999", "comment": "comment 2", "rating": "2", "userRated": "99998" }';
        const ratingJson = JSON.parse(rating);
        request_1.default.post({
            form: ratingJson,
            url: `${uri}/ratings/add_rating`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.userId).to.equal(ratingJson.userId);
            chai_1.expect(bodyJson.comment).to.equal(ratingJson.comment);
            chai_1.expect(bodyJson.rating).to.equal(2);
            chai_1.expect(bodyJson.userRated).to.equal(ratingJson.userRated);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should get the average rating = 1.5", (done) => {
        request_1.default.get({
            url: `${uri}/ratings/get_rating/99999`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson[0].averageRating).to.equal(1.5);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
});
//# sourceMappingURL=ratingTest.spec.js.map
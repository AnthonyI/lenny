"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
require("mocha");
const request_1 = __importDefault(require("request"));
const uri = "http://localhost:8080";
const user = '{"userId":"88888","name":"PhotoTest","surname":"surname1"}';
const userJson = JSON.parse(user);
const item = '{"userId":"88888","title":"PhotoTest"}';
const itemJson = JSON.parse(item);
let id = "";
let fileName1 = "";
let fileName2 = "";
let fileName3 = "";
let fileName4 = "";
let fileName5 = "";
// for skip test: xit
describe("photo tests", () => {
    it("Should post the new user", (done) => {
        request_1.default.post({
            form: userJson,
            url: `${uri}/users/add_user`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.userId).to.equal(userJson.userId);
            chai_1.expect(bodyJson.name).to.equal(userJson.name);
            chai_1.expect(bodyJson.surname).to.equal(userJson.surname);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should post the new item", (done) => {
        request_1.default.post({
            form: itemJson,
            url: `${uri}/items/add_item`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            id = bodyJson._id;
            chai_1.expect(bodyJson.userId).to.equal(itemJson.userId);
            chai_1.expect(bodyJson.title).to.equal(itemJson.title);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should not have photos in the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_item/${id}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.photos.length).to.equal(0);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should add photo1", (done) => {
        const photo = '{"photo":"PhotoTest photo1 in base64"}';
        const photoJson = JSON.parse(photo);
        photoJson._id = id;
        request_1.default.post({
            form: photoJson,
            url: `${uri}/items/add_photo`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.fileName.length).to.equal(52);
            chai_1.expect(bodyJson.fileName.substring(0, 27)).to.equal(id + "_20");
            chai_1.expect(bodyJson.fileName.substring(49, 52)).to.equal(".tw");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should have 1 photo in the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_item/${id}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            fileName1 = bodyJson.photos[0];
            chai_1.expect(bodyJson.photos.length).to.equal(1);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should get photo1 of the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_photo/${fileName1}`,
        }, (error, response, body) => {
            chai_1.expect(body).to.equal("PhotoTest photo1 in base64");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should add photo2", (done) => {
        const photo = '{"photo":"PhotoTest photo2 in base64"}';
        const photoJson = JSON.parse(photo);
        photoJson._id = id;
        request_1.default.post({
            form: photoJson,
            url: `${uri}/items/add_photo`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.fileName.length).to.equal(52);
            chai_1.expect(bodyJson.fileName.substring(0, 27)).to.equal(id + "_20");
            chai_1.expect(bodyJson.fileName.substring(49, 52)).to.equal(".tw");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should have 2 photos in the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_item/${id}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            fileName2 = bodyJson.photos[1];
            chai_1.expect(bodyJson.photos.length).to.equal(2);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should get photo2 of the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_photo/${fileName2}`,
        }, (error, response, body) => {
            chai_1.expect(body).to.equal("PhotoTest photo2 in base64");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should add photo3", (done) => {
        const photo = '{"photo":"PhotoTest photo3 in base64"}';
        const photoJson = JSON.parse(photo);
        photoJson._id = id;
        request_1.default.post({
            form: photoJson,
            url: `${uri}/items/add_photo`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.fileName.length).to.equal(52);
            chai_1.expect(bodyJson.fileName.substring(0, 27)).to.equal(id + "_20");
            chai_1.expect(bodyJson.fileName.substring(49, 52)).to.equal(".tw");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should have 3 photos in the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_item/${id}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            fileName3 = bodyJson.photos[2];
            chai_1.expect(bodyJson.photos.length).to.equal(3);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should get photo3 of the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_photo/${fileName3}`,
        }, (error, response, body) => {
            chai_1.expect(body).to.equal("PhotoTest photo3 in base64");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should add photo4", (done) => {
        const photo = '{"photo":"PhotoTest photo4 in base64"}';
        const photoJson = JSON.parse(photo);
        photoJson._id = id;
        request_1.default.post({
            form: photoJson,
            url: `${uri}/items/add_photo`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.fileName.length).to.equal(52);
            chai_1.expect(bodyJson.fileName.substring(0, 27)).to.equal(id + "_20");
            chai_1.expect(bodyJson.fileName.substring(49, 52)).to.equal(".tw");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should have 4 photos in the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_item/${id}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            fileName4 = bodyJson.photos[3];
            chai_1.expect(bodyJson.photos.length).to.equal(4);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should get photo4 of the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_photo/${fileName4}`,
        }, (error, response, body) => {
            chai_1.expect(body).to.equal("PhotoTest photo4 in base64");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should add photo5", (done) => {
        const photo = '{"photo":"PhotoTest photo5 in base64"}';
        const photoJson = JSON.parse(photo);
        photoJson._id = id;
        request_1.default.post({
            form: photoJson,
            url: `${uri}/items/add_photo`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.fileName.length).to.equal(52);
            chai_1.expect(bodyJson.fileName.substring(0, 27)).to.equal(id + "_20");
            chai_1.expect(bodyJson.fileName.substring(49, 52)).to.equal(".tw");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should not add photo6", (done) => {
        const photo = '{"photo":"PhotoTest photo6 in base64"}';
        const photoJson = JSON.parse(photo);
        photoJson._id = id;
        request_1.default.post({
            form: photoJson,
            url: `${uri}/items/add_photo`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("The maximum number of photos is 5");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it("Should have 5 photos in the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_item/${id}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            fileName5 = bodyJson.photos[4];
            chai_1.expect(bodyJson.photos.length).to.equal(5);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should get photo5 of the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_photo/${fileName5}`,
        }, (error, response, body) => {
            chai_1.expect(body).to.equal("PhotoTest photo5 in base64");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should make the first photo5", (done) => {
        request_1.default.put({
            url: `${uri}/items/make_first/${fileName5}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.photos[0]).to.equal(fileName5);
            chai_1.expect(bodyJson.photos[1]).to.equal(fileName1);
            chai_1.expect(bodyJson.photos[2]).to.equal(fileName2);
            chai_1.expect(bodyJson.photos[3]).to.equal(fileName3);
            chai_1.expect(bodyJson.photos[4]).to.equal(fileName4);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it('Should return Bad Request "404 Not Found" parameter is absent in make_first', (done) => {
        request_1.default.put({
            url: `${uri}/items/make_first/`,
        }, (error, response, body) => {
            chai_1.expect(response.statusCode).to.equal(404);
            done();
        });
    });
    it('Should return Bad Request "404 Not Found" parameter is absent in delete_photo', (done) => {
        request_1.default.delete({
            url: `${uri}/items/delete_photo/`,
        }, (error, response, body) => {
            chai_1.expect(response.statusCode).to.equal(404);
            done();
        });
    });
    it('Should return Bad Request "404 Not Found" parameter is absent in get_photo', (done) => {
        request_1.default.get({
            url: `${uri}/items/get_photo/`,
        }, (error, response, body) => {
            chai_1.expect(response.statusCode).to.equal(404);
            done();
        });
    });
    it('Should return Bad Request "400" "CastError" ObjectId format is incorrect in delete_photo', (done) => {
        request_1.default.delete({
            url: `${uri}/items/delete_photo/test`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it('Should return Bad Request "400" "CastError" ObjectId format is incorrect in make_first', (done) => {
        request_1.default.put({
            url: `${uri}/items/make_first/test`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it('Should return Bad Request "400" "CastError" ObjectId format is incorrect in add_photo', (done) => {
        const photo = '{"photo":"PhotoTest photo1 in base64"}';
        const photoJson = JSON.parse(photo);
        photoJson._id = "01234567890123456789012";
        request_1.default.post({
            form: photoJson,
            url: `${uri}/items/add_photo`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it("Should not add photo without item", (done) => {
        const photo = '{"photo":"PhotoTest photo1 in base64"}';
        const photoJson = JSON.parse(photo);
        photoJson._id = "012345678901234567890123";
        request_1.default.post({
            form: photoJson,
            url: `${uri}/items/add_photo`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("Item with the given _id does not exist");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it('Should return Bad Request "400" "No such file or directory!" in delete_photo', (done) => {
        request_1.default.delete({
            url: `${uri}/items/delete_photo/012345678901234567890123`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("No such file = 012345678901234567890123 or directory!");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it('Should return Bad Request "400" "No such file or directory!" in get_photo', (done) => {
        request_1.default.get({
            url: `${uri}/items/get_photo/test`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("No such file = test or directory!");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it('Should return Bad Request "400" "No such file or directory!" in make_first', (done) => {
        request_1.default.put({
            url: `${uri}/items/make_first/012345678901234567890123`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("No such file = 012345678901234567890123 or directory!");
            chai_1.expect(response.statusCode).to.equal(400);
            done();
        });
    });
    it("Should delete photo5", (done) => {
        request_1.default.delete({
            url: `${uri}/items/delete_photo/${fileName5}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("Photo " + fileName5 + " deleted!");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should have 4 photo in the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_item/${id}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.photos.length).to.equal(4);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should delete photo4", (done) => {
        request_1.default.delete({
            url: `${uri}/items/delete_photo/${fileName4}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("Photo " + fileName4 + " deleted!");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should have 3 photo in the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_item/${id}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.photos.length).to.equal(3);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should delete photo3", (done) => {
        request_1.default.delete({
            url: `${uri}/items/delete_photo/${fileName3}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("Photo " + fileName3 + " deleted!");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should have 2 photo in the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_item/${id}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.photos.length).to.equal(2);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should delete photo2", (done) => {
        request_1.default.delete({
            url: `${uri}/items/delete_photo/${fileName2}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("Photo " + fileName2 + " deleted!");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should have 1 photo in the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_item/${id}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.photos.length).to.equal(1);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should delete photo1", (done) => {
        request_1.default.delete({
            url: `${uri}/items/delete_photo/${fileName1}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("Photo " + fileName1 + " deleted!");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should not have photos in the item", (done) => {
        request_1.default.get({
            url: `${uri}/items/get_item/${id}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.photos.length).to.equal(0);
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should delete item", (done) => {
        request_1.default.delete({
            url: `${uri}/items/delete_item/${id}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("Item successfully deleted");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it("Should delete user", (done) => {
        request_1.default.delete({
            url: `${uri}/users/delete_user/${userJson.userId}`,
        }, (error, response, body) => {
            const bodyJson = JSON.parse(body);
            chai_1.expect(bodyJson.message).to.equal("User successfully deleted");
            chai_1.expect(response.statusCode).to.equal(200);
            done();
        });
    });
});
//# sourceMappingURL=photoTest.spec.js.map
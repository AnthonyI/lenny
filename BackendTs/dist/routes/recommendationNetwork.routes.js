"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const recommendationNetwork_controller_1 = require("../controllers/recommendationNetwork.controller");
class RecommendationNetworkRoutes {
    constructor() {
        this.RecommendationNetworkController = new recommendationNetwork_controller_1.RecommendationNetworkController();
    }
    routes(app) {
        app.route("/networks/get_all_networks").get(this.RecommendationNetworkController.get_all_networks);
        app.route("/networks/add_network").post(this.RecommendationNetworkController.add_network);
        app.route("/networks/get_network/:userId").get(this.RecommendationNetworkController.get_network);
        app.route("/networks/update_network").put(this.RecommendationNetworkController.update_network);
        app.route("/networks/delete_network/:userId").delete(this.RecommendationNetworkController.delete_network);
    }
}
exports.RecommendationNetworkRoutes = RecommendationNetworkRoutes;
//# sourceMappingURL=recommendationNetwork.routes.js.map
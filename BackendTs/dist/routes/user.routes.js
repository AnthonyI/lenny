"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_controller_1 = require("../controllers/user.controller");
class UserRoutes {
    constructor() {
        this.userController = new user_controller_1.UserController();
    }
    routes(app) {
        app.route("/users/get_all_users").get(this.userController.get_all_users);
        app.route("/users/add_user").post(this.userController.add_user);
        app.route("/users/get_user/:userId").get(this.userController.get_user);
        app.route("/users/update_user").put(this.userController.update_user);
        app.route("/users/delete_user/:userId").delete(this.userController.delete_user);
        app.route("/users/update_photo").put(this.userController.update_photo);
        app.route("/users/get_photo/:userId").get(this.userController.get_photo);
    }
}
exports.UserRoutes = UserRoutes;
//# sourceMappingURL=user.routes.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rating_controller_1 = require("../controllers/rating.controller");
class RatingRoutes {
    constructor() {
        this.ratingController = new rating_controller_1.RatingController();
    }
    routes(app) {
        app.route("/ratings/get_all_ratings").get(this.ratingController.get_all_ratings);
        app.route("/ratings/add_rating").post(this.ratingController.add_rating);
        app.route("/ratings/get_rating/:userId").get(this.ratingController.get_rating);
        app.route("/ratings/delete_rating/:userId").delete(this.ratingController.delete_rating);
    }
}
exports.RatingRoutes = RatingRoutes;
//# sourceMappingURL=rating.routes.js.map
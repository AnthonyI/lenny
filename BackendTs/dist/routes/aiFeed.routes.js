"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aiFeed_controller_1 = require("../controllers/aiFeed.controller");
class AiFeedRoutes {
    constructor() {
        this.aiFeedController = new aiFeed_controller_1.AiFeedController();
    }
    routes(app) {
        app.route("/ai_feed/add_feed").post(this.aiFeedController.add_feed);
        app.route("/ai_feed/get_feed/:userId").get(this.aiFeedController.get_feed);
        app.route("/ai_feed/update_feed").put(this.aiFeedController.update_feed);
        app.route("/ai_feed/delete_feed/:userId").delete(this.aiFeedController.delete_feed);
    }
}
exports.AiFeedRoutes = AiFeedRoutes;
//# sourceMappingURL=aiFeed.routes.js.map
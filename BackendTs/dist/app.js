"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = __importStar(require("body-parser"));
const cors = require("cors");
const express = require("express");
const mongoose = require("mongoose");
const aiFeed_routes_1 = require("./routes/aiFeed.routes");
const item_routes_1 = require("./routes/item.routes");
const rating_routes_1 = require("./routes/rating.routes");
const recommendationNetwork_routes_1 = require("./routes/recommendationNetwork.routes");
const user_routes_1 = require("./routes/user.routes");
class App {
    constructor() {
        this.userRoute = new user_routes_1.UserRoutes();
        this.ratingRoute = new rating_routes_1.RatingRoutes();
        this.RecommendationNetworkRoutes = new recommendationNetwork_routes_1.RecommendationNetworkRoutes();
        this.itemRoute = new item_routes_1.ItemRoutes();
        this.aiFeedRoute = new aiFeed_routes_1.AiFeedRoutes();
        this.MONGO_CONNECTION_STRING_LOCAL = "mongodb://localhost/TradeAndWearDB";
        this.MONGO_CONNECTION_STRING_DEVELOP = "mongodb+srv://akram_TW:" + process.env.TW_MONGO_SECRET + "@clustertw-ioece.gcp.mongodb.net/TradeAndWearDB?retryWrites=true";
        this.app = express();
        this.config();
        this.userRoute.routes(this.app);
        this.ratingRoute.routes(this.app);
        this.RecommendationNetworkRoutes.routes(this.app);
        this.itemRoute.routes(this.app);
        this.aiFeedRoute.routes(this.app);
        this.mongoSetup();
    }
    config() {
        this.app.use(bodyParser.json({ limit: "50mb" }));
        this.app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 1000000 }));
        this.app.use(cors());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        // serving static files
        this.app.use(express.static("public"));
    }
    mongoSetup() {
        mongoose.Promise = global.Promise;
        switch (process.argv[2]) {
            case "local":
                mongoose.connect(this.MONGO_CONNECTION_STRING_LOCAL, {
                    useNewUrlParser: true,
                    useUnifiedTopology: true,
                }).then(() => {
                    // tslint:disable-next-line:no-console
                    console.log("connection to LOCAL database established");
                });
                break;
            default:
                mongoose.connect(this.MONGO_CONNECTION_STRING_DEVELOP, {
                    dbName: "TradeAndWearDB",
                    useNewUrlParser: true,
                    useUnifiedTopology: true,
                }).then(() => {
                    // tslint:disable-next-line:no-console
                    console.log("connection to SERVER database established");
                });
                break;
        }
        mongoose.set("useFindAndModify", false);
    }
}
exports.default = new App().app;
//# sourceMappingURL=app.js.map
//
//  MenuBar.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-01-28.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit
import AudioUnit

class MenuBarView: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    var mainViewControllerInstance: MainViewController?
    
    private var mainView = UIView()
    private var collectionViewWrapper = UIView()
    
    let cellId = "cellId"
    let menuIcons = ["post_menu_icon","feed_menu_icon","profile_menu_icon"]
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        view.backgroundColor = UIColor.clear
        view.dataSource = self
        view.delegate = self
        
        return view
    }()
    
    override init(frame: CGRect){
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: NSNotification.Name("updateUIMenuBarView"), object: nil)
        
        mainView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 70 + appDelegate().service.getBottomSafeAreaHeight()))
        
        collectionView.register(MenuCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.frame.size.height = mainView.bounds.size.height - appDelegate().service.getBottomSafeAreaHeight()
        collectionView.frame.size.width = 70*3
        collectionView.center.x = mainView.center.x
        collectionView.isScrollEnabled = false
        
        let selectedMenuIcon = NSIndexPath(item: 1, section: 0)
        collectionView.selectItem(at: selectedMenuIcon as IndexPath, animated: false, scrollPosition: [])

        mainView.addSubview(collectionView)
        addSubview(mainView)
        
        updateUI()
    }
    
    @objc func updateUI() {
        mainView.backgroundColor = UIColor.isLightModeBackground()
        
        for cell in collectionView.visibleCells as! [MenuCell] {
            cell.setMenuTint()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MenuCell
        cell.imageView.image = UIImage(named: menuIcons[indexPath.item])?.withRenderingMode(.alwaysTemplate)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        mainViewControllerInstance?.scrollToMenuIndex(menuIndex: indexPath.item)
    }
    
}

class MenuCell: UICollectionViewCell {
    
    private var cellView = UIView()
    let imageView: UIImageView = {
        let i = UIImageView()
        i.tintColor = UIColor.lightGray()
        return i
    }()
    
    override var isSelected: Bool {
        didSet{
            setMenuTint()
        }
    }
    
    func setMenuTint() {
        imageView.tintColor = isSelected ? UIColor.colorForText() : UIColor.lightGray()
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){

        addSubview(cellView)
        cellView.frame = self.bounds
        
        addSubview(imageView)
        addConstraintsWithFormat("H:[v0(35)]", views: imageView)
        addConstraintsWithFormat("V:[v0(35)]", views: imageView)
        
        addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }

}

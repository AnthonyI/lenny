//
//  CustomMenuCell.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-03-18.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit

class CustomMenuCell: NSObject {
    var cellInfos: SettingName
    var image = String()
    
    init(title: SettingName, image: String){
        self.cellInfos = title
        self.image = image
    }
}

class CustomMenuColCell: UICollectionViewCell {
    
    override var isHighlighted: Bool {
        didSet{
            nameLabel.textColor = isHighlighted ? UIColor.lightGray() : UIColor.colorForText()
        }
    }
    
    var aCell: CustomMenuCell? {
        didSet {
            nameLabel.text = aCell?.cellInfos.rawValue.0 // This is the title
            
            if let imageName = aCell?.image {
                iconImageView.image = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
            }
        }
    }
    
    func setColor() {
        iconImageView.tintColor = UIColor.colorForText()
        nameLabel.textColor = isHighlighted ? UIColor.lightGray() : UIColor.colorForText()
        arrowIconImageView.tintColor = UIColor.colorForText()
    }
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "TITLE"
        label.textColor = UIColor.colorForText()
        label.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        return label
    }()
    
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "settings_icon")
        imageView.tintColor = UIColor.colorForText()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let arrowIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "arrow_right_icon")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.colorForText()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    func setupViews(){
        
        addSubview(nameLabel)
        addSubview(iconImageView)
        addSubview(arrowIconImageView)
        
        addConstraintsWithFormat("H:|-15-[v0(18)]-15-[v1]-[v2(14)]-15-|", views: iconImageView, nameLabel, arrowIconImageView)
        addConstraintsWithFormat("V:|[v0]|", views: nameLabel)
        addConstraintsWithFormat("V:[v0(18)]", views: iconImageView)
        addConstraintsWithFormat("V:[v0(14)]", views: arrowIconImageView)
        
        addConstraint(NSLayoutConstraint(item: iconImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: arrowIconImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

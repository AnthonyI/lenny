//
//  TWButton.swift
//  Trade&Wear
//
//  Created by Marc Laberge on 2019-01-31.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit

@IBDesignable
public class TWButton: UIButton {
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 10.0
        self.clipsToBounds = true
        self.layer.borderWidth = 5.0
        self.contentEdgeInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        self.titleLabel?.textAlignment = .center
    }
}

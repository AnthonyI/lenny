//
//  PostInfoView.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-03-13.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox

class PostInfoView: UIView, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var postViewControllerInstance: PostViewController?
    
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var mainScrollVIew: UIScrollView!
    @IBOutlet weak var contentWrapper: UIView!
    
    @IBOutlet weak var titleViewWrapper: UIView!
    @IBOutlet weak var titreTextField: UITextField!
    
    @IBOutlet weak var prixViewWrapper: UIView!
    @IBOutlet weak var prixTextField: UITextField!
    
    @IBOutlet weak var tailleViewWrapper: UIView!
    @IBOutlet weak var tailleTextField: UITextField!
    var currentSizesArray = [String]()
    let taillePicker = UIPickerView()
    
    @IBOutlet weak var styleViewWrapper: UIView!
    @IBOutlet weak var styleTextField: UITextField!
    var currentStylesArray = [String]()
    let stylePicker = UIPickerView()
    
    @IBOutlet weak var couleurViewWrapper: UIView!
    @IBOutlet weak var couleurTextField: UITextField!
    
    var keyboardHeight: CGFloat = 0.0
    
    
    //////////////////////////////////////////////////////////////////
    
    
    override init(frame: CGRect){
        super.init(frame: frame)

        self.hideKeyboardWhenTappedAround()
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        Bundle.main.loadNibNamed("PostInfoView", owner: self, options: nil)
        
        titleViewWrapper.backgroundColor = .white
        titleViewWrapper.layer.borderWidth = 2
        titleViewWrapper.layer.borderColor = UIColor.white.cgColor
        titleViewWrapper.layer.cornerRadius = 8.0
        
        titreTextField.becomeFirstResponder()
        titreTextField.autocorrectionType = UITextAutocorrectionType.no
        titreTextField.keyboardType = UIKeyboardType.default
        titreTextField.returnKeyType = UIReturnKeyType.done
        titreTextField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        titreTextField.delegate = self
        titreTextField.backgroundColor = .clear
        titreTextField.tintColor = .black
        titreTextField.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        
        prixViewWrapper.backgroundColor = .white
        prixViewWrapper.layer.borderWidth = 2
        prixViewWrapper.layer.borderColor = UIColor.white.cgColor
        prixViewWrapper.layer.cornerRadius = 8.0
        
        prixTextField.autocorrectionType = UITextAutocorrectionType.no
        prixTextField.keyboardType = UIKeyboardType.default
        prixTextField.returnKeyType = UIReturnKeyType.done
        prixTextField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        prixTextField.delegate = self
        prixTextField.backgroundColor = .clear
        prixTextField.tintColor = .black
        prixTextField.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        prixTextField.keyboardType = .numberPad
        
        tailleViewWrapper.backgroundColor = .white
        tailleViewWrapper.layer.borderWidth = 2
        tailleViewWrapper.layer.borderColor = UIColor.white.cgColor
        tailleViewWrapper.layer.cornerRadius = 8.0
        
        tailleTextField.autocorrectionType = UITextAutocorrectionType.no
        tailleTextField.keyboardType = UIKeyboardType.default
        tailleTextField.returnKeyType = UIReturnKeyType.done
        tailleTextField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        tailleTextField.delegate = self
        tailleTextField.backgroundColor = .clear
        tailleTextField.tintColor = .black
        tailleTextField.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        tailleTextField.inputView = taillePicker
        
        styleViewWrapper.backgroundColor = .white
        styleViewWrapper.layer.borderWidth = 2
        styleViewWrapper.layer.borderColor = UIColor.white.cgColor
        styleViewWrapper.layer.cornerRadius = 8.0
        
        styleTextField.autocorrectionType = UITextAutocorrectionType.no
        styleTextField.keyboardType = UIKeyboardType.default
        styleTextField.returnKeyType = UIReturnKeyType.done
        styleTextField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        styleTextField.delegate = self
        styleTextField.backgroundColor = .clear
        styleTextField.tintColor = .black
        styleTextField.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        styleTextField.inputView = stylePicker
        
        couleurViewWrapper.backgroundColor = .white
        couleurViewWrapper.layer.borderWidth = 2
        couleurViewWrapper.layer.borderColor = UIColor.white.cgColor
        couleurViewWrapper.layer.cornerRadius = 8.0
        
        couleurTextField.autocorrectionType = UITextAutocorrectionType.no
        couleurTextField.keyboardType = UIKeyboardType.default
        couleurTextField.returnKeyType = UIReturnKeyType.done
        couleurTextField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        couleurTextField.delegate = self
        couleurTextField.backgroundColor = .clear
        couleurTextField.tintColor = .black
        couleurTextField.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        
        setCurrentItemInfo()
        
        stylePicker.delegate = self
        pickerToolbar()
        
        taillePicker.delegate = self
        pickerToolbar()
        
        mainView.frame = self.bounds
        mainView.clipsToBounds = true
        addSubview(mainView)
        
        updateUI()
    }
    
    @objc func updateUI(){
        mainView.backgroundColor = UIColor.isLightModeBackground()
    }
    
    func setCurrentItemInfo() {
        DispatchQueue.main.async {
            
            if(self.postViewControllerInstance?.itemInProcess.itemProfile.title != nil){
                self.titreTextField.text = self.postViewControllerInstance?.itemInProcess.itemProfile.title
            }
            if(self.postViewControllerInstance?.itemInProcess.itemProfile.price != nil){
                self.prixTextField.text = self.postViewControllerInstance?.itemInProcess.itemProfile.price
            }
            if(self.postViewControllerInstance?.itemInProcess.itemCharacteristics.size != nil){
                self.tailleTextField.text = self.postViewControllerInstance?.itemInProcess.itemCharacteristics.size
            }
            if(self.postViewControllerInstance?.itemInProcess.itemProfile.style != nil){
                self.styleTextField.text = self.postViewControllerInstance?.itemInProcess.itemProfile.style?.rawValue
            }
            if(self.postViewControllerInstance?.itemInProcess.itemCharacteristics.couleur != nil){
                self.couleurTextField.text = self.postViewControllerInstance?.itemInProcess.itemCharacteristics.couleur
            }
            
            self.currentStylesArray.append("")
            for i in allItemStyle{
                self.currentStylesArray.append(i.rawValue)
            }
            
            self.currentSizesArray = (self.postViewControllerInstance?.chosenItemType?.rawValue.1)!

        }
    }
    
    @objc func saveItemInfo() {
        
        var title = String()
        var price = String()
        var size = String()
        var style = String()
        var couleur = String()
        
        if(!titreTextField.text!.isEmpty){
            title = titreTextField.text!
        }
        if(!prixTextField.text!.isEmpty){
            price = prixTextField.text!
        }
        if(!tailleTextField.text!.isEmpty){
            size = tailleTextField.text!
        }
        if(!styleTextField.text!.isEmpty){
            style = styleTextField.text!
        }
        if(!couleurTextField.text!.isEmpty){
            couleur = couleurTextField.text!
        }
        
        let itemCharacteristics = ItemCharacteristics(
            size: size,
            couleur: couleur
        )
        
        let itemToAdd = ItemModel(
            title: title,
            price: price,
            type: (postViewControllerInstance?.chosenItemType)!,
            style: itemStyle(rawValue: style)!,
            photos: postViewControllerInstance?.itemInProcess.itemProfile.photos ?? [:],
            characteristics: itemCharacteristics
        )
        
        itemToAdd.itemProfile.itemId = postViewControllerInstance?.itemInProcess.itemProfile.itemId
        
        postViewControllerInstance?.itemInProcess = itemToAdd  // Current adding item
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) { }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        if (textField == self.titreTextField) {
            self.prixTextField.becomeFirstResponder()
        }
        else if (textField == self.prixTextField) {
            self.tailleTextField.becomeFirstResponder()
        }
        else if (textField == self.tailleTextField) {
            self.couleurTextField.becomeFirstResponder()
            
        }/* else if (textField == self.tailleTextField) {
            pickerToolbar()
            self.endEditing(true)
            return false
        }*/
        
        return true
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    TAILLE PICKER
    ///////////////////////////////////////////////////////////////
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == stylePicker){
            return currentStylesArray.count
        }else{
            return currentSizesArray.count
        }
        
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == stylePicker){
            return currentStylesArray[row]
        }else{
            return currentSizesArray[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == stylePicker){
            styleTextField.text = currentStylesArray[row]
        }else{
            tailleTextField.text = currentSizesArray[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var titleData = String()
        
        if(pickerView == stylePicker){
            titleData = currentStylesArray[row]
        }else{
            titleData = currentSizesArray[row]
        }
        
        let myTitle = NSAttributedString(string: titleData, attributes: [NSAttributedString.Key.foregroundColor: UIColor.colorForText()])
        
        return myTitle
    }
    
    func pickerToolbar(){
        
        //ToolBar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.isTranslucent = false
        toolbar.barTintColor = UIColor.isLightModeItem()
        
        let cancelButton = UIBarButtonItem(title: "Annuler", style: .plain, target: self, action: #selector(cancelDatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Terminer", style: .plain, target: self, action: #selector(doneDatePicker))
        
        cancelButton.tintColor = UIColor.colorForText()
        doneButton.tintColor = UIColor.colorForText()
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        tailleTextField.inputAccessoryView = toolbar
        tailleTextField.inputView = taillePicker
        
        styleTextField.inputAccessoryView = toolbar
        styleTextField.inputView = stylePicker
        
        taillePicker.backgroundColor = UIColor.isLightModeItem()
        
        stylePicker.backgroundColor = UIColor.isLightModeItem()
        
    }
    @objc func doneDatePicker(){
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        endEditing(true)
    }
    @objc func cancelDatePicker(){
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        endEditing(true)
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        keyboardHeight = keyboardFrame.height
        
        var contentInset:UIEdgeInsets = self.mainScrollVIew.contentInset
        contentInset.bottom = keyboardHeight + 40
        mainScrollVIew.contentInset = contentInset
        
    }
    @objc func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        mainScrollVIew.contentInset = contentInset
        
    }
    
    
    // Check if field are empty
    func checkForErrors() -> Bool {
        var errors = false
        let title = "Oups"
        var message = ""
        
        if titreTextField.text!.isEmpty {
            errors = true
            message += "Pas de titre"
            alertWithTitle(title: title, message: message, ViewController: postViewControllerInstance!, toFocus: self.titreTextField)
        }
        else if prixTextField.text!.isEmpty
        {
            errors = true
            message += "Pas de prix"
            alertWithTitle(title: title, message: message, ViewController: postViewControllerInstance!, toFocus: self.prixTextField)
        }
        else if styleTextField.text!.isEmpty
        {
            errors = true
            message += "Pas de style"
            alertWithTitle(title: title, message: message, ViewController: postViewControllerInstance!, toFocus: self.styleTextField)
        }
        else if tailleTextField.text!.isEmpty
        {
            errors = true
            message += "Pas de taille"
            alertWithTitle(title: title, message: message, ViewController: postViewControllerInstance!, toFocus: self.tailleTextField)
        }
        
        return errors
    }
    func alertWithTitle(title: String!, message: String, ViewController: UIViewController, toFocus:UITextField) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel,handler: {_ in
            toFocus.becomeFirstResponder()
        });
        alert.addAction(action)
        ViewController.present(alert, animated: true, completion:nil)
    }

}

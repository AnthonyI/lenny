//
//  CustomThemeModeCell.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-03-31.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit

class CustomThemeModeCell: NSObject {
    var cellInfos = String()
    var image = String()
    
    init(title: String, image: String){
        self.cellInfos = title
        self.image = image
    }
}

class CustomColorsCell: NSObject {
    var cellInfos = String()
    var image = String()
    
    init(title: String, image: String){
        self.cellInfos = title
        self.image = image
    }
}

class CustomPreferencesCell: UICollectionViewCell {
    
    var selectedColor: String?
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "TITLE"
        label.textColor = UIColor.colorForText()
        label.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        return label
    }()
    
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "color_icon")
        imageView.tintColor = UIColor.colorForText()
        imageView.contentMode = .scaleAspectFill
        
        return imageView
    }()
    
    let arrowIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "arrow_right_icon")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.colorForText()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let switchOnOff: UISwitch = {
        let s = UISwitch()
        s.onTintColor = UIColor.themeItemBlack()
        s.tintColor = UIColor.isLightModeItem()
        //s.thumbTintColor = UIColor.white
        s.addTarget(self, action: #selector(switchStateDidChange), for: .valueChanged)
        s.setOn(!appDelegate().isLightModeCurrently!, animated: false)
        return s
    }()
    
    let whiteColorImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "color_icon")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.themeItemWhite()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 0
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.colorForText().cgColor
        imageView.layer.cornerRadius = 17.5
        imageView.clipsToBounds = true
        
        return imageView
    }()
    let blackColorImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "color_icon")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.themeItemBlack()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 0
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.colorForText().cgColor
        imageView.layer.cornerRadius = 17.5
        imageView.clipsToBounds = true
        
        return imageView
    }()
    let orangeColorImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "color_icon")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.themeItemOrange()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 0
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.colorForText().cgColor
        imageView.layer.cornerRadius = 17.5
        imageView.clipsToBounds = true
        
        return imageView
    }()
    let blueColorImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "color_icon")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.themeItemBlue()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 0
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.colorForText().cgColor
        imageView.layer.cornerRadius = 17.5
        imageView.clipsToBounds = true
        
        return imageView
    }()
    let redColorImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "color_icon")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.themeItemRed()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 0
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.colorForText().cgColor
        imageView.layer.cornerRadius = 17.5
        imageView.clipsToBounds = true
        
        return imageView
    }()
    let greenColorImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "color_icon")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.themeItemGreen()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 0
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.colorForText().cgColor
        imageView.layer.cornerRadius = 17.5
        imageView.clipsToBounds = true
        
        return imageView
    }()
    
    
    //////////////////////////////////////////////////////////
    
    
    override var isHighlighted: Bool {
        didSet{
            //nameLabel.textColor = isHighlighted ? UIColor.lightGray() : UIColor.colorForText()
        }
    }
    
    var aThemeModeCell: CustomThemeModeCell? {
        didSet {
            
            if let imageName = aThemeModeCell?.image {
                iconImageView.image = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
            }
            
            nameLabel.text = aThemeModeCell?.cellInfos
            
            addSubview(switchOnOff)
            arrowIconImageView.removeFromSuperview()
            
            addConstraintsWithFormat("H:|-15-[v0(18)]-15-[v1]-[v2]-15-|", views: iconImageView, nameLabel, switchOnOff)
            addConstraintsWithFormat("V:[v0(30)]", views: switchOnOff)
            
            addConstraint(NSLayoutConstraint(item: switchOnOff, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        }
    }
    
    var aColorsCell: CustomColorsCell? {
        didSet {
            
            if let imageName = aColorsCell?.image {
                iconImageView.image = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
                iconImageView.layer.borderWidth = 1
                iconImageView.layer.masksToBounds = false
                iconImageView.layer.borderColor = UIColor.colorForText().cgColor
                iconImageView.layer.cornerRadius = 9
                iconImageView.clipsToBounds = true
            }
            
            nameLabel.text = aColorsCell?.cellInfos
            
            nameLabel.removeFromSuperview()
            iconImageView.removeFromSuperview()
            switchOnOff.removeFromSuperview()
            arrowIconImageView.removeFromSuperview()
            
            
            // TITLE VIEW ---------
            
            let titleView = UIView()
            addSubview(titleView)
            
            addConstraintsWithFormat("H:|[v0]|", views: titleView)
            addConstraintsWithFormat("V:[v0(40)]", views: titleView)
            
            titleView.addSubview(iconImageView)
            titleView.addSubview(nameLabel)
            
            titleView.addConstraintsWithFormat("H:|-15-[v0(18)]-15-[v1]-15-|", views: iconImageView, nameLabel)
            titleView.addConstraintsWithFormat("V:|[v0]|", views: nameLabel)
            titleView.addConstraintsWithFormat("V:[v0(18)]", views: iconImageView)
            
            titleView.addConstraint(NSLayoutConstraint(item: iconImageView, attribute: .centerY, relatedBy: .equal, toItem: titleView, attribute: .centerY, multiplier: 1, constant: 0))
            
            
            // COLORS VIEW ---------
        
            let colorsView = UIView()
            addSubview(colorsView)
            
            addConstraintsWithFormat("H:|[v0]|", views: colorsView)
            addConstraintsWithFormat("V:|-40-[v0(40)]", views: colorsView)
            
            let whiteView = UIView()
            colorsView.addSubview(whiteView)
            whiteView.addSubview(whiteColorImage)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.colorTapped(sender:)))
            whiteColorImage.isUserInteractionEnabled = true
            whiteColorImage.addGestureRecognizer(tap)
            
            let blackView = UIView()
            colorsView.addSubview(blackView)
            blackView.addSubview(blackColorImage)
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.colorTapped(sender:)))
            blackColorImage.isUserInteractionEnabled = true
            blackColorImage.addGestureRecognizer(tap1)
            
            let orangeView = UIView()
            colorsView.addSubview(orangeView)
            orangeView.addSubview(orangeColorImage)
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.colorTapped(sender:)))
            orangeColorImage.isUserInteractionEnabled = true
            orangeColorImage.addGestureRecognizer(tap2)
            
            let blueView = UIView()
            colorsView.addSubview(blueView)
            blueView.addSubview(blueColorImage)
            let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.colorTapped(sender:)))
            blueColorImage.isUserInteractionEnabled = true
            blueColorImage.addGestureRecognizer(tap3)
            
            let redView = UIView()
            colorsView.addSubview(redView)
            redView.addSubview(redColorImage)
            let tap4 = UITapGestureRecognizer(target: self, action: #selector(self.colorTapped(sender:)))
            redColorImage.isUserInteractionEnabled = true
            redColorImage.addGestureRecognizer(tap4)
            
            let greenView = UIView()
            colorsView.addSubview(greenView)
            greenView.addSubview(greenColorImage)
            let tap5 = UITapGestureRecognizer(target: self, action: #selector(self.colorTapped(sender:)))
            greenColorImage.isUserInteractionEnabled = true
            greenColorImage.addGestureRecognizer(tap5)
            
            let width = UIScreen.main.bounds.width / 6
            
            colorsView.addConstraintsWithFormat("H:|[v0(\(width))]-0-[v1(\(width))]-0-[v2(\(width))]-0-[v3(\(width))]-0-[v4(\(width))]-0-[v5(\(width))]|", views: whiteView,blackView,orangeView,blueView,redView,greenView)
            colorsView.addConstraintsWithFormat("V:|[v0]|", views: whiteView)
            colorsView.addConstraintsWithFormat("V:|[v0]|", views: blackView)
            colorsView.addConstraintsWithFormat("V:|[v0]|", views: orangeView)
            colorsView.addConstraintsWithFormat("V:|[v0]|", views: blueView)
            colorsView.addConstraintsWithFormat("V:|[v0]|", views: redView)
            colorsView.addConstraintsWithFormat("V:|[v0]|", views: greenView)
            
            let iconSize = 35
            
            whiteView.addConstraintsWithFormat("H:[v0(\(iconSize))]", views: whiteColorImage)
            whiteView.addConstraintsWithFormat("V:[v0(\(iconSize))]", views: whiteColorImage)
            addConstraint(NSLayoutConstraint(item: whiteColorImage, attribute: .centerX, relatedBy: .equal, toItem: whiteView, attribute: .centerX, multiplier: 1, constant: 0))
            
            blackView.addConstraintsWithFormat("H:[v0(\(iconSize))]", views: blackColorImage)
            blackView.addConstraintsWithFormat("V:[v0(\(iconSize))]", views: blackColorImage)
            addConstraint(NSLayoutConstraint(item: blackColorImage, attribute: .centerX, relatedBy: .equal, toItem: blackView, attribute: .centerX, multiplier: 1, constant: 0))
            
            orangeView.addConstraintsWithFormat("H:[v0(\(iconSize))]", views: orangeColorImage)
            orangeView.addConstraintsWithFormat("V:[v0(\(iconSize))]", views: orangeColorImage)
            addConstraint(NSLayoutConstraint(item: orangeColorImage, attribute: .centerX, relatedBy: .equal, toItem: orangeView, attribute: .centerX, multiplier: 1, constant: 0))
            
            blueView.addConstraintsWithFormat("H:[v0(\(iconSize))]", views: blueColorImage)
            blueView.addConstraintsWithFormat("V:[v0(\(iconSize))]", views: blueColorImage)
            addConstraint(NSLayoutConstraint(item: blueColorImage, attribute: .centerX, relatedBy: .equal, toItem: blueView, attribute: .centerX, multiplier: 1, constant: 0))
            
            redView.addConstraintsWithFormat("H:[v0(\(iconSize))]", views: redColorImage)
            redView.addConstraintsWithFormat("V:[v0(\(iconSize))]", views: redColorImage)
            addConstraint(NSLayoutConstraint(item: redColorImage, attribute: .centerX, relatedBy: .equal, toItem: redView, attribute: .centerX, multiplier: 1, constant: 0))
            
            greenView.addConstraintsWithFormat("H:[v0(\(iconSize))]", views: greenColorImage)
            greenView.addConstraintsWithFormat("V:[v0(\(iconSize))]", views: greenColorImage)
            addConstraint(NSLayoutConstraint(item: greenColorImage, attribute: .centerX, relatedBy: .equal, toItem: greenView, attribute: .centerX, multiplier: 1, constant: 0))
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        initUserColor()
    }
    
    func setupViews(){
        
        switchOnOff.addTarget(self, action: #selector(switchStateDidChange), for: .valueChanged)
    
        addSubview(nameLabel)
        addSubview(iconImageView)
        addSubview(arrowIconImageView)
        
        addConstraintsWithFormat("H:|-15-[v0(18)]-15-[v1]-[v2]-15-|", views: iconImageView, nameLabel, arrowIconImageView)
        addConstraintsWithFormat("V:|[v0]|", views: nameLabel)
        addConstraintsWithFormat("V:[v0(18)]", views: iconImageView)
        addConstraintsWithFormat("V:[v0(18)]", views: arrowIconImageView)
        
        addConstraint(NSLayoutConstraint(item: iconImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: arrowIconImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setColor() {
        iconImageView.tintColor = UIColor.colorForText()
        iconImageView.layer.borderColor = UIColor.colorForText().cgColor
        nameLabel.textColor = isHighlighted ? UIColor.lightGray() : UIColor.colorForText()
        arrowIconImageView.tintColor = UIColor.colorForText()
    }
    
    @objc func switchStateDidChange(_ sender:UISwitch){
        if (sender.isOn == true){
            // DARK MODE
            appDelegate().isLightModeCurrently = false
        }
        else{
            // WHITE MODE
            appDelegate().isLightModeCurrently = true
        }
    }
    
    @objc func colorTapped(sender: UITapGestureRecognizer) {
        
        let allColors:[UIImageView] = [whiteColorImage,blackColorImage,orangeColorImage,blueColorImage,redColorImage,greenColorImage]
        
        for color in allColors {
            color.layer.borderWidth = 0
        }
        
        let tappedImage = sender.view as! UIImageView
        tappedImage.layer.borderWidth = 2
        
        selectColor(image: tappedImage)
    }
    
    func initUserColor(){
        
        var color: String = "White"
        
        if(appDelegate().isLoggedIn()){
            color = (appDelegate().loggedInUser?.userPreferences.color)!
        }else{
            color = appDelegate().currentChosenColor!
        }
        
        switch color {
        case "White":
            selectedColor = "White"
            whiteColorImage.layer.borderWidth = 2
        case "Black":
            selectedColor = "Black"
            blackColorImage.layer.borderWidth = 2
        case "Orange":
            selectedColor = "Orange"
            orangeColorImage.layer.borderWidth = 2
        case "Blue":
            selectedColor = "Blue"
            blueColorImage.layer.borderWidth = 2
        case "Red":
            selectedColor = "Red"
            redColorImage.layer.borderWidth = 2
        case "Green":
            selectedColor = "Green"
            greenColorImage.layer.borderWidth = 2
        default:
            selectedColor = "White"
            whiteColorImage.layer.borderWidth = 2
        }
        
        appDelegate().currentChosenColor = selectedColor
    }
    
    func selectColor(image: UIImageView){
        
        switch image {
            
        case whiteColorImage:
            selectedColor = "White"
        case blackColorImage:
            selectedColor = "Black"
        case orangeColorImage:
            selectedColor = "Orange"
        case blueColorImage:
            selectedColor = "Blue"
        case redColorImage:
            selectedColor = "Red"
        case greenColorImage:
            selectedColor = "Green"
        default:
            selectedColor = "White"
        }
        
        appDelegate().currentChosenColor = selectedColor
    }
    
}

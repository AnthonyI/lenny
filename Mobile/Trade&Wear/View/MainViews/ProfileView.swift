//
//  ProfileView.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-01-29.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit
import AudioUnit

class ProfileView: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    var mainViewControllerInstance: MainViewController?
    
    @IBOutlet var mainView: UIView!
    
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profileInfoView: UIView!
    
    @IBOutlet weak var mesArticlesTitle: UILabel!
    @IBOutlet weak var itemInfoView: UIView!
    
    private var loginView: UIView!
    lazy var login: LoginView = {
        let login = LoginView()
        return login
    }()
    
    @IBOutlet weak var PRENOM: UILabel!
    @IBOutlet weak var NOM: UILabel!
    @IBOutlet weak var familyName: UILabel!
    @IBOutlet weak var givenName: UILabel!
    @IBOutlet weak var picture: UIImageView!
    
    let myItemsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
    let cellId = "cellId"
    let cellHeight:CGFloat = 50
    
    var allCells: [CustomItemCell] = []
    var myItems: [ItemModel] = []
    
    
    //////////////////////////////////////////////////////////////////

    
    override init(frame: CGRect){
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){

        NotificationCenter.default.addObserver(self, selector: #selector(updateLayout), name: NSNotification.Name("updateLayoutProfileView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateMyItems), name: NSNotification.Name("updateMyItemsProfileView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: NSNotification.Name("updateUIProfileView"), object: nil)
        
        Bundle.main.loadNibNamed("ProfileView", owner: self, options: nil)
        
        addSubview(mainView)
        mainView.frame = self.frame
        
        updateUI()
        updateLayout()
        
    }
    
    @objc func updateLayout() {
        DispatchQueue.main.async {
            if(appDelegate().isLoggedIn()){
                self.updateMyItems()
                self.initViewIfConnected()
            }else{
                self.initViewIfNotConnected()
            }
        }
    }
    
    @objc func updateUI() {
        if(appDelegate().isLoggedIn()){
            profileView.backgroundColor = UIColor.isLightModeBackground()
            
            profileInfoView.backgroundColor = UIColor.isLightModeItem()
            mesArticlesTitle.textColor = UIColor.colorForText()
            PRENOM.textColor = UIColor.colorForText()
            NOM.textColor = UIColor.colorForText()
            familyName.textColor = UIColor.colorForText()
            givenName.textColor = UIColor.colorForText()
            
            itemInfoView.backgroundColor = UIColor.isLightModeItem()
            myItemsCollectionView.backgroundColor = UIColor.isLightModeBackground()
            
            myItemsCollectionView.reloadData()
        }else{
            
        }
    }
    
    @objc func updateMyItems() {
        allCells = []
        myItems = (appDelegate().loggedInUser?.myItems)!
        for item in myItems {
            let tempCell = CustomItemCell(item: item)
            allCells.append(tempCell)
        }
        
        myItemsCollectionView.reloadData()
        myItemsCollectionView.delegate = self
    }
    
    // UI IF USER LOGGED IN -----------------------------------
    func initViewIfConnected(){
        
        let colViewBottomInset: CGFloat = 70 + appDelegate().service.getBottomSafeAreaHeight() // MenuBar height
        
        profileView.alpha = 0.0
        
        if(loginView != nil){
            UIView.animate(withDuration: 0.15){
                self.loginView.alpha = 0.0
            }
            self.loginView.removeFromSuperview()
        }
        
        // USER INFO
        let tempFamilyName = appDelegate().loggedInUser?.userProfile.name
        self.familyName.text = tempFamilyName?.uppercased()
        
        let tempGivenName = appDelegate().loggedInUser?.userProfile.surname
        self.givenName.text = tempGivenName?.uppercased()
        
        self.picture.contentMode = .scaleAspectFill
        self.picture.layer.borderWidth = 2
        self.picture.layer.masksToBounds = false
        self.picture.layer.borderColor = UIColor.white.cgColor
        self.picture.layer.cornerRadius = 8.0
        self.picture.clipsToBounds = true
        
        self.picture.image = (appDelegate().loggedInUser?.userProfile.photo)?.base64InImage
        
        myItemsCollectionView.frame = CGRect(x: 0, y: 0, width: itemInfoView.frame.width, height: itemInfoView.frame.size.height)
        myItemsCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: colViewBottomInset, right: 0)
        myItemsCollectionView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: colViewBottomInset, right: 0)
        myItemsCollectionView.showsVerticalScrollIndicator = false
        myItemsCollectionView.dataSource = self
        myItemsCollectionView.delegate = self
        myItemsCollectionView.register(CustomItemColCell.self, forCellWithReuseIdentifier: cellId)
        myItemsCollectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        itemInfoView.addSubview(myItemsCollectionView)
        
        UIView.animate(withDuration: 0.15){
            self.profileView.alpha = 1.0
        }
        
    }
    
    // UI IF USER LOGGED OUT ------------------------------------
    func initViewIfNotConnected(){
        
        loginView = UIView(frame: CGRect(x: 0, y: 0, width: mainView.bounds.size.width, height: mainView.bounds.size.height - appDelegate().service.getBottomSafeAreaHeight() - appDelegate().service.getTopSafeAreaHeight() - 70))
        loginView.alpha = 0.0
        
        login.frame.size.height = loginView.frame.size.height
        login.frame.size.width = loginView.frame.size.width
        
        login.mainViewControllerInstance = mainViewControllerInstance
        
        loginView.addSubview(login)
        profileView.addSubview(loginView)
        
        UIView.animate(withDuration: 0.15){
            self.loginView.alpha = 1.0
        }
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    ALL ABOUT COLLECTION VIEW
    ///////////////////////////////////////////////////////////////
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allCells.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CustomItemColCell
        
        let aCell = allCells[indexPath.item]
        cell.aCell = aCell
        cell.setColor()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        let item = allCells[indexPath.item].item
        
        mainViewControllerInstance?.showItemView(item: item, isMine: true)
    }
}


//
//  PostView.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-01-29.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit
import AudioUnit

class PostView: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    var mainViewControllerInstance: MainViewController?
    
    let postViewWrapper = UIView()
    let postView = UIView()
    
    let topLabelView = UIView()
    let topLabel: UILabel = {
        let label = UILabel()
        label.text = "QUE VOULEZ VOUS VENDRE ?"
        label.textColor = UIColor.colorForText()
        label.font = UIFont(name:"HelveticaNeue", size: 12.0)
        label.textAlignment = .center
        
        return label
    }()
    
    let categoryButtonViewWrapper = UIView()
    let categoryButton = UIButton()
    
    let collectionViewWrapper = UIView()
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
    let cellId = "cellId"
    let cellHeight:CGFloat = 50
    
    var allCells: [CustomCategoryCell] = []
    
    //////////////////////////////////////////////////////////////////
    
    
    override init(frame: CGRect){
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        for i in allItemType {
            allCells.append(CustomCategoryCell(title: i))
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: NSNotification.Name("updateUIPostView"), object: nil)
        
        let window = UIApplication.shared.keyWindow
        let width: CGFloat = window!.frame.width
        let height: CGFloat = window!.frame.height - appDelegate().service.getTopSafeAreaHeight() - appDelegate().service.getBottomSafeAreaHeight() - 70 - 40
        
        postViewWrapper.frame = CGRect(x: 0, y: 0, width: width, height: height)
        postView.frame = postViewWrapper.frame
        
        topLabelView.frame = CGRect(x: 0, y: 0, width: width, height: 100)
        topLabel.frame = topLabelView.frame
        
        collectionViewWrapper.frame = postView.frame
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        collectionView.frame = CGRect(x: 20, y: topLabelView.frame.height, width: collectionViewWrapper.frame.size.width - 40, height: collectionViewWrapper.frame.size.height )
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.showsVerticalScrollIndicator = false
        collectionView.register(CustomCategoryColCell.self, forCellWithReuseIdentifier: cellId)
        
        topLabelView.addSubview(topLabel)
        postView.addSubview(topLabelView)
        
        collectionViewWrapper.addSubview(collectionView)
        postView.addSubview(collectionViewWrapper)
        
        postViewWrapper.addSubview(postView)
        addSubview(postViewWrapper)
        
        updateUI()
    }
    
    @objc func updateUI() {
        postViewWrapper.backgroundColor = UIColor.isLightModeBackground()
        postView.backgroundColor = UIColor.isLightModeBackground()
        collectionView.backgroundColor = UIColor.isLightModeBackground()
        
        topLabel.textColor = UIColor.colorForText()
        
        collectionView.reloadData()
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    ALL ABOUT COLLECTION VIEW
    ///////////////////////////////////////////////////////////////
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allCells.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CustomCategoryColCell
        
        let aCell = allCells[indexPath.item]
        cell.aCell = aCell
        cell.setColor()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        let alert = UIAlertController(title: "Vous n'êtes pas connecté !", message: "Pour vendre des articles, il faut vous authentifier.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Login", style: .default, handler: { action in
            NotificationCenter.default.post(name: NSNotification.Name("doLogin"), object: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Annuler", style: .cancel, handler: nil))
        
        if(!appDelegate().isLoggedIn()){
            self.mainViewControllerInstance?.present(alert, animated: true)
        }else{
            mainViewControllerInstance?.showPostView(category: allCells[indexPath.item].cellInfos)
        }
        
    }
    
}

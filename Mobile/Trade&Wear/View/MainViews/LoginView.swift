//
//  LoginView.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-02-07.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit
import AudioToolbox

class LoginView: UIView{

    var mainViewControllerInstance: MainViewController?

    @IBOutlet var mainView: UIView!
    
    @IBOutlet weak var loginText: UITextView!
    @IBOutlet weak var loginButton: TWButton!
    @IBOutlet weak var demoModeButton: TWButton!
    
    var picture: UIImageView = {
        let photo = UIImageView()
        photo.image = UIImage(named: "default_profile")
        return photo
    }()
    
    let semaphore = DispatchSemaphore(value: 0)
    
    //////////////////////////////////////////////////////////////////
    
    override init(frame: CGRect){
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: NSNotification.Name("updateUILoginView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loginButtonPressed), name: NSNotification.Name("doLogin"), object: nil)
        
        Bundle.main.loadNibNamed("LoginView", owner: self, options: nil)

        loginButton.setTitle("LOGIN", for: .normal)
        loginButton.layer.borderWidth = 2
        loginButton.addTarget(self, action: #selector(loginButtonPressed), for: .touchUpInside)
        
        //demoModeButton.setTitle("DEMO MODE", for: .normal)
        //demoModeButton.layer.borderWidth = 2
        //demoModeButton.addTarget(self, action: #selector(demoModeButtonPressed), for: .touchUpInside)
        demoModeButton.removeFromSuperview()
        
        addSubview(mainView)
        mainView.frame = self.bounds
        
        updateUI()
        
    }
    
    @objc func loginButtonPressed(sender: UIButton!) {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        if appDelegate().tokens == nil {
            appDelegate().authServer.authorize(viewController: mainViewControllerInstance!, useSfAuthSession: true/*self.sfAuthSessionSwitch.isOn*/, handler: { (success) in
                
                if !success {
                    print("ERROR: authorize failed.")
                }else{
                    self.checkState()
                    
                    self.semaphore.wait()
                    
                    if(appDelegate().loggedInUser != nil){
                        NotificationCenter.default.post(name: NSNotification.Name("showInBoarding"), object: nil)
                    }
                }
            })
        } else {
            appDelegate().logout()
        }
    }
    
    @objc func updateUI(){
        
        loginText.textColor = UIColor.colorForText()
        
        loginButton.backgroundColor = UIColor.isLightModeItem()
        loginButton.layer.borderColor = UIColor.isLightModeItem().cgColor
        loginButton.setTitleColor(UIColor.colorForText(), for: .normal)
        
        //demoModeButton.layer.borderColor = UIColor.colorForText().cgColor
        //demoModeButton.setTitleColor(UIColor.colorForText(), for: .normal)
        
        mainView.backgroundColor = UIColor.isLightModeBackground()
    }
    
    private func checkState() {
        if appDelegate().authServer.receivedCode != nil && appDelegate().authServer.receivedState != nil {
            
            appDelegate().authServer.getToken() { (tokens) in
                appDelegate().tokens = tokens
                
                if tokens != nil {
                    appDelegate().authServer.getProfile(accessToken: tokens!.accessToken, handler: { (profile) in
                        
                        ////// USER SUCCESSFULLY LOGGED IN //////
                        
                        let delimiter = "|"
                        let receivedUserId = profile?.userId
                        var result = receivedUserId?.components(separatedBy: delimiter)
                        let connectionMethod = result?[0]
                        let realUserId = result?[1]
                        
                        var tempUser = UserModel()
                        tempUser = appDelegate().service.getOneUser(userId: realUserId!)
                        
                        // If userId == nil -> create user
                        if(tempUser.userProfile.userId == nil){

                            let userToAdd = UserModel(
                                userId: realUserId!,
                                name: profile?.name ?? "",
                                surname: profile?.surname ?? "",
                                email: profile?.email ?? "",
                                birthDate: profile?.birthDate ?? "",
                                photo: self.picture.imageInBase64
                            )
                        
                            tempUser = appDelegate().service.addUser(user: userToAdd)
                            if(tempUser.userProfile.userId != nil){
                                appDelegate().service.updateUserPhoto(userId: tempUser.userProfile.userId!, photo: userToAdd.userProfile.photo!)
                                tempUser.userProfile.photo = appDelegate().service.getUserPhoto(userId: tempUser.userProfile.userId!)
                            }
                            
                        }
                        
                        if(tempUser.userProfile.userId != nil) {
                            appDelegate().setLoggedIn(userId: realUserId!)
                            appDelegate().setLoggedInUser(user: tempUser)
                            appDelegate().loggedInUser!.setMyItems(items: appDelegate().service.getMyItems(userId: realUserId!))
                            
                            DispatchQueue.main.async {
                                NotificationCenter.default.post(name: NSNotification.Name("updateLayoutProfileView"), object: nil)
                                appDelegate().service.updateEveryUI()
                            }
                            
                        } else { // Error connecting to DB
                            let alert = UIAlertController(title: "ERREUR", message: "Impossible de vous authentifier.", preferredStyle: .alert)

                            alert.addAction(UIAlertAction(title: "Fermer", style: .cancel, handler: nil))
                            
                            self.mainViewControllerInstance?.present(alert, animated: true)
                        }
                        
                        self.semaphore.signal()
                    })
                } else {
                    appDelegate().logout()
                }
            }
        }
    }
    
    @objc func demoModeButtonPressed(sender: UIButton!) {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        _ = appDelegate().service.demoMode()

        NotificationCenter.default.post(name: NSNotification.Name("updateLayoutProfileView"), object: nil)
        appDelegate().service.updateEveryUI()

    }
}


//
//  LargeFeedArticleCell.swift
//  Feed
//
//  Created by Marc Laberge on 2019-03-18.
//  Copyright © 2019 ML. All rights reserved.
//

import UIKit

class LargeFeedArticleCell: UICollectionViewCell {
    
    @IBOutlet var titre: UILabel!
    @IBOutlet var photo: UIImageView!
    @IBOutlet var price: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: NSNotification.Name("updateUIFeedCells"), object: nil)
        
        //mainView.layer.cornerRadius = 15.0
        //mainView.layer.borderWidth = 1.0
        //mainView.layer.borderColor = UIColor.clear.cgColor
        mainView.layer.masksToBounds = false
        
        //mainView.layer.shadowOffset = CGSize(width: 2, height: 2)
        //mainView.layer.shadowRadius = 3
        //mainView.layer.shadowOpacity = 0.1
        
        photo.contentMode = .scaleAspectFill
        photo.clipsToBounds = true
        //photo.layer.borderWidth = 4
        //photo.layer.borderColor = UIColor.clear.cgColor
        //photo.layer.cornerRadius = 9.0

        updateUI()
    }
    
    public func configure(with model: ArticleModel) {
        titre.text = model.titre.text
        price.text = model.price.text
        photo.image = model.photo.image
    }
    
    @objc func updateUI() {
        mainView.backgroundColor = UIColor.isLightModeItem()
        titre.textColor = UIColor.colorForTextSwap()
        price.textColor = UIColor.colorForTextSwap()
    }
    
}

struct ArticleModel {
    var titre: UILabel!
    var photo: UIImageView!
    var price: UILabel!
}


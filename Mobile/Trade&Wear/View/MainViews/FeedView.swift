//
//  FeedView.swift
//  Trade&Wear
//
//  Created by Marc Laberge on 2019-03-18.
//  Copyright © 2019 ML. All rights reserved.
//

import UIKit
import AudioToolbox
//import VegaScrollFlowLayout

class FeedView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    var mainViewControllerInstance: MainViewController?

    let cellId = "LargeFeedArticleCell"
    
    var allItems: [ItemModel] = []
    var data: [ArticleModel] = []
    
    lazy var feedCollectionView: UICollectionView = {

//        let layout = VegaScrollFlowLayout()
//        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    
        
        let layout = UICollectionViewFlowLayout()
        
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = UIColor.clear
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    
    ///////////////////////////////////////////////////////////////////////////////
    
    
    override init(frame: CGRect){
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: NSNotification.Name("updateUIFeedView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(fetchItems), name: NSNotification.Name("updateFeedItems"), object: nil)
        
        fetchItems()
        
        feedCollectionView.register(UINib.init(nibName: cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
        feedCollectionView.contentInset = .init(top: 18, left: 0, bottom: 60, right: 0)
        feedCollectionView.scrollIndicatorInsets = .init(top: 18, left: 0, bottom: 60, right: 0)
        feedCollectionView.isScrollEnabled = true
        feedCollectionView.allowsMultipleSelection = false
        feedCollectionView.showsHorizontalScrollIndicator = false
        feedCollectionView.showsVerticalScrollIndicator = false
        
        addSubview(feedCollectionView)
        addConstraintsWithFormat("H:|-15-[v0]-15-|", views: feedCollectionView)
        addConstraintsWithFormat("V:|-15-[v0]-108-|", views: feedCollectionView) // 108 = top bar is 88 + bottom safe area is 20

        updateUI()
    }
    
    @objc func fetchItems(){
        // **** TEMPORAIRE ------------
        data = []
        
        if(appDelegate().isLoggedIn()){
            allItems = appDelegate().service.getFeedItems(userId: (appDelegate().loggedInUser?.userProfile.userId)!)
            
            if(allItems.isEmpty){
                allItems = appDelegate().service.getAllItems()
            }
        }else{
            allItems = appDelegate().service.getAllItems()
        }
        
        for item in allItems {
            
            let titre = UILabel()
            titre.text = item.itemProfile.title
            
            let photo = UIImageView()
            photo.image = item.itemProfile.photos?.first?.value.base64InImage
            
            let price = UILabel()
            price.text = (item.itemProfile.price ?? "?")
            price.text?.append(" $")
            
            let article = ArticleModel(titre: titre, photo: photo, price: price)
            data.append(article)
        }
        
        feedCollectionView.reloadData()
        // **** TEMPORAIRE ------------
    }
    
    @objc func updateUI(){
        NotificationCenter.default.post(name: NSNotification.Name("updateUIFeedCells"), object: nil)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! LargeFeedArticleCell
        cell.configure(with: data[indexPath.row])
        
        return cell
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width - 30, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        let item = allItems[indexPath.item]
        
        mainViewControllerInstance?.showItemView(item: item, isMine: false)
    }
    
    
}


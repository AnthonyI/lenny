//
//  CameraView.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-03-12.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import Foundation
import UIKit
import AudioUnit

class CameraView: UIView, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate {

    var postViewControllerInstance: PostViewController?
    
    @IBOutlet var mainView: UIView!
    
    @IBOutlet weak var photosScrollView: UIScrollView!
    @IBOutlet weak var photosPageControl: UIPageControl!

    @IBOutlet weak var cameraIcon: UIImageView!
    @IBOutlet weak var textInfo: UITextView!
    
    let addPhotoImageView: UIImageView = {
        let i = UIImageView()
        return i
    }()
    
    var images: [UIImage] = []
    var imagesWithFilename: [String:String] = [:]
    var imagesToAdd: [String] = []
    
    
    //////////////////////////////////////////////////////////////////
    
    
    override init(frame: CGRect){
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(choosePhotos), name: NSNotification.Name("choosePhotosPressed"), object: nil)
        
        Bundle.main.loadNibNamed("CameraView", owner: self, options: nil)
        
        mainView.backgroundColor = UIColor.isLightModeBackground()
        
        photosPageControl.currentPage = 0
        photosPageControl.numberOfPages = images.count
        let pageNumber = round(photosScrollView.contentOffset.x / photosScrollView.frame.size.width)
        photosPageControl.currentPage = Int(pageNumber)
        photosPageControl.currentPageIndicatorTintColor = UIColor.colorForText()
        
        photosScrollView.delegate = self
        
        cameraIcon.tintColor = UIColor.colorForText()
        textInfo.textColor = UIColor.colorForText()
        
        addSubview(mainView)
        mainView.frame = self.frame
        mainView.clipsToBounds = true
        
        if(appDelegate().isLightModeCurrently!){
            addPhotoImageView.image = UIImage(named: "addPhoto_image_white")
        }else{
            addPhotoImageView.image = UIImage(named: "addPhoto_image_black")
        }
        
        DispatchQueue.main.async {
            self.initItemPhotos()
            self.loadPhotos()
        }
    }
    
    func initItemPhotos(){
        if((self.postViewControllerInstance?.isUpdatingItem)!){
            if(!(self.postViewControllerInstance?.itemInProcess.itemProfile.photos!.isEmpty)!){
                for photo in (self.postViewControllerInstance?.itemInProcess.itemProfile.photos!)! {
                    self.images.append(photo.value.base64InImage)
                    self.imagesWithFilename = self.postViewControllerInstance?.itemInProcess.itemProfile.photos ?? [:]
                }
            }
        }
        self.images.append(self.addPhotoImageView.image!)
    }
    
    func loadPhotos(){
        
        let photoWidth = photosScrollView.bounds.size.width
        
        let subViews = self.photosScrollView.subviews
        for subview in subViews{
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                subview.alpha = 0
            })
            subview.removeFromSuperview()
        }
        
        photosScrollView.isPagingEnabled = true
        photosScrollView.contentSize = CGSize(width: (photoWidth * CGFloat(images.count)), height: photosScrollView.bounds.size.height)
        photosScrollView.showsHorizontalScrollIndicator = false
        photosScrollView.clipsToBounds = false
        photosScrollView.delaysContentTouches = false
        photosScrollView.isExclusiveTouch = false
        
        for (index, photo) in images.enumerated(){
            
            let photoView = UIImageView()
            photoView.contentMode = .scaleAspectFill
            photoView.clipsToBounds = true
            photoView.frame.size.height = photosScrollView.bounds.size.height
            photoView.frame.size.width = photoWidth
            photoView.frame.origin.x = (CGFloat(index) * photoView.frame.size.width)
            photoView.image = photo
            photoView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            photoView.layer.borderWidth = 4
            photoView.layer.borderColor = UIColor.isLightModeBackground().cgColor
            photoView.layer.cornerRadius = 9.0
            photoView.tintColor = UIColor.lightGray()
            photoView.alpha = 0
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            photoView.isUserInteractionEnabled = true
            photoView.tag = index
            photoView.addGestureRecognizer(tapGestureRecognizer)
            
            photosScrollView.addSubview(photoView)
            
            if(index != images.count - 1){
                let removeImageButton = UIButton()
                
                removeImageButton.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
                removeImageButton.setImage(UIImage(named:"close_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
                removeImageButton.tintColor = .white
                removeImageButton.layer.masksToBounds = false
                removeImageButton.layer.cornerRadius = removeImageButton.frame.width / 2
                removeImageButton.layer.backgroundColor = UIColor.black.cgColor
                removeImageButton.titleEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
                removeImageButton.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
                removeImageButton.tag = index
                removeImageButton.addTarget(self, action: #selector(removePhotoPressed(sender:)), for: .touchUpInside)
                
                photoView.addSubview(removeImageButton)
            }

            if(index == 5){
                photoView.removeFromSuperview()
                photosScrollView.contentSize = CGSize(width: (photoWidth * CGFloat(images.count-1)), height: photosScrollView.bounds.size.height)
            }
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                photoView.alpha = 1
            })
        }
        
        photosPageControl.numberOfPages = images.count - 1
    }
    
    @objc func saveItemInfo() {
        
        var photoDic = [String:String]()
        var photoDic2 = [String:String]()
        
        images.remove(at: images.count - 1)
    
        if((self.postViewControllerInstance?.isUpdatingItem)!){
            
            photoDic2 = imagesWithFilename
            
            for photo in imagesToAdd {
                photoDic[appDelegate().service.addItemPhoto(itemId: (self.postViewControllerInstance?.itemInProcess.itemProfile.itemId)!, photo: photo)] = photo
            }
            
            photoDic2.map { photoDic[$0.0] = $0.1 } // Copy photoDic2 in photoDic
            
        }else{
            var counter: Int = 0
            for photo in images {
                let i = UIImageView()
                i.image = photo
                
                photoDic["\(counter)"] = i.imageInBase64
                counter += 1
            }
        }
    
        postViewControllerInstance?.itemInProcess.itemProfile.photos = photoDic
    }
    
    @objc func removePhotoPressed(sender: UIButton) {

        print(sender.tag)
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)

        if(!imagesToAdd.isEmpty){
            for i in 0...images.count {
                if(UIImageView(image: images[sender.tag]).imageInBase64 == imagesToAdd[i]){
                    imagesToAdd.remove(at: i)
                    break
                }else{
                    let dict = postViewControllerInstance?.itemInProcess.itemProfile.photos
                    let key: String = Array((dict?.keys)!)[sender.tag]
                    
                    appDelegate().service.deleteItemPhoto(filename: key)
                    
                    postViewControllerInstance?.itemInProcess.itemProfile.photos?.removeValue(forKey: key)
                    
                    imagesWithFilename.removeValue(forKey: key)
                    break
                }
            }
        }else{
            let dict = postViewControllerInstance?.itemInProcess.itemProfile.photos
            let key: String = Array((dict?.keys)!)[sender.tag]
            
            appDelegate().service.deleteItemPhoto(filename: key)
            
            postViewControllerInstance?.itemInProcess.itemProfile.photos?.removeValue(forKey: key)
            
            imagesWithFilename.removeValue(forKey: key)
        }

        images.remove(at: sender.tag)
        
        self.loadPhotos()
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        if(tappedImage.tag == images.count - 1){
            choosePhotos()
        }
    }
    
    @objc func choosePhotos() {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.navigationBar.tintColor = UIColor.colorForText()
        imagePickerController.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.colorForText()]
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePickerController.sourceType = .camera
                self.postViewControllerInstance?.present(imagePickerController, animated: true, completion: nil)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: {(action: UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                imagePickerController.sourceType = .photoLibrary
                self.postViewControllerInstance?.present(imagePickerController, animated: true, completion: nil)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Annuler", style: .cancel, handler: nil))
        
        self.postViewControllerInstance?.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        images.insert(image, at: images.count - 1)
        imagesToAdd.append(UIImageView(image: image).imageInBase64)
        
        photosPageControl.numberOfPages = images.count
        loadPhotos()
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageNumber = round(photosScrollView.contentOffset.x / photosScrollView.frame.size.width)
        photosPageControl.currentPage = Int(pageNumber)
        
        if(Int(pageNumber) < images.count){
            photosPageControl.currentPageIndicatorTintColor = UIColor.colorForText()
        }
    }
}

//
//  ItemInfoView.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-04-01.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import Foundation
import UIKit

class ItemInfoView: UIView, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate {
    
    @IBOutlet var mainViewWrapper: UIView!
    @IBOutlet weak var photosViewWrapper: UIView!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var scrollViewContentView: UIView!
    
    @IBOutlet weak var photosScrollView: UIScrollView!
    @IBOutlet weak var photosPageControl: UIPageControl!
    
    @IBOutlet weak var titreLabel: UILabel!
    var title: String = "TITLE"
    
    @IBOutlet weak var priceLabel: UILabel!
    var price: String = "$"
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var tailleView: UIView!
    @IBOutlet weak var tailleTitle: UILabel!
    @IBOutlet weak var tailleLabel: UILabel!
    var taille: String = ""
    
    @IBOutlet weak var couleurView: UIView!
    @IBOutlet weak var couleurTitle: UILabel!
    @IBOutlet weak var couleurLabel: UILabel!
    var couleur: String = ""
    
    @IBOutlet weak var styleView: UIView!
    @IBOutlet weak var styleTitle: UILabel!
    @IBOutlet weak var styleLabel: UILabel!
    var style: String = ""
    
    var images: [UIImage] = []
    
    //////////////////////////////////////////////////////////////////
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {

        Bundle.main.loadNibNamed("ItemInfoView", owner: self, options: nil)
        
        mainViewWrapper.backgroundColor = UIColor.isLightModeBackground()
        
        mainScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 75, right: 0);
        
        photosPageControl.currentPage = 0
        photosPageControl.numberOfPages = images.count
        let pageNumber = round(photosScrollView.contentOffset.x / photosScrollView.frame.size.width)
        photosPageControl.currentPage = Int(pageNumber)
        photosPageControl.currentPageIndicatorTintColor = UIColor.colorForText()
        
        self.photosScrollView.delegate = self
    
        descriptionLabel.textColor = UIColor.colorForText()
        tailleTitle.textColor = UIColor.colorForText()
        couleurTitle.textColor = UIColor.colorForText()
        styleTitle.textColor = UIColor.colorForText()
        
        addLineToView(view: tailleView, position:.LINE_POSITION_TOP, color: UIColor.colorForBorder(), width: 0.5)
        addLineToView(view: couleurView, position:.LINE_POSITION_TOP, color: UIColor.colorForBorder(), width: 0.5)
        addLineToView(view: styleView, position:.LINE_POSITION_TOP, color: UIColor.colorForBorder(), width: 0.5)
        addLineToView(view: styleView, position:.LINE_POSITION_BOTTOM, color: UIColor.colorForBorder(), width: 0.5)
        
        addSubview(mainViewWrapper)
        mainViewWrapper.frame = self.frame
    
    }
    
    func loadItemInfo(){
        
        // ITEM PHOTOS ----------
        
        let photoWidth = photosScrollView.bounds.size.width
        
        let subViews = self.photosScrollView.subviews
        for subview in subViews{
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                subview.alpha = 0
            })
            subview.removeFromSuperview()
        }
        
        photosScrollView.isPagingEnabled = true
        photosScrollView.contentSize = CGSize(width: (photoWidth * CGFloat(images.count)), height: photosScrollView.bounds.size.height)
        photosScrollView.showsHorizontalScrollIndicator = false
        photosScrollView.clipsToBounds = false
        photosScrollView.delaysContentTouches = false
        photosScrollView.isExclusiveTouch = false
        
        for (index, photo) in images.enumerated(){
            
            let photoView = UIImageView()
            photoView.frame.size.height = photosScrollView.bounds.size.height
            photoView.frame.size.width = photoWidth
            photoView.frame.origin.x = (CGFloat(index) * photoView.frame.size.width)
            photoView.contentMode = .scaleAspectFill
            photoView.clipsToBounds = true
            photoView.image = photo
            photoView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            photoView.layer.borderWidth = 4
            photoView.layer.borderColor = UIColor.isLightModeBackground().cgColor
            photoView.layer.cornerRadius = 9.0
            photoView.tintColor = UIColor.lightGray()
            photoView.alpha = 0
            
            photosScrollView.addSubview(photoView)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                photoView.alpha = 1
            })
        }
        
        photosPageControl.numberOfPages = images.count
        
        // ITEM TITLE ----------
        titreLabel.text = title
        titreLabel.textColor = UIColor.colorForText()
        
        // ITEM PRICE ----------
        priceLabel.text = price + " $"
        priceLabel.textColor = UIColor.colorForText()
        
        // ITEM SIZE  ----------
        tailleLabel.text = taille
        tailleLabel.textColor = UIColor.colorForText()
        
        // ITEM COLOR ----------
        couleurLabel.text = couleur
        couleurLabel.textColor = UIColor.colorForText()
        
        // ITEM STYLE ----------
        styleLabel.text = style
        styleLabel.textColor = UIColor.colorForText()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageNumber = round(photosScrollView.contentOffset.x / photosScrollView.frame.size.width)
        photosPageControl.currentPage = Int(pageNumber)
        
        if(Int(pageNumber) < images.count){
            photosPageControl.currentPageIndicatorTintColor = UIColor.colorForText()
        }
    }
}

//
//  CustomItemCell.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-03-20.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit

class CustomItemCell: NSObject {
    var item: ItemModel
    var title: String
    var price: String
    var image: UIImage?
    
    init(item: ItemModel){
        self.item = item
        self.title = item.itemProfile.title!
        self.price = item.itemProfile.price! + " $"
        
        if(!(item.itemProfile.photos?.isEmpty)!){
            let dict = item.itemProfile.photos
            let value: String = Array((dict?.values)!)[0]
            self.image = value.base64InImage
        }else{
            self.image = nil
        }
        
    }
}

class CustomItemColCell: UICollectionViewCell {
    
    override var isHighlighted: Bool {
        didSet{
            nameLabel.textColor = isHighlighted ? UIColor.lightGray() : UIColor.colorForText()
        }
    }
    
    var aCell: CustomItemCell? {
        didSet {
            nameLabel.text = aCell?.title
            priceLabel.text = aCell?.price
            
            if let imageName = aCell?.image {
                itemImageView.image = imageName
            }
        }
    }
    
    func setColor() {
        nameLabel.textColor = isHighlighted ? UIColor.lightGray() : UIColor.colorForText()
        priceLabel.textColor = isHighlighted ? UIColor.lightGray() : UIColor.colorForText()
    }
    
    let itemImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "TITLE"
        label.textColor = UIColor.colorForText()
        label.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        return label
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.text = "PRICE"
        label.textColor = UIColor.colorForText()
        label.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    func setupViews(){
        
        addSubview(itemImageView)
        addSubview(nameLabel)
        addSubview(priceLabel)
        
        addConstraintsWithFormat("H:|[v0(40)]-15-[v1]-[v2]|", views: itemImageView, nameLabel, priceLabel)
        addConstraintsWithFormat("V:[v0(40)]", views: itemImageView)
        addConstraintsWithFormat("V:|[v0]|", views: nameLabel)
        addConstraintsWithFormat("V:|[v0]|", views: priceLabel)
        
        addConstraint(NSLayoutConstraint(item: itemImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

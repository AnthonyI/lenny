//
//  CustomCategoryCell.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-03-18.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit

class CustomCategoryCell: NSObject {
    var cellInfos: itemType
    
    init(title: itemType){
        self.cellInfos = title
    }
}

class CustomCategoryColCell: UICollectionViewCell {
    
    override var isHighlighted: Bool {
        didSet{
            nameLabel.textColor = isHighlighted ? UIColor.lightGray() : UIColor.colorForText()
        }
    }
    
    var aCell: CustomCategoryCell? {
        didSet {
            let text = aCell?.cellInfos.rawValue.0 // This is the title
            nameLabel.text = text!.uppercased()
            
            nameLabel.frame = CGRect(x: 0, y: 0, width: nameLabel.intrinsicContentSize.width + 30, height: 30)
            nameLabel.center.x = self.center.x
        }
    }
    
    func setColor() {
        nameLabel.textColor = isHighlighted ? UIColor.lightGray() : UIColor.colorForText()
        nameLabel.backgroundColor = UIColor.isLightModeItem()
        nameLabel.layer.borderColor = UIColor.isLightModeItem().cgColor
    }
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "TITLE"
        label.backgroundColor = UIColor.isLightModeItem()
        label.textColor = UIColor.colorForText()
        label.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        label.textAlignment = .center
        label.layer.borderWidth = 2
        label.layer.borderColor = UIColor.isLightModeItem().cgColor
        
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(nameLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

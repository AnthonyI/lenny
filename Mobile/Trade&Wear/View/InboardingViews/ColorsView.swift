//
//  ColorView.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-02-20.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit
import AudioToolbox

class ColorsView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var inBoardingViewControllerInstance: InBoardingViewController?
    
    var allColorButtons: [UIColor] = [/*UIColor.themeBackgroudWhite()*/.white, /*UIColor.backgroundBlack()*/.black, UIColor.themeBackgroudOrange(), UIColor.themeBackgroudBlue(), UIColor.themeBackgroudRed(), UIColor.themeBackgroudGreen()
    ]
    var allColorAsString: [String] = ["White", "Black", "Orange", "Blue", "Red", "Green"]
    
    @IBOutlet var mainView: UIView!
    
    @IBOutlet weak var previewAppView: UIImageView!
    @IBOutlet weak var colorsWrapper: UIView!
    @IBOutlet weak var themeLabel: UILabel!
    
    var isLightMode: Bool!
    //let lightModeDarkModeButton = UIButton()
    
    var chosenColor = UIColor.themeBackgroudWhite()
    var chosenColorAsString = "White"
    
    lazy var colorsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        view.backgroundColor = UIColor.clear
        view.dataSource = self
        view.delegate = self
        
        return view
    }()
    
    
    let cellId = "cellId"
    
    
    //////////////////////////////////////////////////////////////////
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: NSNotification.Name("updateUIColorsView"), object: nil)

        self.autoresizesSubviews = true
        
        Bundle.main.loadNibNamed("ColorView", owner: self, options: nil)
        
        isLightMode = !(appDelegate().loggedInUser?.userPreferences.lightMode)!
        chosenColorAsString = (appDelegate().loggedInUser?.userPreferences.color)!

        //colorsCollectionView.frame = CGRect(x: 0, y: 0, width: colorsWrapper.frame.size.width, height: 120)
        //colorsCollectionView.register(ColorCell.self, forCellWithReuseIdentifier: cellId)
        //colorsCollectionView.isScrollEnabled = false
       
        //colorsWrapper.addSubview(colorsCollectionView)
        //mainView.addSubview(colorsWrapper)
        
        addSubview(mainView)
        mainView.frame = self.bounds
        mainView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        //selectAlreadySelectedColor()
        
        initUIColor()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allColorButtons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ColorCell
        cell.imageView.tintColor = allColorButtons[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.bounds.width/3.0
        return CGSize(width: cellWidth, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        chosenColor = allColorButtons[indexPath.item]
        chosenColorAsString = allColorAsString[indexPath.item]
    }
    
    /*func selectAlreadySelectedColor(){
        for i in 0...allColorAsString.count-1{
            if(chosenColorAsString == allColorAsString[i]){
                chosenColor = allColorButtons[i]
                
                let indexPath = IndexPath(item: i, section: 0)
                colorsCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: [])
            }
        }
    }*/
    
    @objc func initUIColor(){
        if((appDelegate().loggedInUser?.userPreferences.lightMode)!){  // Light mode on
            isLightMode = true
            mainView.backgroundColor = UIColor.themeBackgroudWhite()
            mainView.backgroundColor = UIColor.themeBackgroudWhite()
            themeLabel.textColor = .black
            previewAppView.image = UIImage(named: "lightModePreview")
            
        }else{  // Dark mode on
            isLightMode = false
            mainView.backgroundColor = UIColor.themeBackgroudBlack()
            mainView.backgroundColor = UIColor.themeBackgroudBlack()
            themeLabel.textColor = .white
            previewAppView.image = UIImage(named: "darkModePreview")
        }
    }
    
    @objc func updateUI(){
        if(!isLightMode){  // Light mode on
            isLightMode = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.previewAppView.alpha = 0
            })
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.previewAppView.image = UIImage(named: "lightModePreview")
                self.previewAppView.alpha = 1
            })
            
            
        }else{  // Dark mode on
            isLightMode = false
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.previewAppView.alpha = 0
            })
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.previewAppView.alpha = 0
                self.previewAppView.image = UIImage(named: "darkModePreview")
                self.previewAppView.alpha = 1
            })
        }
        
        saveProfileInfo()
        //selectAlreadySelectedColor()
    }
    
    @objc func saveProfileInfo() {

        var profile = inBoardingViewControllerInstance?.userInProcess.userPreferences
        profile!.color = chosenColorAsString
        profile!.lightMode = isLightMode
        profile!.style = inBoardingViewControllerInstance?.userInProcess.userPreferences.style

        inBoardingViewControllerInstance?.userInProcess.userPreferences = profile!
        
    }
}

class ColorCell: UICollectionViewCell {
    
    private var cellView = UIView()
    let imageView: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "color_icon")?.withRenderingMode(.alwaysTemplate)
        
        i.layer.borderWidth = 3
        i.layer.masksToBounds = false
        i.layer.borderColor = UIColor.clear.cgColor
        i.layer.cornerRadius = 25
        i.clipsToBounds = true
        
        return i
    }()
    
    override var isSelected: Bool {
        didSet{
            imageView.layer.borderColor = isSelected ? UIColor.colorForText().cgColor : UIColor.clear.cgColor
        }
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        addSubview(cellView)
        cellView.frame = self.bounds
        
        addSubview(imageView)
        addConstraintsWithFormat("H:[v0(50)]", views: imageView)
        addConstraintsWithFormat("V:[v0(50)]", views: imageView)
        
        addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
}

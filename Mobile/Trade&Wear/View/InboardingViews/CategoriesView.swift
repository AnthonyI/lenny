//
//  CategoryView.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-02-20.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//


import UIKit
import AudioToolbox

class CategoriesView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var inBoardingViewControllerInstance: InBoardingViewController?
    
    var allCategoriesButtons: [String] = []
    
    @IBOutlet var mainViewWrapper: UIView!
    @IBOutlet weak var mainView: UIView!
    
    lazy var categoriesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        view.backgroundColor = UIColor.clear
        view.dataSource = self
        view.delegate = self
        
        return view
    }()
    
    let topLabelView = UIView()
    let topLabel: UILabel = {
        let label = UILabel()
        label.text = "CHOISISSEZ AU MINIMUM 3 STYLES"
        label.textColor = UIColor.colorForText()
        label.font = UIFont(name:"HelveticaNeue", size: 12.0)
        label.textAlignment = .center
        
        return label
    }()
    
    var chosenStyles = [String]()
    let cellId = "cellId"
    
    
    //////////////////////////////////////////////////////////////////
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        
        for i in allItemStyle {
            allCategoriesButtons.append(i.rawValue)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: NSNotification.Name("updateUICategoriesView"), object: nil)
        
        self.autoresizesSubviews = true
        
        Bundle.main.loadNibNamed("CategoriesView", owner: self, options: nil)
        
        chosenStyles = (appDelegate().loggedInUser?.userPreferences.style)!
        
        mainViewWrapper.frame = self.bounds
        mainViewWrapper.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        topLabelView.frame = CGRect(x: 0, y: 0, width: mainView.frame.size.width, height: 100)
        topLabel.frame = topLabelView.frame
        
        categoriesCollectionView.register(CategorieCell.self, forCellWithReuseIdentifier: cellId)
        categoriesCollectionView.frame = CGRect(x: 20, y: topLabelView.frame.height, width: mainView.frame.size.width-20, height: mainView.frame.size.height)
        categoriesCollectionView.center.x = mainView.center.x
        categoriesCollectionView.isScrollEnabled = true
        categoriesCollectionView.allowsMultipleSelection = true
        
        topLabelView.addSubview(topLabel)
        mainView.addSubview(topLabelView)
        
        mainView.addSubview(categoriesCollectionView)
        
        addSubview(mainViewWrapper)
        
        updateUI()
    }
    
    func selectAlreadySelectedCategories(){
        for i in 0...allCategoriesButtons.count-1{
            if(chosenStyles.contains(allCategoriesButtons[i])){
                let indexPath = IndexPath(item: i, section: 0)
                categoriesCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: [])
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allCategoriesButtons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CategorieCell
        cell.textView.text = allCategoriesButtons[indexPath.item]
        cell.textView.textColor = UIColor.colorForText()
        cell.backgroundColor = UIColor.isLightModeItem()
        cell.layer.cornerRadius = 8.0
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.clear.cgColor
        cell.layer.masksToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.bounds.width/2.0
        let cellHeight = cellWidth/2
        
        return CGSize(width: cellWidth-5, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        let selectedCategorie = allCategoriesButtons[indexPath.item]
        
        if(!chosenStyles.contains(selectedCategorie)){
            chosenStyles.append(selectedCategorie)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        let selectedCategorie = allCategoriesButtons[indexPath.item]
        
        if(chosenStyles.contains(selectedCategorie)){
            chosenStyles.removeAll { $0 == selectedCategorie }
        }
    }
    
    @objc func updateUI(){
        mainView.backgroundColor = UIColor.isLightModeBackground()
        for i in 0...allCategoriesButtons.count-1{
            let indexPath = IndexPath(item: i, section: 0)
            categoriesCollectionView.cellForItem(at: indexPath)?.backgroundColor = UIColor.isLightModeItem()
        }
        
        selectAlreadySelectedCategories()
    }
    
    @objc func saveProfileInfo() {
        
        var profile = inBoardingViewControllerInstance?.userInProcess.userPreferences
        profile!.color = inBoardingViewControllerInstance?.userInProcess.userPreferences.color
        profile!.lightMode = inBoardingViewControllerInstance?.userInProcess.userPreferences.lightMode
        profile?.style = chosenStyles
        
        inBoardingViewControllerInstance?.userInProcess.userPreferences = profile!
    }
}

class CategorieCell: UICollectionViewCell {
    
    private var cellView = UIView()
    let textView: UILabel = {
        let i = UILabel()
        i.text = "UNDEFINED"
        i.textAlignment = .center
        return i
    }()
    
    override var isSelected: Bool {
        didSet{
            textView.backgroundColor = isSelected ? UIColor.lightGray() : UIColor.isLightModeItem()
        }
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        addSubview(cellView)
        cellView.frame = self.bounds
        
        addSubview(textView)
        textView.frame = self.bounds
    }
    
}


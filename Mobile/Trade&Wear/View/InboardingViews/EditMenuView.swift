//
//  EditMenuView.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-02-10.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox

class EditMenuView: UIView, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    var inBoardingViewControllerInstance: InBoardingViewController?
    
    var userPhotoNeedsUpdate: Bool = false
    
    @IBOutlet weak var profilePhotoView: UIImageView!
    @IBOutlet weak var editImageButton: UIButton!
    
    @IBOutlet var mainView: UIView!
    
    @IBOutlet weak var mainScrollVIew: UIScrollView!
    
    @IBOutlet weak var pageTitle: UILabel!
    
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var birthTextField: UITextField!
    
    let datePicker = UIDatePicker()
    let imagePicker = UIImagePickerController()
    
    var keyboardHeight: CGFloat = 0.0
    
    
    //////////////////////////////////////////////////////////////////
    
    
    override init(frame: CGRect){
        super.init(frame: frame)
        
        self.hideKeyboardWhenTappedAround()
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: NSNotification.Name("updateUIEditMenuView"), object: nil)
        
        self.autoresizesSubviews = true
        
        Bundle.main.loadNibNamed("EditMenuView", owner: self, options: nil)
        
        mainScrollVIew.backgroundColor = UIColor.themeItemWhite()
        mainScrollVIew.layer.borderWidth = 2
        mainScrollVIew.layer.borderColor = UIColor.themeItemWhite().cgColor
        mainScrollVIew.layer.cornerRadius = 8.0
        
        profilePhotoView.contentMode = .scaleAspectFill
        profilePhotoView.layer.borderWidth = 2
        profilePhotoView.layer.masksToBounds = false
        profilePhotoView.layer.borderColor = UIColor.clear.cgColor
        profilePhotoView.layer.cornerRadius = 8.0
        profilePhotoView.clipsToBounds = true
        profilePhotoView.image = UIImage(named: "default_profile")

        editImageButton.layer.borderWidth = 2
        editImageButton.layer.masksToBounds = false
        editImageButton.layer.borderColor = UIColor.themeItemWhite().cgColor
        editImageButton.layer.cornerRadius = 8.5
        editImageButton.backgroundColor = UIColor.themeItemWhite()
        
        if(appDelegate().loggedInUser?.userProfile.surname != nil){
            surnameTextField.text = appDelegate().loggedInUser?.userProfile.surname
        }
        surnameTextField.autocorrectionType = UITextAutocorrectionType.no
        surnameTextField.keyboardType = UIKeyboardType.default
        surnameTextField.returnKeyType = UIReturnKeyType.done
        surnameTextField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        surnameTextField.delegate = self
        surnameTextField.backgroundColor = .clear
        surnameTextField.tintColor = .black
        surnameTextField.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        surnameTextField.autocapitalizationType = .none
        surnameTextField.setLeftPaddingPoints(10)
        addLineToView(view: surnameTextField, position:.LINE_POSITION_BOTTOM, color: .black, width: 0.6)

        if(appDelegate().loggedInUser?.userProfile.name != nil){
            nameTextField.text = appDelegate().loggedInUser?.userProfile.name
        }
        nameTextField.autocorrectionType = UITextAutocorrectionType.no
        nameTextField.keyboardType = UIKeyboardType.default
        nameTextField.returnKeyType = UIReturnKeyType.done
        nameTextField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        nameTextField.delegate = self
        nameTextField.backgroundColor = .clear
        nameTextField.tintColor = .black
        nameTextField.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        nameTextField.autocapitalizationType = .none
        nameTextField.setLeftPaddingPoints(10)
        addLineToView(view: nameTextField, position:.LINE_POSITION_BOTTOM, color: .black, width: 0.6)
        
        if(appDelegate().loggedInUser?.userProfile.email != nil){
            emailTextField.text = appDelegate().loggedInUser?.userProfile.email
        }
        emailTextField.autocorrectionType = UITextAutocorrectionType.no
        emailTextField.keyboardType = UIKeyboardType.default
        emailTextField.returnKeyType = UIReturnKeyType.done
        emailTextField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        emailTextField.delegate = self
        emailTextField.backgroundColor = .clear
        emailTextField.tintColor = .black
        emailTextField.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        emailTextField.autocapitalizationType = .none
        emailTextField.setLeftPaddingPoints(10)
        addLineToView(view: emailTextField, position:.LINE_POSITION_BOTTOM, color: .black, width: 0.6)
        
        if(appDelegate().loggedInUser?.userProfile.birthDate != nil){
            //birthTextField.text = appDelegate.profile?.birthDate
            
            let dateString = appDelegate().loggedInUser?.userProfile.birthDate
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.timeZone = TimeZone.autoupdatingCurrent
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date = dateFormatter.date(from: dateString!)
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "MMM dd, yyyy"

            datePicker.date = date!
            birthTextField.text = dateFormatter2.string(from: date!)
        }
        birthTextField.autocorrectionType = UITextAutocorrectionType.no
        birthTextField.keyboardType = UIKeyboardType.default
        birthTextField.returnKeyType = UIReturnKeyType.done
        birthTextField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        birthTextField.delegate = self
        birthTextField.backgroundColor = .clear
        
        birthTextField.tintColor = .black
        birthTextField.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        birthTextField.autocapitalizationType = .none
        birthTextField.setLeftPaddingPoints(10)
        addLineToView(view: birthTextField, position:.LINE_POSITION_BOTTOM, color: .black, width: 0.6)

        mainView.frame = self.bounds
        mainView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    
        addSubview(mainView)
        
        showDatePicker()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        editImageButton.addGestureRecognizer(tapGestureRecognizer)
        
        profilePhotoView.isUserInteractionEnabled = true
        profilePhotoView.addGestureRecognizer(tapGestureRecognizer2)
        
        imagePicker.delegate = self
        
        profilePhotoView.image = (appDelegate().loggedInUser?.userProfile.photo)?.base64InImage
        
        initUI()
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        let currentController = self.getCurrentViewController()
        imagePicker.navigationBar.barTintColor = .white
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        currentController?.present(imagePicker, animated: true, completion: nil)
        
    }
    
    func getCurrentViewController() -> UIViewController? {
        
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let selectedImage = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        profilePhotoView.image = selectedImage
        
        if(inBoardingViewControllerInstance != nil){
            inBoardingViewControllerInstance!.userPhotoNeedsUpdate = true
        }else{
            userPhotoNeedsUpdate = true
        }
        
        let currentController = self.getCurrentViewController()
        currentController?.dismiss(animated: true, completion: nil)
    
    }
    
    @objc func initUI(){
        if((appDelegate().loggedInUser?.userPreferences.lightMode)!){ // LIGHT MODE ON
            self.mainView.backgroundColor = UIColor.themeBackgroudWhite()
            self.pageTitle.textColor = .black
        }else{ // DARK MODE ON
            self.mainView.backgroundColor = UIColor.themeBackgroudBlack()
            self.pageTitle.textColor = .white
        }
    }
    
    @objc func updateUI(){
        if(inBoardingViewControllerInstance!.userInProcess.userPreferences.lightMode ?? true){ // LIGHT MODE ON
            self.mainView.backgroundColor = UIColor.themeBackgroudWhite()
            self.pageTitle.textColor = .black
        }else{ // DARK MODE ON
            self.mainView.backgroundColor = UIColor.themeBackgroudBlack()
            self.pageTitle.textColor = .white
        }
    }
    
    @objc func saveProfileInfo() {
        
        let userId = appDelegate().loggedInUser?.userProfile.userId
        var name = appDelegate().loggedInUser?.userProfile.name ?? ""
        var surname = appDelegate().loggedInUser?.userProfile.surname ?? ""
        var email = appDelegate().loggedInUser?.userProfile.email ?? ""
        var birthDate = appDelegate().loggedInUser?.userProfile.birthDate ?? ""

        if(!nameTextField.text!.isEmpty){
            name = nameTextField.text!
        }
        if(!surnameTextField.text!.isEmpty){
            surname = surnameTextField.text!
        }
        if(!emailTextField.text!.isEmpty){
            email = emailTextField.text!
        }
        if(!birthTextField.text!.isEmpty){
            birthDate = birthTextField.text!
        }
        
        let userToUpdate = UserModel(
            userId: userId!,
            name: name,
            surname: surname,
            email: email,
            birthDate: birthDate,
            photo: profilePhotoView.imageInBase64
        )
        userToUpdate.setUserPreferences(userPreferences: (appDelegate().loggedInUser?.userPreferences)!)
        
        // We are in the InboardingController
        if(inBoardingViewControllerInstance != nil){
            inBoardingViewControllerInstance?.userInProcess = userToUpdate
            
        // We are in the ModifyProfileController
        }else{
            
            // Update user info
            let user = appDelegate().service.updateUser(user: userToUpdate)
            
            // Update user photo
            let userPhoto = userToUpdate.userProfile.photo
            if(userPhotoNeedsUpdate && (userPhoto != nil || userPhoto != "")){
                appDelegate().service.updateUserPhoto(userId: user.userProfile.userId!, photo: userToUpdate.userProfile.photo!)
            }
            user.userProfile.photo = userPhoto
            
            // Set user items
            user.setMyItems(items: (appDelegate().loggedInUser?.myItems)!)
            
            // Updated user besomes loggedInUser
            appDelegate().setLoggedInUser(user: user)
        }
    
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    DATE PICKER
    ///////////////////////////////////////////////////////////////
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.isTranslucent = false
        toolbar.barTintColor = UIColor.isLightModeItem()
        
        let cancelButton = UIBarButtonItem(title: "Annuler", style: .plain, target: self, action: #selector(cancelDatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Terminer", style: .plain, target: self, action: #selector(doneDatePicker))
        
        cancelButton.tintColor = UIColor.colorForText()
        doneButton.tintColor = UIColor.colorForText()
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        birthTextField.inputAccessoryView = toolbar
        birthTextField.inputView = datePicker
        
        datePicker.backgroundColor = UIColor.isLightModeItem()
        datePicker.setValue(UIColor.colorForText(), forKeyPath: "textColor")
        
    }
    @objc func doneDatePicker(){
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.dateFormat = "MMM dd, yyyy"
        birthTextField.text = dateFormatter.string(from: datePicker.date)
        endEditing(true)
    }
    @objc func cancelDatePicker(){
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        endEditing(true)
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        keyboardHeight = keyboardFrame.height
        
        var contentInset:UIEdgeInsets = self.mainScrollVIew.contentInset
        contentInset.bottom = keyboardHeight
        mainScrollVIew.contentInset = contentInset
        
    }
    @objc func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        mainScrollVIew.contentInset = contentInset
        
    }
}

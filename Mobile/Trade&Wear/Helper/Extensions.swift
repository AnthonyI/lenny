//
//  Extensions.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-01-28.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit

func appDelegate() -> AppDelegate
{
    return UIApplication.shared.delegate as! AppDelegate
}

extension UIFont {
    func sizeOfString(string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRect(with: CGSize(width: width, height: DBL_MAX),
                                                     options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                     attributes: [NSAttributedString.Key.font: self],
                                                     context: nil).size
    }
}

extension Date {
    var ticks: Int64 {
        return Int64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}

extension UIImageView {
    func load(url: URL) {
        if let data = try? Data(contentsOf: url) {
            if let image = UIImage(data: data) {
                self.image = image
            }
        }
    }
    
    var imageInBase64: String {
        get {
            var base64String = ""
            let cim = CIImage(image: self.image ?? UIImage())
            if (cim != nil) {
                let imageData = self.image?.jpeg(.medium)
                base64String = (imageData?.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))!
            }
            return base64String
        }
    }
}

extension String {
    var base64InImage: UIImage {
        get {
            var img: UIImage = UIImage()
            if (!self.isEmpty) {
                if let decodedData = Data(base64Encoded: self , options: NSData.Base64DecodingOptions.ignoreUnknownCharacters) {
                    let decodedimage = UIImage(data: decodedData)
                    img = (decodedimage as UIImage?)!
                }
                
            }
            return img
        }
    }
}

// CODE DE COULEURS DE L'APP ------------------------

//  DARKER GRAY:    #161314
//  DARK GRAY:      #2d2d2d
//  LIGHT GRAY:     #a0a0a0

// --------------------------------------------------

extension UIColor {
    
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
    
    static func darkerGray() -> UIColor { return UIColor(red:0.09, green:0.07, blue:0.08, alpha:1.0) }
    static func darkGray() -> UIColor { return UIColor(red:0.18, green:0.18, blue:0.18, alpha:1.0) }
    static func lightGray() -> UIColor { return UIColor(red:0.63, green:0.63, blue:0.63, alpha:1.0) }
    
    //static func backgroundBlack() -> UIColor { return UIColor.rgb(red: 25, green: 28, blue: 34, alpha: 1) }
    static func backgroundGray() -> UIColor { return UIColor.rgb(red: 250, green: 250, blue: 250, alpha: 1) }
    
    static func themeBackgroudWhite()   -> UIColor { return #colorLiteral(red: 0.9516589679, green: 0.9470615816, blue: 0.9562563542, alpha: 1) }
    static func themeBackgroudBlack()   -> UIColor { return #colorLiteral(red: 0.1028630063, green: 0.1215652898, blue: 0.1474581957, alpha: 1) }
    static func themeBackgroudOrange()  -> UIColor { return #colorLiteral(red: 0.8245621324, green: 0.3067413568, blue: 0.0956575349, alpha: 1) }
    static func themeBackgroudBlue()    -> UIColor { return #colorLiteral(red: 0.1619981825, green: 0.4780096412, blue: 0.7297570705, alpha: 1) }
    static func themeBackgroudRed()     -> UIColor { return #colorLiteral(red: 0.5843881965, green: 0.019526219, blue: 0.1596390009, alpha: 1) }
    static func themeBackgroudGreen()   -> UIColor { return #colorLiteral(red: 0.1639354825, green: 0.5395297408, blue: 0.3757186532, alpha: 1) }
    
    static func themeItemWhite()        -> UIColor { return #colorLiteral(red: 0.999904573, green: 1, blue: 0.9998808503, alpha: 1) }
    static func themeItemBlack()        -> UIColor { return #colorLiteral(red: 0.1531499028, green: 0.1764166951, blue: 0.2109666169, alpha: 1) }
    static func themeItemOrange()       -> UIColor { return #colorLiteral(red: 0.8626320958, green: 0.3240494132, blue: 0.1058363393, alpha: 1) }
    static func themeItemBlue()         -> UIColor { return #colorLiteral(red: 0.1714869738, green: 0.5093082786, blue: 0.7781248689, alpha: 1) }
    static func themeItemRed()          -> UIColor { return #colorLiteral(red: 0.6183231473, green: 0.01920981333, blue: 0.1800200045, alpha: 1) }
    static func themeItemGreen()        -> UIColor { return #colorLiteral(red: 0.1778595448, green: 0.5833924413, blue: 0.4104744792, alpha: 1) }
    
    static func isLightModeBackground() -> UIColor {
        if(appDelegate().isLightModeCurrently!){ // LIGHT MODE ON
            return UIColor.themeBackgroudWhite()
        }else{ // DARK MODE ON
            return UIColor.themeBackgroudBlack()
        }
    }
    
    static func isLightModeItem() -> UIColor {
        if(appDelegate().isLightModeCurrently!){ // LIGHT MODE ON
            return UIColor.themeItemWhite()
        }else{ // DARK MODE ON
            return UIColor.themeItemBlack()
        }
    }
    
    static func colorForText() -> UIColor {
        if(appDelegate().isLightModeCurrently!){ // LIGHT MODE ON
            return .black
        }else{ // DARK MODE ON
            return .white
        }
    }
    static func colorForTextSwap() -> UIColor {
        if(appDelegate().isLightModeCurrently!){ // LIGHT MODE ON
            return .white
        }else{ // DARK MODE ON
            return .black
        }
    }
    
    static func colorForBorder() -> UIColor {
        if(appDelegate().isLightModeCurrently!){ // LIGHT MODE ON
            return lightGray()
        }else{ // DARK MODE ON
            return themeItemBlack()
        }
    }
    
    static func getFavoriteColor() -> UIColor {
        
        switch appDelegate().loggedInUser?.userPreferences.color {
            
        case "White":
            return themeItemWhite()
        case "Black":
            return themeItemBlack()
        case "Orange":
            return themeItemOrange()
        case "Blue":
            return themeItemBlue()
        case "Red":
            return themeItemRed()
        case "Green":
            return themeItemGreen()
        default:
            return themeItemWhite()
        }
        
    }
    
    static func colorForStatusBar() {
        if(appDelegate().isLightModeCurrently!){ // LIGHT MODE ON
            UIApplication.shared.statusBarStyle = .default
        }else{ // DARK MODE ON
            UIApplication.shared.statusBarStyle = .lightContent
        }
    }
}

extension UIView {
    
    ///////////////////////////////////////////////////////////////
    //    ADD LINE BORDER TOP OR BOTTOM
    ///////////////////////////////////////////////////////////////
    enum LINE_POSITION {
        case LINE_POSITION_TOP
        case LINE_POSITION_BOTTOM
    }
    func addLineToView(view : UIView, position : LINE_POSITION, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        view.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
    
    ///////////////////////////////////////////////////////////////
    //    CUSTOM FUNCTION FOR CONSTRAINTS
    ///////////////////////////////////////////////////////////////
    func addConstraintsWithFormat(_ format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
       endEditing(true)
    }
}

extension UITextField {
    
    ///////////////////////////////////////////////////////////////
    //    ADD PADDING TO UITEXTFIELD
    ///////////////////////////////////////////////////////////////
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


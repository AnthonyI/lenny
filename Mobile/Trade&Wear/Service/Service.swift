//
//  Service.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-01-28.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit

class Service: NSObject{

    //var urlPath = "http://localhost:8080" // Local
    var urlPath = "http://34.70.8.24:8080" // Server
    //var urlPath = "http://10.238.187.165:8080" // My mac
    //var urlPath = "http://10.238.169.114:8080" // Antoine
    
    
    //////////////////////////////////////////////////////////////////////////
    
    
    ///////////////////////////////////////////////////////////////
    //    USERS
    ///////////////////////////////////////////////////////////////
    
    // GET ALL USERS
    func getAllUsers(){

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: urlPath + "/users/get_all_users")!
        let task = session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                var jsonResult = NSArray()
                
                do{
                    jsonResult = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                } catch let error as NSError {
                    print(error)
                }
                
                var jsonElement = NSDictionary()
                
                for i in 0 ..< jsonResult.count{
                    jsonElement = jsonResult[i] as! NSDictionary
                    
                    if  let userId = jsonElement["userId"] as? String,
                        let name = jsonElement["name"] as? String
                    {
                        print("------------")
                        print(userId)
                        print(name)
                    }
                }
            }
        }
        task.resume()
    }
    
    // GET USER
    func getOneUser(userId: String) -> UserModel {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: (urlPath + "/users/get_user/" + userId))!
        
        let semaphore = DispatchSemaphore(value: 0)
        
        let user = UserModel()
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else {
                semaphore.signal()
                return
            }
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary {
                    
                    print("SUCCESS: Get one user.")
                    
                    user.userProfile.userId = jsonResult["userId"] as? String
                    user.userProfile.name = jsonResult["name"] as? String
                    user.userProfile.surname = jsonResult["surname"] as? String
                    user.userProfile.email = jsonResult["email"] as? String
                    user.userProfile.birthDate = jsonResult["birthDate"] as? String
                    user.userProfile.photo = self.getUserPhoto(userId: user.userProfile.userId!) // jsonResult["photo"] as? String
                    
                    let p = jsonResult["profile"] as! [String: Any]
                    for (key, value) in p {
                        if(key == "lightMode"){
                            user.userPreferences.lightMode = value as? Bool
                        }
                        if(key == "color"){
                            user.userPreferences.color = value as? String
                        }
                        if(key == "style"){
                            user.userPreferences.style = value as? [String]
                        }
                    }
                }
                
                semaphore.signal()
                
            } catch let error {
                print("ERROR: Can't get user: " + error.localizedDescription)
                
                semaphore.signal()
            }
        }
        task.resume()
        
        semaphore.wait()
        
        return user
    }
    
    // ADD USER
    func addUser(user: UserModel) -> UserModel {
        
        let defaultProfile = ["lightMode": false, "color": "White", "style":[]] as [String : Any]
        
        let parameters = [
            "userId":       user.userProfile.userId!,
            "name":         user.userProfile.name!,
            "surname":      user.userProfile.surname!,
            "email":        user.userProfile.email!,
            "birthDate":    user.userProfile.birthDate!,
            "profile":      defaultProfile
        ] as [String : Any]
        
        let url = URL(string: urlPath + "/users/add_user")!
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let semaphore = DispatchSemaphore(value: 0)
        
        let userToAdd = UserModel()
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
         
            guard error == nil else {
                semaphore.signal()
                return
            }
            
            do {
                if let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary) {
                
                    userToAdd.userProfile.userId    = jsonResult["userId"] as? String
                    userToAdd.userProfile.name      = jsonResult["name"] as? String
                    userToAdd.userProfile.surname   = jsonResult["surname"] as? String
                    userToAdd.userProfile.email     = jsonResult["email"] as? String
                    userToAdd.userProfile.birthDate = jsonResult["birthDate"] as? String
                    
                    userToAdd.userPreferences.lightMode = true
                    userToAdd.userPreferences.color = ""
                    userToAdd.userPreferences.style = []

                    print("SUCCESS: User added.")
                    
                    semaphore.signal()
                }
             } catch let error {
                print("ERROR: User can't be added: " + error.localizedDescription)
                semaphore.signal()
            }
        })
        task.resume()
        
        semaphore.wait()
        
        return userToAdd
    }
    
    // DELETE USER
    func deleteUser(userId: String){
        
        let parameters = ["userId": userId]
        
        let url = URL(string: urlPath + "/users/delete_user/" + userId)!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    // UPDATE USER
    func updateUser(user: UserModel) -> UserModel {

        let parameters = [
            "userId": user.userProfile.userId!,
            "name": user.userProfile.name!,
            "surname": user.userProfile.surname!,
            "email": user.userProfile.email!,
            "birthDate": user.userProfile.birthDate ?? "",
            "profile": [
                "lightMode": user.userPreferences.lightMode!,
                "color": user.userPreferences.color!,
                "style": user.userPreferences.style!
            ]
        ] as [String : Any]
        
        let url = URL(string: urlPath + "/users/update_user")!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        let semaphore = DispatchSemaphore(value: 0)

        let userToUpdate = UserModel()
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                semaphore.signal()
                return
            }
            
            do {
                if let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary) {
                    
                    print("SUCCESS: Update one user.")
                    
                    userToUpdate.userProfile.userId     = jsonResult["userId"] as? String
                    userToUpdate.userProfile.name       = jsonResult["name"] as? String
                    userToUpdate.userProfile.surname    = jsonResult["surname"] as? String
                    userToUpdate.userProfile.email      = jsonResult["email"] as? String
                    userToUpdate.userProfile.birthDate  = jsonResult["birthDate"] as? String
                    //userToUpdate.userProfile.photo      = jsonResult["photo"] as? String
                    
                    let p = jsonResult["profile"] as! [String: Any]
                    for (key, value) in p {
                        if(key == "lightMode"){
                            userToUpdate.userPreferences.lightMode = value as? Bool
                        }
                        if(key == "color"){
                            userToUpdate.userPreferences.color = value as? String
                        }
                        if(key == "style"){
                            userToUpdate.userPreferences.style = value as? [String]
                        }
                    }
                }
                
                semaphore.signal()
                
            } catch let error {
                print("ERROR: Can't update user: " + error.localizedDescription)
                semaphore.signal()
            }
        })
        task.resume()
        
        semaphore.wait()
        
        return userToUpdate
    }
    
    // UPDATE USER PHOTO
    func updateUserPhoto(userId: String, photo: String) {
        
        let parameters = [
                "userId": userId,
                "photo": photo
            ] as [String : Any]
        
        let url = URL(string: urlPath + "/users/update_photo")!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        let semaphore = DispatchSemaphore(value: 0)
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                semaphore.signal()
                return
            }
            
            do {
                if (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary) != nil {
                    print("SUCCESS: Update user photo.")
                }
                
                semaphore.signal()
                
            } catch let error {
                print("ERROR: Can't update user photo: " + error.localizedDescription)
                semaphore.signal()
            }
        })
        task.resume()
        
        semaphore.wait()
    }
    
    // GET USER PHOTO
    func getUserPhoto(userId: String) -> String {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: (urlPath + "/users/get_photo/" + userId))!
        
        let semaphore = DispatchSemaphore(value: 0)
        
        var photoBas64 = String()
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else {
                print("ERROR: Can't get user photo.")
                semaphore.signal()
                return
            }
            
            guard let data = data else { return }
            photoBas64 = String(data: data, encoding: .utf8) ?? ""
            
            print("SUCCESS: get user photo.")
            
            semaphore.signal()
            
        }
        task.resume()
        
        semaphore.wait()
        
        return photoBas64
    }
    
    //DEMO MODE
    func demoMode() -> UserModel {
        
        let user = UserModel()
        
        user.userProfile.userId = "12345678"
        user.userProfile.name = "Wear"
        user.userProfile.surname = "Leny"
        user.userProfile.email = "leny@usherbrooke.ca"
        user.userProfile.birthDate = nil
        user.userProfile.photo = UIImageView(image: UIImage(named: "default_profile")).imageInBase64

        user.userPreferences.lightMode = true
        user.userPreferences.color = ""
        user.userPreferences.style = []
        
        appDelegate().setDemoMode()
        appDelegate().setLoggedIn(userId: user.userProfile.userId!)
        appDelegate().setLoggedInUser(user: user)
        
        let itemCharacteristics0 = ItemCharacteristics(
            size: "8.5",
            couleur: "Orange"
        )
        let item0 = ItemModel(
            title: "Nike Air Max orange",
            price: "99",
            type: .Chandail,
            style: itemStyle(rawValue: "Tendance")!,
            photos: ["": UIImageView(image: UIImage(named: "tempShoes")).imageInBase64],
            characteristics: itemCharacteristics0
        )
        let item1 = ItemModel(
            title: "Item 1",
            price: "99",
            type: .Chandail,
            style: itemStyle(rawValue: "Tendance")!,
            photos: ["": UIImageView(image: UIImage(named: "aj1")).imageInBase64],
            characteristics: itemCharacteristics0
        )
        let item2 = ItemModel(
            title: "Item 2",
            price: "99",
            type: .Chandail,
            style: itemStyle(rawValue: "Tendance")!,
            photos: ["": UIImageView(image: UIImage(named: "tempShoes")).imageInBase64],
            characteristics: itemCharacteristics0
        )
        
        appDelegate().loggedInUser!.setMyItems(items: [item0,item1,item2])
        
        return user
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    ITEMS
    ///////////////////////////////////////////////////////////////

    // GET ALL ITEMS
    func getAllItems() -> [ItemModel]{
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: (urlPath + "/items/get_all_items/"))!
        
        let semaphore = DispatchSemaphore(value: 0)
        
        var items = [ItemModel]()
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else {
                semaphore.signal()
                return
            }
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as? [Any] {
                    
                    if(!jsonResult.isEmpty){
                        for i in 0...jsonResult.count - 1 {
                            let item = ItemModel()
                            let dic = jsonResult[i] as? NSDictionary
                            
                            item.itemProfile.itemId = dic!["_id"] as? String
                            item.itemProfile.title = dic!["title"] as? String
                            item.itemProfile.price = dic!["price"] as? String
                            item.itemProfile.type = itemType(title: ((dic!["type"] as? String)!))
                            item.itemProfile.style = itemStyle(rawValue: ((dic!["style"] as? String)!))
                            
                            let c = dic!["characteristic"] as! [String: Any]
                            for (key, value) in c {
                                if(key == "size"){
                                    item.itemCharacteristics.size = value as? String
                                }
                                if(key == "color"){
                                    item.itemCharacteristics.couleur = value as? String
                                }
                            }
                            
                            if let p = dic!["photos"] as? [Any] {
                                var photoDic: [String:String] = [:]
                                
                                for filename in p {
                                    photoDic[filename as! String] = self.getItemPhoto(filename: filename as! String)
                                }
                                
                                item.itemProfile.photos = photoDic
                            }
                            
                            items.append(item)
                        }
                        
                        print("SUCCESS: Get all item.")
                        
                    }
                }
                
                semaphore.signal()
                
            } catch let error {
                print("ERROR: Can't get all item: " + error.localizedDescription)
                semaphore.signal()
            }
        }
        task.resume()
        
        semaphore.wait()
        
        return items
    }
    
    // GET FEED ITEMS
    func getFeedItems(userId: String) -> [ItemModel]{
       
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: (urlPath + "/aifeeds/load_feed/" + userId))!
        
        let semaphore = DispatchSemaphore(value: 0)
        
        var items = [ItemModel]()
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else {
                semaphore.signal()
                return
            }
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as? [Any] {
                    
                    if(!jsonResult.isEmpty){
                        for i in 0...jsonResult.count - 1 {
                            let item = ItemModel()
                            let dic = jsonResult[i] as? NSDictionary
                            
                            item.itemProfile.itemId = dic!["_id"] as? String
                            item.itemProfile.title = dic!["title"] as? String
                            item.itemProfile.price = dic!["price"] as? String
                            item.itemProfile.type = itemType(title: ((dic!["type"] as? String)!))
                            item.itemProfile.style = itemStyle(rawValue: ((dic!["style"] as? String)!))
                            
                            let c = dic!["characteristic"] as! [String: Any]
                            for (key, value) in c {
                                if(key == "size"){
                                    item.itemCharacteristics.size = value as? String
                                }
                                if(key == "color"){
                                    item.itemCharacteristics.couleur = value as? String
                                }
                            }
                            
                            if let p = dic!["photos"] as? [Any] {
                                //var photos = [String]()
                                var photoDic: [String:String] = [:]
                                
                                for filename in p {
                                    photoDic[filename as! String] = self.getItemPhoto(filename: filename as! String)
                                    //photos.append(self.getItemPhoto(filename: filename as! String))
                                }
                                
                                item.itemProfile.photos = photoDic
                            }
                            
                            items.append(item)
                        }
                    }
                }
                
                semaphore.signal()
                
                print("SUCCESS: Get feed item.")
                
            } catch let error {
                print("ERROR: Can't get feed item: " + error.localizedDescription)
                semaphore.signal()
            }
        }
        task.resume()
        
        semaphore.wait()
        
        return items
    }
    
    // GET ONE ITEM
    func getOneItem(itemId: String) -> ItemModel {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: (urlPath + "/items/get_item/" + itemId))!
        
        let semaphore = DispatchSemaphore(value: 0)
        
        let item = ItemModel()
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else {
                semaphore.signal()
                return
            }
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary {
                    
                    print("SUCCESS: Get one item.")
                    
                    item.itemProfile.itemId = jsonResult["_id"] as? String
                    item.itemProfile.title = jsonResult["title"] as? String
                    item.itemProfile.price = jsonResult["price"] as? String
                    item.itemProfile.type = itemType(title: ((jsonResult["type"] as? String)!))
                    item.itemProfile.style = itemStyle(rawValue: ((jsonResult["style"] as? String)!))
                    
                    let c = jsonResult["characteristic"] as! [String: Any]
                    for (key, value) in c {
                        if(key == "size"){
                            item.itemCharacteristics.size = value as? String
                        }
                        if(key == "color"){
                            item.itemCharacteristics.couleur = value as? String
                        }
                    }
                    
                    if let p = jsonResult["photos"] as? [Any] {
                        //var photos = [String]()
                        var photoDic: [String:String] = [:]
                        
                        for filename in p {
                            photoDic[filename as! String] = self.getItemPhoto(filename: filename as! String)
                            //photos.append(self.getItemPhoto(filename: filename as! String))
                        }
                        
                        item.itemProfile.photos = photoDic
                    }
                }
                
                semaphore.signal()
                
                
            } catch let error {
                print("ERROR: Can't get item: " + error.localizedDescription)
                semaphore.signal()
            }
        }
        task.resume()
        
        semaphore.wait()
        
        return item
    }
    
    // GET MY ITEM
    func getMyItems(userId: String) -> [ItemModel] {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: (urlPath + "/items/get_my_items/" + userId))!
        
        let semaphore = DispatchSemaphore(value: 0)
        
        var items = [ItemModel]()
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else {
                semaphore.signal()
                return
            }
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as? [Any] {
                    
                    print("SUCCESS: Get my item.")
                    
                    if(!jsonResult.isEmpty){
                        for i in 0...jsonResult.count - 1 {
                            let item = ItemModel()
                            let dic = jsonResult[i] as? NSDictionary
                            
                            item.itemProfile.itemId = dic!["_id"] as? String
                            item.itemProfile.title = dic!["title"] as? String
                            item.itemProfile.price = dic!["price"] as? String
                            item.itemProfile.type = itemType(title: ((dic!["type"] as? String)!))
                            item.itemProfile.style = itemStyle(rawValue: ((dic!["style"] as? String)!))
                            
                            let c = dic!["characteristic"] as! [String: Any]
                            for (key, value) in c {
                                if(key == "size"){
                                    item.itemCharacteristics.size = value as? String
                                }
                                if(key == "color"){
                                    item.itemCharacteristics.couleur = value as? String
                                }
                            }
                            
                            if let p = dic!["photos"] as? [Any] {
                                //var photos = [String]()
                                var photoDic: [String:String] = [:]
                                
                                for filename in p {
                                    photoDic[filename as! String] = self.getItemPhoto(filename: filename as! String)
                                    //photos.append(self.getItemPhoto(filename: filename as! String))
                                }
                                
                                item.itemProfile.photos = photoDic
                            }
                            
                            items.append(item)
                        }
                    }
                }
                
                semaphore.signal()
                
                
            } catch let error {
                print("ERROR: Can't get my item: " + error.localizedDescription)
                semaphore.signal()
            }
        }
        task.resume()
        
        semaphore.wait()
        
        return items
    }
    
    // ADD ITEM
    func addItem(item: ItemModel) -> ItemModel {
        
        let parameters = [
            "userId": appDelegate().loggedInUser?.userProfile.userId! as Any,
            "title": item.itemProfile.title!,
            "price": item.itemProfile.price!,
            "type": item.itemProfile.type?.rawValue.0 as Any,
            "style": item.itemProfile.style?.rawValue as Any,
            
            "characteristic": [
                "size": item.itemCharacteristics.size!,
                "color": item.itemCharacteristics.couleur!
            ]
        ] as [String : Any]
        
        let url = URL(string: urlPath + "/items/add_item")!
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        
        let semaphore = DispatchSemaphore(value: 0)
        
        let itemToAdd = ItemModel()
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                print("ERROR: User can't be added")
                semaphore.signal()
                return
            }
            
            do {
                if let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary) {
                    
                    itemToAdd.itemProfile.itemId = jsonResult["_id"] as? String
                    itemToAdd.itemProfile.title = jsonResult["title"] as? String
                    itemToAdd.itemProfile.price = jsonResult["price"] as? String
                    itemToAdd.itemProfile.type = itemType(title: ((jsonResult["type"] as? String)!))
                    itemToAdd.itemProfile.style = itemStyle(rawValue: ((jsonResult["style"] as? String)!))
                    
                    let c = jsonResult["characteristic"] as! [String: Any]
                    for (key, value) in c {
                        if(key == "size"){
                            itemToAdd.itemCharacteristics.size = value as? String
                        }
                    }
                    
                    print("SUCCESS: Item added.")
                    semaphore.signal()
                }
            } catch let error {
                print("ERROR: User can't be added: " + error.localizedDescription)
                semaphore.signal()
            }
        })
        task.resume()
        
        semaphore.wait()
        
        return itemToAdd
    }
    
    // DELETE ITEM
    func deleteItem(itemId: String){
        
        let parameters = ["itemId": itemId]
        
        let url = URL(string: urlPath + "/items/delete_item/" + itemId)!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    // UPDATE ITEM
    func updateItem(item: ItemModel) -> ItemModel {
        
        let parameters = [
            "_id": item.itemProfile.itemId!,
            "title": item.itemProfile.title!,
            "price": item.itemProfile.price!,
            "type": item.itemProfile.type?.rawValue.0 as Any,
            "style": item.itemProfile.style?.rawValue as Any,
            "characteristic": [
                "size": item.itemCharacteristics.size!,
                "color": item.itemCharacteristics.couleur!
            ]
        ] as [String : Any]
        
        let url = URL(string: urlPath + "/items/update_item")!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        let semaphore = DispatchSemaphore(value: 0)
        
        let itemToUpdate = ItemModel()
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                semaphore.signal()
                return
            }
            
            do {
                if let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary) {
                    
                    itemToUpdate.itemProfile.itemId = jsonResult["_id"] as? String
                    itemToUpdate.itemProfile.title = jsonResult["title"] as? String
                    itemToUpdate.itemProfile.price = jsonResult["price"] as? String
                    itemToUpdate.itemProfile.type = itemType(title: ((jsonResult["type"] as? String)!))
                    itemToUpdate.itemProfile.style = itemStyle(rawValue: ((jsonResult["style"] as? String)!))
                    
                    let c = jsonResult["characteristic"] as! [String: Any]
                    for (key, value) in c {
                        if(key == "size"){
                            itemToUpdate.itemCharacteristics.size = value as? String
                        }
                        if(key == "color"){
                            itemToUpdate.itemCharacteristics.couleur = value as? String
                        }
                    }
                    
                    if let p = jsonResult["photos"] as? [Any] {
                        //var photos = [String]()
                        var photoDic: [String:String] = [:]
                        
                        for filename in p {
                            photoDic[filename as! String] = self.getItemPhoto(filename: filename as! String)
                            //photos.append(self.getItemPhoto(filename: filename as! String))
                        }
                        
                        itemToUpdate.itemProfile.photos = photoDic
                        
                        print("SUCCESS: Update one item.")
                    }
                }
                
                semaphore.signal()
                
            } catch let error {
                print("ERROR: Can't update item: " + error.localizedDescription)
                semaphore.signal()
            }
        })
        task.resume()
        
        semaphore.wait()
        
        return itemToUpdate
    }

    // ADD ITEM PHOTO
    func addItemPhoto(itemId: String, photo: String) -> String {
        
        let parameters = [
            "_id": itemId,
            "photo": photo
        ]
        
        let url = URL(string: urlPath + "/items/add_photo")!
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let semaphore = DispatchSemaphore(value: 0)
        
        var filename = String()
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                semaphore.signal()
                return
            }
            
            do {
                if let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary) {
                    
                    filename = jsonResult["fileName"] as! String
                    
                    print("SUCCESS: photo added to item.")
                }
                
                semaphore.signal()
                
            } catch let error {
                print("ERROR: Photo can't be added: " + error.localizedDescription)
                semaphore.signal()
            }
        })
        task.resume()
        semaphore.wait()
        
        return filename
    }
    
    // DELETE ITEM PHOTO
    func deleteItemPhoto(filename: String){
        
        let parameters = ["filename": filename]
        
        let url = URL(string: urlPath + "/items/delete_photo/" + filename)!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    // GET ITEM PHOTO
    func getItemPhoto(filename: String) -> String {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: (urlPath + "/items/get_photo/" + filename))!
        
        let semaphore = DispatchSemaphore(value: 0)
        
        var photoBas64 = String()
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else {
                print("ERROR: Can't get item photo.")
                semaphore.signal()
                return
            }

            guard let data = data else { return }
            photoBas64 = String(data: data, encoding: .utf8)!
            
            print("SUCCESS: get item photo.")
            
            semaphore.signal()

        }
        task.resume()
        
        semaphore.wait()
        
        return photoBas64
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    UPDATE EVERY UI FOR LIGHT OR DARK MODE
    ///////////////////////////////////////////////////////////////
    func updateEveryUI(){
        
        UIView.animate(withDuration: 0.25, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
        
            appDelegate().window?.backgroundColor = UIColor.isLightModeBackground() // window background
            appDelegate().statusBarBackgroundView?.backgroundColor = UIColor.isLightModeItem() // topbar with clock background
            
            NotificationCenter.default.post(name: NSNotification.Name("updateUIMainViewController"), object: nil)
        
            NotificationCenter.default.post(name: NSNotification.Name("updateUIMenuBarView"), object: nil)
        
            NotificationCenter.default.post(name: NSNotification.Name("updateUILoginView"), object: nil)
        
            NotificationCenter.default.post(name: NSNotification.Name("updateUIProfileView"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name("updateUIFeedView"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name("updateUIPostView"), object: nil)
            
        })
        
            
        print("---> Every UI updated")
    }
    
    func getTopSafeAreaHeight() -> CGFloat{
        var topSafeAreaHeight: CGFloat = 0
        if UIApplication.shared.keyWindow != nil {
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.windows[0]

                let safeFrame = window.safeAreaLayoutGuide.layoutFrame
                topSafeAreaHeight = safeFrame.minY
            }
        }
       
        return topSafeAreaHeight
    }
    func getBottomSafeAreaHeight() -> CGFloat{
        var bottomSafeAreaHeight: CGFloat = 0
        // Make collectionView use safe area guide -----
        if UIApplication.shared.keyWindow != nil {
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.windows[0]

                let safeFrame = window.safeAreaLayoutGuide.layoutFrame
                bottomSafeAreaHeight = window.frame.maxY - safeFrame.maxY
            }
        }
        
        return bottomSafeAreaHeight
    }
    
}

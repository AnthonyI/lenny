//
//  AuthorizationServer.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-01-29.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import Foundation
import SafariServices
import AuthenticationServices

// Auth0 stuff
let domain = "https://tradewearco.auth0.com"
let clientId = "NTh2Offu3U5YJci0dL0LQCDLdzlKm3UE"

struct auth0Tokens {
    var accessToken: String
    var idToken: String?
    var refreshToken: String?
}

func base64UrlEncode(_ data: Data) -> String {
    var b64 = data.base64EncodedString()
    b64 = b64.replacingOccurrences(of: "=", with: "")
    b64 = b64.replacingOccurrences(of: "+", with: "-")
    b64 = b64.replacingOccurrences(of: "/", with: "_")
    return b64
}

func generateRandomBytes() -> String? {
    var keyData = Data(count: 32)
    var keyDataTemp = keyData
    let result = keyData.withUnsafeMutableBytes {
        (mutableBytes: UnsafeMutablePointer<UInt8>) -> Int32 in
        SecRandomCopyBytes(kSecRandomDefault, keyDataTemp.count, mutableBytes)
    }
    if result == errSecSuccess {
        return base64UrlEncode(keyData)
    } else {
        // TODO: handle error
        return nil
    }
}

func sha256(string: String) -> Data {
    let messageData = string.data(using:String.Encoding.utf8)!
    var digestData = Data(count: Int(CC_SHA256_DIGEST_LENGTH))
    
    _ = digestData.withUnsafeMutableBytes {digestBytes in
        messageData.withUnsafeBytes {messageBytes in
            CC_SHA256(messageBytes, CC_LONG(messageData.count), digestBytes)
        }
    }
    return digestData
}

class AuthorizationServer {
    private var codeVerifier: String? = nil
    private var savedState: String? = nil
    
    var receivedCode: String? = nil
    var receivedState: String? = nil
    
    private var sfAuthSession: ASWebAuthenticationSession? = nil
    private var sfSafariViewController: SFSafariViewController? = nil
    
    func authorize(viewController: UIViewController, useSfAuthSession: Bool, handler: @escaping (Bool) -> Void) {

        savedState = generateRandomBytes()
        
        var urlComp = URLComponents(string: domain + "/authorize")!
        
        urlComp.queryItems = [
            URLQueryItem(name: "response_type", value: "code"),
            URLQueryItem(name: "client_id", value: clientId),
            URLQueryItem(name: "state", value: savedState),
            URLQueryItem(name: "scope", value: "openid profile"),
            URLQueryItem(name: "redirect_uri", value: "auth0tradeandwear://tw"),
        ]
        
        if useSfAuthSession {
            sfAuthSession = ASWebAuthenticationSession(url: urlComp.url!, callbackURLScheme: "auth0tradeandwear", completionHandler: { (url, error) in
                guard error == nil else {
                    return handler(false)
                }
                
                handler(url != nil && self.parseAuthorizeRedirectUrl(url: url!))
            })
            sfAuthSession?.start()
        } else {
            
            guard let url = urlComp.url else {
                return
            }
            
            sfSafariViewController = SFSafariViewController(url: url)
            
            viewController.present(sfSafariViewController!, animated: false)
            
            handler(true)
        }
    }
    
    func parseAuthorizeRedirectUrl(url: URL) -> Bool {
        guard let urlComp = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            sfSafariViewController?.dismiss(animated: true, completion: nil)
            return false
        }
        
        if urlComp.queryItems == nil {
            sfSafariViewController?.dismiss(animated: true, completion: nil)
            return false
        }
        
        for item in urlComp.queryItems! {
            if item.name == "code" {
                receivedCode = item.value
            } else if item.name == "state" {
                receivedState = item.value
            }
        }
        
        sfSafariViewController?.dismiss(animated: true, completion: nil)
        
        return receivedCode != nil && receivedState != nil
    }
    
    func getToken(handler: @escaping (auth0Tokens?) -> Void) {
        if receivedCode == nil || savedState != receivedState {
            handler(nil)
            return
        }
        
        let urlComp = URLComponents(string: domain + "/oauth/token")!
        
        let body = [
            "grant_type": "authorization_code",
            "client_id": clientId,
            "code": receivedCode,
            "redirect_uri": "auth0tradeandwear://tw",
        ]
        
        var request = URLRequest(url: urlComp.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: body)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if(error != nil || data == nil) {
                // TODO: handle error
                handler(nil)
                return
            }
            
            guard let json = try? JSONSerialization.jsonObject(with: data!) as? [String: Any],
                let accessToken = json!["access_token"] as? String
                else {
                    handler(nil)
                    return
            }
            
            handler(auth0Tokens(accessToken: accessToken,
                           idToken: json!["id_token"] as? String,
                           refreshToken: json!["refresh_token"] as? String))
        }
        
        task.resume()
    }
    
    func getProfile(accessToken: String, handler: @escaping (UserProfile?) -> Void) {
        let urlComp = URLComponents(string: domain + "/userinfo")!
        
        let urlSessionConfig = URLSessionConfiguration.default;
        urlSessionConfig.httpAdditionalHeaders = [
            AnyHashable("Authorization"): "Bearer " + accessToken
        ]
        let urlSession = URLSession(configuration: urlSessionConfig)
        let task = urlSession.dataTask(with: urlComp.url!) {
            (data, response, error) in
            
            if(error != nil || data == nil) {
                // TODO: handle error
                handler(nil)
                return
            }
            
            guard let json = try? JSONSerialization.jsonObject(with: data!) as? [String: String] else {
                handler(nil)
                // TODO: handle error
                return
            }
            let result = UserModel(
                userId:         json?["sub"] ?? "",
                name:           json?["family_name"] ?? "",
                surname:        json?["given_name"] ?? "",
                email:          json?["email"] ?? "",
                birthDate:      json?["user_birthday"] ?? "",
                photo:          json?["picture"] ?? ""
            )
            handler(result.userProfile)
        }
        
        task.resume()
    }
    
    func reset() {
        codeVerifier = nil
        savedState = nil
        receivedCode = nil
        receivedState = nil
        sfAuthSession = nil
    }
    
    private func generateCodeChallenge() -> String? {
        codeVerifier = generateRandomBytes()
        guard codeVerifier != nil else {
            return nil
        }
        return base64UrlEncode(sha256(string: codeVerifier!))
    }
    
}

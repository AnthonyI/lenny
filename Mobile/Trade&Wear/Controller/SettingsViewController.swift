//
//  SettingsViewController.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-02-11.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit
import AudioUnit


class SettingsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let closeViewWrapper = UIView()
    let closeButton = UIButton()
    
    let closeViewHeight:CGFloat = 50
    
    var colView: UICollectionView!
    
    let cellId = "cellId"
    let allCells: [Any] = {
        return [
            CustomThemeModeCell(title: "THÈME SOMBRE", image: "lightMode_icon"),
            CustomColorsCell(title: "COULEURS", image: "color_icon")
        ]
    }()

    
    ///////////////////////////////////////////////////////////////
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initCollectionView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func initCollectionView() {
        
        let padding:CGFloat = 20

        let layout = UICollectionViewFlowLayout()
        
        colView = UICollectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - closeViewHeight - appDelegate().service.getBottomSafeAreaHeight()), collectionViewLayout: layout)
        colView.delegate   = self
        colView.dataSource = self
        colView.backgroundColor = UIColor.isLightModeBackground()
        colView.register(CustomPreferencesCell.self, forCellWithReuseIdentifier: cellId)
        colView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        colView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        colView.isPagingEnabled = true
        colView.showsHorizontalScrollIndicator = false

        closeViewWrapper.frame = CGRect(x: 0, y: colView.frame.size.height, width: self.view.frame.width, height: closeViewHeight + appDelegate().service.getBottomSafeAreaHeight())
        closeViewWrapper.backgroundColor = UIColor.isLightModeBackground()
        
        closeButton.frame = CGRect(x: 0, y: closeViewHeight - closeViewHeight - 20, width: 60, height: 60)
        closeButton.setImage(UIImage(named:"arrow_right_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.transform = self.closeButton.transform.rotated(by: .pi / 2)
        closeButton.tintColor = .white
        closeButton.layer.backgroundColor = UIColor.black.cgColor
        closeButton.layer.masksToBounds = false
        closeButton.layer.cornerRadius = closeButton.frame.width / 2
        closeButton.center.x = closeViewWrapper.center.x
        closeButton.titleEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        closeButton.contentEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        
        closeViewWrapper.addSubview(closeButton)
        
        self.view.addSubview(colView)
        self.view.addSubview(closeViewWrapper)
    }
    
    @objc func closeButtonPressed() {
        
        if(appDelegate().isLoggedIn()){
            let user = appDelegate().loggedInUser
            
            var updateTheUser: Bool = false
            
            if(user?.userPreferences.lightMode != appDelegate().isLightModeCurrently){
                user?.userPreferences.lightMode = appDelegate().isLightModeCurrently
                
                updateTheUser = true
            }
            
            if(user?.userPreferences.color != appDelegate().currentChosenColor){
                user?.userPreferences.color = appDelegate().currentChosenColor
                
                updateTheUser = true
            }
            
            if(updateTheUser){
                _ = appDelegate().service.updateUser(user: user!)
            }
        }
        
        appDelegate().service.updateEveryUI()
        
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allCells.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CustomPreferencesCell
        
        let aCell = allCells[indexPath.item]
        
        if(aCell is CustomThemeModeCell){
            cell.aThemeModeCell = aCell as? CustomThemeModeCell
        }
        else if(aCell is CustomColorsCell){
            cell.aColorsCell = aCell as? CustomColorsCell
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let aCell = allCells[indexPath.item]
        
        if(aCell is CustomThemeModeCell){
            return CGSize(width: collectionView.frame.width, height: 50)
        }
        else if(aCell is CustomColorsCell){
            return CGSize(width: collectionView.frame.width, height: 100)
        }
        
        return CGSize(width: collectionView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }
}

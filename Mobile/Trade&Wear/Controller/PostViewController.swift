//
//  PostViewController.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-03-13.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox

class PostViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var isUpdatingItem: Bool = false
    
    var mainViewControllerInstance: MainViewController?
    
    var itemInProcess = ItemModel()
    var chosenItemType: itemType?
    
    var keyboardHeight: CGFloat = 0.0
    
    var currentIndex = 0
    
    let closeViewHeight:CGFloat = 75
    
    let bottomButtonBar = UIView()
    let closeButton = UIButton()
    let deleteButton = UIButton()
    let previousButton = UIButton()
    let nextButton = UIButton()
    
    private var postInfoViewWrapper: UIView!
    lazy var postInfoView: PostInfoView = {
        let c = PostInfoView()
        c.postViewControllerInstance = self
        return c
    }()
    
    private var cameraViewWrapper: UIView!
    lazy var cameraView: CameraView = {
        let c = CameraView()
        c.postViewControllerInstance = self
        return c
    }()
    
    var colView: UICollectionView!
    
    var allViews: [UIView] = []
    let cellId = "cellId"
    
    
    ///////////////////////////////////////////////////////////////

    convenience init() {
        self.init(currentItem: ItemModel())
        isUpdatingItem = false
    }
    
    init(currentItem: ItemModel?) {
        self.itemInProcess = currentItem!
        
        if(currentItem?.itemProfile.itemId != nil){
            isUpdatingItem = true
            self.chosenItemType = currentItem?.itemProfile.type
        }
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        initCollectionView()
        initPostInfoView()
        initCameraView()
        
        allViews = [postInfoViewWrapper, cameraViewWrapper]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    INIT COLLECTION VIEW
    ///////////////////////////////////////////////////////////////

    func initCollectionView() {

        if UIApplication.shared.keyWindow != nil {
            
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0
            
            // ----------------------------------------------

            colView = UICollectionView(frame: CGRect(x: 0, y: appDelegate().service.getTopSafeAreaHeight(), width: self.view.frame.width, height: self.view.frame.height - closeViewHeight - appDelegate().service.getBottomSafeAreaHeight()), collectionViewLayout: layout)
            colView.delegate   = self
            colView.dataSource = self
            colView.backgroundColor = .white
            colView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
            colView.isPagingEnabled = true
            colView.isScrollEnabled = false
            colView.showsHorizontalScrollIndicator = false
            
            bottomButtonBar.frame = CGRect(x: 0, y: colView.frame.size.height, width: self.view.frame.width, height: closeViewHeight + appDelegate().service.getBottomSafeAreaHeight())
           
            let padding:CGFloat = 20
            
            closeButton.frame = CGRect(x: 40, y: (self.closeViewHeight/2) - 30, width: 60, height: 60)
            //closeButton.center.x = colView.center.x
            closeButton.setImage(UIImage(named:"close_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
            closeButton.tintColor = UIColor.colorForText()
            closeButton.layer.masksToBounds = false
            closeButton.layer.cornerRadius = closeButton.frame.width / 2
            closeButton.titleEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
            closeButton.contentEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
            closeButton.addTarget(self, action: #selector(dismissViewController), for: .touchUpInside)
            
            deleteButton.frame = CGRect(x: self.view.frame.width - 100, y: (closeViewHeight/2) - 30, width: 60, height: 60)
            deleteButton.setImage(UIImage(named:"delete_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
            deleteButton.tintColor = UIColor.colorForText()
            deleteButton.layer.masksToBounds = false
            deleteButton.layer.cornerRadius = closeButton.frame.width / 2
            deleteButton.titleEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
            deleteButton.contentEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
            deleteButton.addTarget(self, action: #selector(deleteItemButtonPressed), for: .touchUpInside)
            
            previousButton.frame = CGRect(x: 0, y: (closeViewHeight/2) - 30, width: 60, height: 60)
            previousButton.center.x = colView.center.x
            previousButton.setImage(UIImage(named:"arrow_left_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
            previousButton.tintColor = .white
            previousButton.layer.masksToBounds = false
            previousButton.layer.cornerRadius = previousButton.frame.width / 2
            previousButton.layer.backgroundColor = UIColor.black.cgColor
            previousButton.titleEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
            previousButton.contentEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
            previousButton.addTarget(self, action: #selector(previousButtonPressed), for: .touchUpInside)
            previousButton.alpha = 0
            
            nextButton.frame = CGRect(x: 0, y: (closeViewHeight/2) - 30, width: 60, height: 60)
            nextButton.center.x = colView.center.x
            nextButton.setImage(UIImage(named:"arrow_right_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
            nextButton.tintColor = .white
            nextButton.layer.masksToBounds = false
            nextButton.layer.cornerRadius = nextButton.frame.width / 2
            nextButton.layer.backgroundColor = UIColor.black.cgColor
            nextButton.titleEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
            nextButton.contentEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
            nextButton.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
            
            if(isUpdatingItem){
                bottomButtonBar.addSubview(deleteButton)
            }
            bottomButtonBar.addSubview(closeButton)
            bottomButtonBar.addSubview(previousButton)
            bottomButtonBar.addSubview(nextButton)
            
            self.view.addSubview(colView)
            self.view.addSubview(bottomButtonBar)
            
            // Start app at menu index 0
            let selectedMenuIcon = NSIndexPath(item: 0, section: 0)
            scrollToMenuIndex(menuIndex: selectedMenuIcon.item)
            colView.selectItem(at: selectedMenuIcon as IndexPath, animated: false, scrollPosition: [])
            
            updateUI()
        }
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    INIT POST INFO VIEW
    ///////////////////////////////////////////////////////////////

    private func initPostInfoView(){
        
        postInfoViewWrapper = UIView(frame: CGRect(x: 0, y: 0, width: colView.bounds.size.width, height: colView.bounds.size.height))
        
        postInfoView.frame.size.height = postInfoViewWrapper.frame.size.height
        postInfoView.frame.size.width = postInfoViewWrapper.frame.size.width
        
        postInfoViewWrapper.addSubview(postInfoView)
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    INIT CAMERA VIEW
    ///////////////////////////////////////////////////////////////
    
    private func initCameraView(){
        
        cameraViewWrapper = UIView(frame: CGRect(x: 0, y: 0, width: colView.bounds.size.width, height: colView.bounds.size.height))
        
        cameraView.frame.size.height = cameraViewWrapper.frame.size.height
        cameraView.frame.size.width = cameraViewWrapper.frame.size.width
        
        cameraViewWrapper.addSubview(cameraView)
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    UPDATE UI FOR LIGHT OR DARK MODE
    ///////////////////////////////////////////////////////////////
    @objc func updateUI(){
        self.view.backgroundColor = UIColor.isLightModeBackground()
        bottomButtonBar.backgroundColor = UIColor.isLightModeBackground()
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    PAGE FLOW CONTROLLER
    ///////////////////////////////////////////////////////////////
    
    @objc func previousButtonPressed() {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        if(currentIndex - 1 >= 0){
            currentIndex -= 1
            scrollToMenuIndex(menuIndex: currentIndex)
        }
    }
    @objc func nextButtonPressed() {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        if(currentIndex == 0){
            if(postInfoView.checkForErrors()){
                return
            }
            
            self.view.endEditing(true)
            postInfoView.saveItemInfo()
        }
        if(currentIndex == 1){
            cameraView.saveItemInfo()
            
            if(isUpdatingItem){
                updateItemInDB()
            }else{
                addItemToDB()
            }
        }
        
        if(currentIndex + 1 < allViews.count){
            currentIndex += 1
            scrollToMenuIndex(menuIndex: currentIndex)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.closeButton.alpha = 0
                
                self.previousButton.frame = CGRect(x: 40, y: (self.closeViewHeight/2) - 30, width: 60, height: 60)
                self.nextButton.frame = CGRect(x: self.view.frame.width - 100, y: (self.closeViewHeight/2) - 30, width: 60, height: 60)
                
            })
        }
        
    }
    
    @objc func addItemToDB() {
        
        let item = appDelegate().service.addItem(item: itemInProcess) // Add item to DB
        itemInProcess.itemProfile.itemId =  item.itemProfile.itemId
        
        var photoDic: [String:String] = [:]
        for photo in itemInProcess.itemProfile.photos! {
            photoDic[appDelegate().service.addItemPhoto(itemId: itemInProcess.itemProfile.itemId!, photo: photo.value)] = photo.value
        }
        itemInProcess.itemProfile.photos = photoDic
        
        appDelegate().loggedInUser?.addToMyItems(item: itemInProcess) // Add item to loggedin user
        
        NotificationCenter.default.post(name: NSNotification.Name("updateMyItemsProfileView"), object: nil)
        
        closeViewController()
    }
    
    @objc func updateItemInDB() {
        
        itemInProcess = appDelegate().service.updateItem(item: itemInProcess) // Update item in DB
        
        appDelegate().loggedInUser?.updateOneItem(item: itemInProcess)
        
        NotificationCenter.default.post(name: NSNotification.Name("updateMyItemsProfileView"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name("updateItemInfo"), object: nil, userInfo: ["Item": itemInProcess])
        
        dismissViewController()
    }
    
    @objc func deleteItemButtonPressed(){
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "Supprimer ?", message: "Êtes vous sûr de vouloir supprimer cet article?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Supprimer", style: .destructive, handler: { action in
            
            appDelegate().service.deleteItem(itemId: self.itemInProcess.itemProfile.itemId!)
            
            appDelegate().loggedInUser?.deleteOneItem(item: self.itemInProcess)
            
            NotificationCenter.default.post(name: NSNotification.Name("updateMyItemsProfileView"), object: nil)
            
            self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Annuler", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    @objc func closeViewController() {
        
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
        
        let navController = UINavigationController(rootViewController: ItemViewController(currentItem: itemInProcess, isMine: true))
        mainViewControllerInstance?.present(navController, animated: true, completion: nil)
    }
    
    @objc func dismissViewController() {
        
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    ALL ABOUT COLLECTION VIEW
    ///////////////////////////////////////////////////////////////
    
    var previousButtonVisible = false
    func scrollToMenuIndex(menuIndex: Int) {
        if(currentIndex != 0){
            if(!self.previousButtonVisible){
                self.previousButton.alpha = 0
                UIView.animate(withDuration: 0.2){
                    self.previousButton.alpha = 1
                    self.previousButtonVisible = true
                }
            }
        }else{
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.closeButton.alpha = 1
                
                self.previousButton.frame = CGRect(x: 0, y: (self.closeViewHeight/2) - 30, width: 60, height: 60)
                self.previousButton.center.x = self.colView.center.x
                
                self.nextButton.frame = CGRect(x: 0, y: (self.closeViewHeight/2) - 30, width: 60, height: 60)
                self.nextButton.center.x = self.colView.center.x
                
            }) { finished in
                self.previousButton.alpha = 0
                self.previousButtonVisible = false
            }
        }
        
        let indexPath = IndexPath(item: menuIndex, section: 0)
        colView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        cell.addSubview(allViews[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: colView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    @objc func keyboardWillShow(notification: NSNotification){
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        keyboardHeight = keyboardFrame.height
        
        //colView.frame = CGRect(x: 0, y: appDelegate.service.getTopSafeAreaHeight(), width: self.view.frame.width, height: self.view.frame.height - closeViewHeight - appDelegate.service.getBottomSafeAreaHeight() - keyboardHeight)
        
        bottomButtonBar.frame = CGRect(x: 0, y: self.view.frame.height - keyboardHeight - closeViewHeight, width: self.view.frame.width, height: closeViewHeight)
    }
    @objc func keyboardWillHide(notification: NSNotification){
        
        //colView.frame = CGRect(x: 0, y: appDelegate.service.getTopSafeAreaHeight(), width: self.view.frame.width, height: self.view.frame.height - closeViewHeight - appDelegate.service.getBottomSafeAreaHeight())
        
        bottomButtonBar.frame = CGRect(x: 0, y: colView.frame.size.height, width: self.view.frame.width, height: closeViewHeight + appDelegate().service.getBottomSafeAreaHeight())
    }
    
    @objc func choosePhotos() {
        NotificationCenter.default.post(name: NSNotification.Name("choosePhotosPressed"), object: nil)
    }
    
}

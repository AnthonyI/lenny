//
//  ItemViewController.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-04-01.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit
import Foundation
import MessageUI
import WebKit
import AudioToolbox

class ItemViewController: UIViewController, MFMailComposeViewControllerDelegate, WKNavigationDelegate {
    
    var currentItem: ItemModel?
    var isMine: Bool = false
    
    let mainViewWrapper = UIView()
    
    let closeViewHeight:CGFloat = 75
    let bottomBarWrapper = UIView()
    let actionButton = UIButton()
    
    private var itemViewWrapper: UIView!
    lazy var itemView: ItemInfoView = {
        let view = ItemInfoView()
        return view
    }()
    
    var wkwebView = WKWebView()
    var htmlForEmail = String()
    
    ///////////////////////////////////////////////////////////////
    
    
    init(currentItem: ItemModel?, isMine: Bool) {
        self.currentItem = currentItem
        self.isMine = isMine
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = UIColor.colorForText()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateItemInfo), name: NSNotification.Name("updateItemInfo"), object: nil)
        
        wkwebView.navigationDelegate = self
        
        updateNavBar()
        initItemInfo()
        initView()
        initItemView()
    }
    override func viewDidAppear(_ animated: Bool) {
    }
    
    func initView() {
        
        mainViewWrapper.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - appDelegate().service.getBottomSafeAreaHeight() - self.navigationController!.navigationBar.frame.size.height*2)
        
        bottomBarWrapper.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - closeViewHeight*2 - appDelegate().service.getBottomSafeAreaHeight(), width: self.view.frame.width, height: closeViewHeight + appDelegate().service.getBottomSafeAreaHeight())
        bottomBarWrapper.backgroundColor = UIColor.isLightModeBackground()
        
        
        if(isMine){
            actionButton.setTitle("MARQUER COMME VENDU", for: .normal)
            actionButton.addTarget(self, action: #selector(markAsSoldButtonPressed), for: .touchUpInside)
        }else{
            actionButton.setTitle("CONTACTER LE VENDEUR", for: .normal)
            actionButton.addTarget(self, action: #selector(contactSellerButtonPressed), for: .touchUpInside)
        }
        
        actionButton.frame = CGRect(x: 0, y: (closeViewHeight/2) - 15, width: actionButton.intrinsicContentSize.width, height: 30)
        actionButton.setTitleColor(UIColor.colorForText(), for: .normal)
        actionButton.titleLabel?.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
        actionButton.layer.borderWidth = 2
        actionButton.backgroundColor = UIColor.isLightModeItem()
        actionButton.layer.borderColor = UIColor.isLightModeItem().cgColor
        actionButton.center.x = bottomBarWrapper.center.x
        
        bottomBarWrapper.addSubview(actionButton)
        self.view.addSubview(mainViewWrapper)
        self.view.addSubview(bottomBarWrapper)
    }
    
    func initItemView(){
        itemViewWrapper = UIView(frame: CGRect(x: 0, y: 0, width: mainViewWrapper.bounds.size.width, height: mainViewWrapper.bounds.size.height))
        
        itemView.frame.size.height = itemViewWrapper.frame.size.height
        itemView.frame.size.width = itemViewWrapper.frame.size.width
        
        itemViewWrapper.addSubview(itemView)
        mainViewWrapper.addSubview(itemViewWrapper)
    }
    
    func initItemInfo() {
        
        itemView.title = currentItem?.itemProfile.title ?? ""
        itemView.price = currentItem?.itemProfile.price ?? ""
        itemView.taille = currentItem?.itemCharacteristics.size ?? ""
        itemView.couleur = currentItem?.itemCharacteristics.couleur ?? ""
        itemView.style = currentItem?.itemProfile.style?.rawValue ?? ""
        
        itemView.images = []
        for (_,image) in currentItem?.itemProfile.photos ?? [:] {
            itemView.images.insert(image.base64InImage, at: itemView.images.count)
        }
        
        itemView.loadItemInfo()
    }
    
    @objc func updateItemInfo(_ notification: NSNotification) {
        
        if let dict = notification.userInfo as NSDictionary? {
            if let item = dict["Item"] as? ItemModel{
                currentItem = item
            }
        }
        
        updateNavBar()
        initItemInfo()
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    UPDATE TOP NAV BAR
    ///////////////////////////////////////////////////////////////
    func updateNavBar(){
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.colorForText()]
        
        let titleLabel = UILabel()
        titleLabel.textColor = UIColor.colorForText()
        titleLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
        titleLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width - 75, height: view.frame.height)
        titleLabel.text = (currentItem?.itemProfile.title)!
        titleLabel.textAlignment = .left
        self.navigationItem.titleView = titleLabel
        
        let editItemButton = UIButton(type: .custom)
        editItemButton.frame = CGRect(x: 0.0, y: 0.0, width: 40, height: 40)
        editItemButton.setImage(UIImage(named:"edit_image")?.withRenderingMode(.alwaysTemplate), for: .normal)
        editItemButton.tintColor = UIColor.colorForText()
        editItemButton.contentEdgeInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        editItemButton.addTarget(self, action: #selector(self.editItemButtonPressed), for: UIControl.Event.touchUpInside)
        
        let menuBarItemRight = UIBarButtonItem(customView: editItemButton)
        let currWidthRight = menuBarItemRight.customView?.widthAnchor.constraint(equalToConstant: 40)
        currWidthRight?.isActive = true
        let currHeightRight = menuBarItemRight.customView?.heightAnchor.constraint(equalToConstant: 40)
        currHeightRight?.isActive = true
        
        let closeButton = UIButton(type: .custom)
        closeButton.frame = CGRect(x: 0.0, y: 0.0, width: 40, height: 40)
        closeButton.setImage(UIImage(named:"close_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.tintColor = UIColor.colorForText()
        closeButton.contentEdgeInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        closeButton.addTarget(self, action: #selector(self.closeButtonPressed), for: UIControl.Event.touchUpInside)
        
        let menuBarItemLeft = UIBarButtonItem(customView: closeButton)
        let currWidthLeft = menuBarItemLeft.customView?.widthAnchor.constraint(equalToConstant: 40)
        currWidthLeft?.isActive = true
        let currHeightLeft = menuBarItemLeft.customView?.heightAnchor.constraint(equalToConstant: 40)
        currHeightLeft?.isActive = true
        
        if(isMine){
            self.navigationItem.rightBarButtonItem = menuBarItemRight
        }
        
        self.navigationItem.leftBarButtonItem = menuBarItemLeft
    }
    
    @objc func contactSellerButtonPressed() {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        var contents = String()
        do{
            guard let filePath = Bundle.main.path(forResource: "htmlEmail", ofType: "html") else { return }
            contents = try String(contentsOfFile: filePath, encoding: .utf8)
            let baseUrl = URL(fileURLWithPath: filePath)
            wkwebView.loadHTMLString(contents as String, baseURL: baseUrl)
            
        } catch {
            print("HTML File error")
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        let userName = (appDelegate().loggedInUser?.userProfile.surname)! + " " + (appDelegate().loggedInUser?.userProfile.name)!
        
        let base64img = currentItem?.itemProfile.photos?.first?.value ?? ""
        let image = base64img.base64InImage
        let imageData = image.jpegData(compressionQuality: 1)
        
        let itemTitle = currentItem?.itemProfile.title ?? "Titre"
        let itemPrice = currentItem?.itemProfile.price ?? "Prix"
        let itemTaille = currentItem?.itemCharacteristics.size ?? "Taille"
        let itemStyle = currentItem?.itemProfile.style?.rawValue ?? "Style"
        let itemCouleur = currentItem?.itemCharacteristics.couleur ?? "Couleur"
        
        wkwebView.evaluateJavaScript(
            "populateEmail(\"\(userName)\", \"\(imageData!.base64EncodedString())\", \"\(itemTitle)\", \"\(itemPrice)\", \"\(itemTaille)\", \"\(itemStyle)\", \"\(itemCouleur)\")"
            , completionHandler: nil)
        
        wkwebView.evaluateJavaScript("document.documentElement.outerHTML.toString()", completionHandler: { (html: Any?, error: Error?) in
            
            self.htmlForEmail = html as! String
            
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.navigationBar.tintColor = UIColor.colorForText()
                mail.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.colorForText()]
                mail.mailComposeDelegate = self
                mail.setSubject("\(itemTitle)")
                mail.setToRecipients(["tradewearteam@gmail.com"])
                mail.setMessageBody(self.htmlForEmail, isHTML: true)
                
                self.present(mail, animated: true)
            } else {
                // Won't work in simulator
                print("ERROR: Can't send mail.")
            }
        })
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @objc func markAsSoldButtonPressed() {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @objc func closeButtonPressed() {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @objc func editItemButtonPressed() {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        self.navigationController?.pushViewController(PostViewController(currentItem: currentItem), animated: true)
    }
    
}

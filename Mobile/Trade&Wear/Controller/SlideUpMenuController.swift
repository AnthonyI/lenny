//
//  SlideUpMenu.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-02-09.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit
import AudioUnit

class SlideUpMenuController: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var mainViewControllerInstance: MainViewController?

    let blackView = UIView()
    
    let collectionViewThemeWrapper = UIView()
    let collectionViewTheme: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.isLightModeItem()
        return cv
    }()
    
    let collectionViewWrapper = UIView()
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.isLightModeItem()
        return cv
    }()
    
    let logoutButtonViewWrapper = UIView()
    let logoutButton = UIButton()
    
    let cellId = "cellId"
    let cellHeight:CGFloat = 50
    let logoutViewHeight:CGFloat = 75
    
    var allCells: [CustomMenuCell] = []
    var shownCells: [CustomMenuCell] = []
    
    
    ///////////////////////////////////////////////////////////////
    
    func showMenu() {
        
        if let window = UIApplication.shared.keyWindow {

            blackView.backgroundColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.2)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
            
            blackView.frame = window.frame
            blackView.alpha = 0
            window.addSubview(blackView)
            
            let width: CGFloat = window.frame.width - 40
            var collectionHeight: CGFloat
            var height: CGFloat
            var y: CGFloat
            
            if(appDelegate().isLoggedIn()){
                
                self.shownCells = allCells
                
                collectionViewTheme.reloadData()
                collectionView.reloadData()
                
                collectionHeight = CGFloat(shownCells.count) * cellHeight
                height = collectionHeight + appDelegate().service.getBottomSafeAreaHeight() + logoutViewHeight + cellHeight
                y = window.frame.height - height
                
                collectionViewWrapper.frame = CGRect(x: 20, y: window.frame.height, width: width, height: height)
                collectionView.frame = CGRect(x: 0, y: 0, width: collectionViewWrapper.frame.size.width, height: collectionHeight)
                
                collectionViewWrapper.addSubview(logoutButtonViewWrapper)
                
            }else{
                
                var trueCells = [CustomMenuCell]()
                
                for c in allCells {
                    if(!c.cellInfos.rawValue.2){
                        trueCells.append(c)
                    }
                }
                
                self.shownCells = trueCells
                
                collectionViewTheme.reloadData()
                collectionView.reloadData()
                
                collectionHeight = CGFloat(shownCells.count) * cellHeight
                height = collectionHeight + appDelegate().service.getBottomSafeAreaHeight() + cellHeight
                y = window.frame.height - height

                logoutButtonViewWrapper.removeFromSuperview()
            }
            
            collectionViewWrapper.frame = CGRect(x: 20, y: window.frame.height, width: width, height: height)
            collectionViewWrapper.backgroundColor = UIColor.isLightModeBackground()
            
            collectionViewTheme.frame = CGRect(x: 0, y: 0, width: collectionViewWrapper.frame.size.width, height: cellHeight)
            collectionViewTheme.layer.backgroundColor = UIColor.isLightModeBackground().cgColor
            
            collectionView.frame = CGRect(x: 0, y: collectionViewTheme.frame.height, width: collectionViewWrapper.frame.width, height: collectionHeight)
            collectionView.layer.backgroundColor = UIColor.clear.cgColor // isLightModeBackground()

            logoutButtonViewWrapper.frame = CGRect(x: 0, y: collectionView.frame.size.height + collectionViewTheme.frame.height, width: width, height: logoutViewHeight)
            
            logoutButton.frame = CGRect(x: 0, y: (logoutViewHeight/2) - 15, width: 100, height: 30)
            logoutButton.setTitle("LOGOUT", for: .normal)
            logoutButton.setTitleColor(UIColor.colorForText(), for: .normal)
            logoutButton.titleLabel?.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
            logoutButton.layer.borderWidth = 2
            logoutButton.backgroundColor = UIColor.isLightModeItem()
            logoutButton.layer.borderColor = UIColor.isLightModeItem().cgColor
            logoutButton.center.x = logoutButtonViewWrapper.center.x
            logoutButton.addTarget(self, action: #selector(logoutButtonPressed), for: .touchUpInside)
            
            logoutButtonViewWrapper.addSubview(logoutButton)
            
            collectionViewWrapper.addSubview(collectionViewTheme)
            collectionViewWrapper.addSubview(collectionView)
            
            window.addSubview(collectionViewWrapper)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.collectionViewWrapper.frame = CGRect(x: 20, y: y, width: self.collectionViewWrapper.frame.width, height: self.collectionViewWrapper.frame.height)
            })
        }
    }
    
    @objc func handleDismiss() {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        if(appDelegate().isLoggedIn()){
            let user = appDelegate().loggedInUser
            
            var updateTheUser: Bool = false
            
            if(user?.userPreferences.lightMode != appDelegate().isLightModeCurrently){
                user?.userPreferences.lightMode = appDelegate().isLightModeCurrently
                
                updateTheUser = true
            }
            
            if(updateTheUser){
                _ = appDelegate().service.updateUser(user: user!)
            }
        }
        
        appDelegate().service.updateEveryUI()
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blackView.alpha = 0
            
            if let window = UIApplication.shared.keyWindow {
                self.collectionViewWrapper.frame = CGRect(x: 20, y: window.frame.height, width: self.collectionViewWrapper.frame.width, height: self.collectionViewWrapper.frame.height)
                
            }
        })
    }
    
    @IBAction func logoutButtonPressed(sender: UIButton!) {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        self.handleDismiss()
        
        let alert = UIAlertController(title: "Logout?", message: "Êtes vous sûr de vouloir vous déconnecter?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Logout", style: .default, handler: { action in
            
            appDelegate().logout()
            
            NotificationCenter.default.post(name: NSNotification.Name("updateLayoutProfileView"), object: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Annuler", style: .cancel, handler: nil))
        
        self.mainViewControllerInstance?.present(alert, animated: true)
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    ALL ABOUT COLLECTION VIEW
    ///////////////////////////////////////////////////////////////
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == self.collectionView){
            return shownCells.count
        }else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == self.collectionView){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CustomMenuColCell
            
            let aCell = shownCells[indexPath.item]
            cell.aCell = aCell
            cell.setColor()
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CustomPreferencesCell
            
            let aCell = CustomThemeModeCell(title: "THÈME SOMBRE", image: "lightMode_icon")
            cell.aThemeModeCell = aCell
            cell.setColor()
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView == self.collectionView){
            let peek = SystemSoundID(1519)
            AudioServicesPlaySystemSound(peek)
            
            handleDismiss()
            
            let controllerToShow = self.allCells[indexPath.item].cellInfos.rawValue.1 // This is the controller of the cell
            mainViewControllerInstance?.navigationController?.present(controllerToShow, animated: true)
        }
    }
    
    init(allCells: [CustomMenuCell]){
        super.init()
        
        self.allCells = allCells
        self.shownCells = allCells
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(CustomMenuColCell.self, forCellWithReuseIdentifier: cellId)
        
        self.collectionViewTheme.dataSource = self
        self.collectionViewTheme.delegate = self
        self.collectionViewTheme.register(CustomPreferencesCell.self, forCellWithReuseIdentifier: cellId)
    }
    
}

//
//  InBoardingViewController.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-02-11.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit
import AudioUnit

class InBoardingViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var userPhotoNeedsUpdate = false
    
    lazy var userInProcess: UserModel = {
        let u = UserModel()
        return u
    }()
    
    var currentIndex = 0
    
    let closeViewHeight:CGFloat = 75
    
    var keyboardHeight: CGFloat = 0.0

    let bottomButtonBar = UIView()
    let closeButton = UIButton()
    let lightModeDarkModeButton = UIButton()
    let previousButton = UIButton()
    let nextButton = UIButton()
    
    let mainViewWrapper = UIView()
    let titleLabel = UILabel()
    
    private var editProfileViewWrapper: UIView!
    lazy var editProfileView: EditMenuView = {
        let v = EditMenuView()
        v.inBoardingViewControllerInstance = self
        return v
    }()
    
    private var colorsViewWrapper: UIView!
    lazy var colorsView: ColorsView = {
        let v = ColorsView()
        v.inBoardingViewControllerInstance = self
        return v
    }()
    
    private var categoriesViewWrapper: UIView!
    lazy var categoriesView: CategoriesView = {
        let v = CategoriesView()
        v.inBoardingViewControllerInstance = self
        return v
    }()
    
    var colView: UICollectionView!
    
    var allViews: [UIView] = []
    let cellId = "cellId"
    
    
    ///////////////////////////////////////////////////////////////
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: NSNotification.Name("updateUIInBoardingViewController"), object: nil)

        initCollectionView()
        initEditProfileView()
        initColorsView()
        initCategoriesView()
        
        //appDelegate.service.updateEveryUI()
        
        allViews = [editProfileViewWrapper, colorsViewWrapper, categoriesViewWrapper]
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    func initCollectionView() {
        
        if UIApplication.shared.keyWindow != nil {

            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0
            
            // ----------------------------------------------

            colView = UICollectionView(frame: CGRect(x: 0, y: appDelegate().service.getTopSafeAreaHeight(), width: self.view.frame.width, height: self.view.frame.height - closeViewHeight - appDelegate().service.getBottomSafeAreaHeight()), collectionViewLayout: layout)
            colView.delegate   = self
            colView.dataSource = self
            colView.backgroundColor = .white
            colView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
            colView.isPagingEnabled = true
            colView.isScrollEnabled = false
            colView.showsHorizontalScrollIndicator = false
            
            bottomButtonBar.frame = CGRect(x: 0, y: colView.frame.size.height, width: self.view.frame.width, height: closeViewHeight + appDelegate().service.getBottomSafeAreaHeight())
            
            
            let padding:CGFloat = 20
            
            previousButton.frame = CGRect(x: 0, y: (closeViewHeight/2) - 30, width: 60, height: 60)
            previousButton.center.x = colView.center.x
            previousButton.setImage(UIImage(named:"arrow_left_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
            previousButton.tintColor = .white
            previousButton.layer.masksToBounds = false
            previousButton.layer.cornerRadius = previousButton.frame.width / 2
            previousButton.layer.backgroundColor = UIColor.black.cgColor
            previousButton.titleEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
            previousButton.contentEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
            previousButton.addTarget(self, action: #selector(previousButtonPressed), for: .touchUpInside)
            previousButton.alpha = 0
            
            lightModeDarkModeButton.frame = CGRect(x: 0, y: (closeViewHeight/2) - 30, width: 60, height: 60)
            lightModeDarkModeButton.center.x = colView.center.x
            lightModeDarkModeButton.layer.masksToBounds = false
            lightModeDarkModeButton.layer.cornerRadius = lightModeDarkModeButton.frame.width / 2
            lightModeDarkModeButton.titleEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
            lightModeDarkModeButton.contentEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
            lightModeDarkModeButton.tintColor = .black
            lightModeDarkModeButton.alpha = 0
            lightModeDarkModeButton.addTarget(self, action: #selector(lightModeDarkModeButtonPressed), for: .touchUpInside)
            
            nextButton.frame = CGRect(x: 0, y: (closeViewHeight/2) - 30, width: 60, height: 60)
            nextButton.center.x = colView.center.x
            nextButton.setImage(UIImage(named:"arrow_right_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
            nextButton.tintColor = .white
            nextButton.layer.masksToBounds = false
            nextButton.layer.cornerRadius = nextButton.frame.width / 2
            nextButton.layer.backgroundColor = UIColor.black.cgColor
            nextButton.titleEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
            nextButton.contentEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
            nextButton.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
            
            //bottomButtonBar.addSubview(closeButton)
            bottomButtonBar.addSubview(previousButton)
            bottomButtonBar.addSubview(lightModeDarkModeButton)
            bottomButtonBar.addSubview(nextButton)
            
            //self.view.addSubview(mainViewWrapper)
            self.view.addSubview(colView)
            self.view.addSubview(bottomButtonBar)
            
            // Start app at menu index 1
            let selectedMenuIcon = NSIndexPath(item: currentIndex, section: 0)
            scrollToMenuIndex(menuIndex: selectedMenuIcon.item)
            colView.selectItem(at: selectedMenuIcon as IndexPath, animated: false, scrollPosition: [])
            
            updateUI()
        }
    }
    
    func initEditProfileView(){
        editProfileViewWrapper = UIView(frame: CGRect(x: 0, y: 0, width: colView.bounds.size.width, height: colView.bounds.size.height))
    
        editProfileView.frame.size.height = editProfileViewWrapper.frame.size.height
        editProfileView.frame.size.width = editProfileViewWrapper.frame.size.width
        
        editProfileViewWrapper.addSubview(editProfileView)
    }
    
    func initColorsView() {
        colorsViewWrapper = UIView(frame: CGRect(x: 0, y: 0, width: colView.bounds.size.width, height: colView.bounds.size.height))
        
        colorsView.frame.size.height = colorsViewWrapper.frame.size.height
        colorsView.frame.size.width = colorsViewWrapper.frame.size.width
        
        colorsViewWrapper.addSubview(colorsView)
    }
    
    func initCategoriesView() {
        categoriesViewWrapper = UIView(frame: CGRect(x: 0, y: 0, width: colView.bounds.size.width, height: colView.bounds.size.height))
        
        categoriesView.frame.size.height = categoriesViewWrapper.frame.size.height
        categoriesView.frame.size.width = categoriesViewWrapper.frame.size.width
        
        categoriesViewWrapper.addSubview(categoriesView)
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    UPDATE UI FOR LIGHT OR DARK MODE
    ///////////////////////////////////////////////////////////////
    @objc func updateUI(){
        if((appDelegate().loggedInUser?.userPreferences.lightMode)!){ // LIGHT MODE ON
            bottomButtonBar.backgroundColor = UIColor.themeBackgroudWhite()
            lightModeDarkModeButton.tintColor = .black
            lightModeDarkModeButton.setImage(UIImage(named:"lightMode_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        }else{ // DARK MODE ON
            bottomButtonBar.backgroundColor = UIColor.themeBackgroudBlack()
            lightModeDarkModeButton.tintColor = .white
            lightModeDarkModeButton.setImage(UIImage(named:"inBoarding_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        }
    }
    
    @objc func lightModeDarkModeButtonPressed() {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        NotificationCenter.default.post(name: NSNotification.Name("updateUIColorsView"), object: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.lightModeDarkModeButton.alpha = 0
        })
        
        if(colorsView.isLightMode){
            lightModeDarkModeButton.setImage(UIImage(named:"lightMode_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        }else{  // Dark mode on
            lightModeDarkModeButton.setImage(UIImage(named:"inBoarding_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.lightModeDarkModeButton.alpha = 1
        })
        
    }

    @objc func closeButtonPressed() {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        // Update user info
        let user = appDelegate().service.updateUser(user: userInProcess)

        // Update user photo
        let userPhoto = userInProcess.userProfile.photo
        if(userPhotoNeedsUpdate && (userPhoto != nil || userPhoto != "")){
            appDelegate().service.updateUserPhoto(userId: user.userProfile.userId!, photo: userInProcess.userProfile.photo!)
        }
        user.userProfile.photo = userPhoto
        
        // Set user items
        user.setMyItems(items: (appDelegate().loggedInUser?.myItems)!)
        
        // Updated user besomes loggedInUser
        appDelegate().setLoggedInUser(user: user)
        
        // Update feed
        NotificationCenter.default.post(name: NSNotification.Name("updateFeedItems"), object: nil)
        
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
        
        appDelegate().service.updateEveryUI()
    }
    
    @objc func previousButtonPressed() {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        if(currentIndex == 1){
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.lightModeDarkModeButton.alpha = 0
            })
            
            colorsView.saveProfileInfo()
        }
        if(currentIndex == 2){
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.lightModeDarkModeButton.alpha = 1
            })
            
            categoriesView.saveProfileInfo()
        }
        
        if(currentIndex - 1 >= 0){
            currentIndex -= 1
            scrollToMenuIndex(menuIndex: currentIndex)
        }
        
        // appDelegate.service.updateEveryUI()
    }
    
    @objc func nextButtonPressed() {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        if(currentIndex == 0){
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.lightModeDarkModeButton.alpha = 1
            })
            
            editProfileView.saveProfileInfo()
        }
        if(currentIndex == 1){
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.lightModeDarkModeButton.alpha = 0
            })
            
            colorsView.saveProfileInfo()
        }
        if(currentIndex == 2){
            categoriesView.saveProfileInfo()
            closeButtonPressed()
        }
        
        if(currentIndex + 1 < allViews.count){
            currentIndex += 1
            scrollToMenuIndex(menuIndex: currentIndex)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.previousButton.frame = CGRect(x: 40, y: (self.closeViewHeight/2) - 30, width: 60, height: 60)
                self.nextButton.frame = CGRect(x: self.view.frame.width - 100, y: (self.closeViewHeight/2) - 30, width: 60, height: 60)
            })
        }
        
        // appDelegate.service.updateEveryUI()
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    ALL ABOUT COLLECTION VIEW
    ///////////////////////////////////////////////////////////////
    var previousButtonVisible = false
    func scrollToMenuIndex(menuIndex: Int) {
        if(currentIndex != 0){
            if(!self.previousButtonVisible){
                self.previousButton.alpha = 0
                UIView.animate(withDuration: 0.2){
                    self.previousButton.alpha = 1
                    self.previousButtonVisible = true
                }
            }
        }else{
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.previousButton.frame = CGRect(x: 0, y: (self.closeViewHeight/2) - 30, width: 60, height: 60)
                self.previousButton.center.x = self.colView.center.x
                
                self.nextButton.frame = CGRect(x: 0, y: (self.closeViewHeight/2) - 30, width: 60, height: 60)
                self.nextButton.center.x = self.colView.center.x
            }) { finished in
                self.previousButton.alpha = 0
                self.previousButtonVisible = false
            }
        }
        
        let indexPath = IndexPath(item: menuIndex, section: 0)
        colView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        cell.addSubview(allViews[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: colView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

}

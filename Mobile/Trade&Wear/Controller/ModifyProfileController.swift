//
//  ModifyProfileController.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-02-18.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit
import AudioUnit

class ModifyProfileController: UIViewController {
    
    let mainViewWrapper = UIView()
    
    let closeViewHeight:CGFloat = 50
    let closeViewWrapper = UIView()
    let closeButton = UIButton()
    
    private var editProfileViewWrapper: UIView!
    lazy var editProfileView: EditMenuView = {
        let view = EditMenuView()
        return view
    }()
    
    
    ///////////////////////////////////////////////////////////////
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initCollectionView()
        initEditProfileView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func initCollectionView() {

        let padding:CGFloat = 20
        
        mainViewWrapper.frame = CGRect(x: 0, y: appDelegate().service.getTopSafeAreaHeight(), width: self.view.frame.width, height: self.view.frame.height - closeViewHeight - appDelegate().service.getBottomSafeAreaHeight())
        mainViewWrapper.backgroundColor = UIColor.isLightModeBackground()
        
        closeViewWrapper.frame = CGRect(x: 0, y: mainViewWrapper.frame.size.height, width: self.view.frame.width, height: closeViewHeight + appDelegate().service.getBottomSafeAreaHeight())
        closeViewWrapper.backgroundColor = .white
        
        closeButton.frame = CGRect(x: 0, y: closeViewHeight - closeViewHeight - 20, width: 60, height: 60)
        closeButton.setImage(UIImage(named:"arrow_right_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.transform = self.closeButton.transform.rotated(by: .pi / 2)
        closeButton.tintColor = .white
        closeButton.layer.backgroundColor = UIColor.black.cgColor
        closeButton.layer.masksToBounds = false
        closeButton.layer.cornerRadius = closeButton.frame.width / 2
        closeButton.center.x = closeViewWrapper.center.x
        closeButton.titleEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        closeButton.contentEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        
        closeViewWrapper.addSubview(closeButton)
        closeViewWrapper.backgroundColor = UIColor.isLightModeBackground()
        
        self.view.addSubview(mainViewWrapper)
        self.view.addSubview(closeViewWrapper)
    }
    
    func initEditProfileView(){
        editProfileViewWrapper = UIView(frame: CGRect(x: 0, y: 0, width: mainViewWrapper.bounds.size.width, height: mainViewWrapper.bounds.size.height))
        
        editProfileView.frame.size.height = editProfileViewWrapper.frame.size.height
        editProfileView.frame.size.width = editProfileViewWrapper.frame.size.width
        
        editProfileViewWrapper.addSubview(editProfileView)
        mainViewWrapper.addSubview(editProfileViewWrapper)
    }
    
    @objc func closeButtonPressed() {
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        editProfileView.saveProfileInfo()
        
        NotificationCenter.default.post(name: NSNotification.Name("updateLayoutProfileView"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name("updateUIProfileView"), object: nil)
        
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }

}

//
//  ViewController.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-01-20.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit
import AudioUnit

class MainViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var titleLabel: UILabel!
    var chatButton: UIButton!
    var settingsButton: UIButton!
    var searchButton: UIButton!
    
    //    POST VIEW
    lazy var postView: PostView = {
        let post = PostView()
        post.mainViewControllerInstance = self
        post.frame.size.height = self.view.bounds.size.height
        post.frame.size.width = self.view.bounds.size.width
        return post
    }()
    
    //    FEED VIEW
    lazy var feedView: FeedView = {
        let feed = FeedView()
        feed.mainViewControllerInstance = self
        feed.frame.size.height = self.view.bounds.size.height
        feed.frame.size.width = self.view.bounds.size.width
        return feed
    }()
    
    //    Profile VIEW
    lazy var profileView: ProfileView = {
        let profile = ProfileView()
        profile.mainViewControllerInstance = self
        profile.frame.size.height = self.view.bounds.size.height
        profile.frame.size.width = self.view.bounds.size.width
        return profile
    }()
    
    //    Bottom menu VIEW
    private var menuBarWrapper: UIView!
    lazy var menuBar: MenuBarView = {
        let menu = MenuBarView()
        menu.mainViewControllerInstance = self
        return menu
    }()
    
    var allViews: [UIView] = []
    let cellId = "cellId"
    
    var menuBarHeight: CGFloat = 60

    
    ///////////////////////////////////////////////////////////////
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Observers
        NotificationCenter.default.addObserver(self, selector: #selector(showInBoarding), name: NSNotification.Name("showInBoarding"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: NSNotification.Name("updateUIMainViewController"), object: nil)
        
        initCollectionView()
        setupBottomMenuBar()
        
        allViews = [postView,feedView,profileView]
        
        startAppOnFeedView()
        updateUI()
    }
    
    
    override func viewWillAppear(_ animated: Bool) { UIColor.colorForStatusBar() }

    
    ///////////////////////////////////////////////////////////////
    
    
    ///////////////////////////////////////////////////////////////
    //    This initiate the logic for Feed, Post and Profile
    ///////////////////////////////////////////////////////////////
    let window = UIApplication.shared.windows[0]
    var removeSafeAreaAndNavBarHeight: CGFloat = 0
    func initCollectionView() {

        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }
        // ----------------------------------------------
        
        collectionView?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        collectionView?.isPagingEnabled = true
        collectionView?.showsHorizontalScrollIndicator = false

        removeSafeAreaAndNavBarHeight = appDelegate().service.getTopSafeAreaHeight() + appDelegate().service.getBottomSafeAreaHeight() + (navigationController?.navigationBar.frame.height)!
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    MENU BAR VIEW
    ///////////////////////////////////////////////////////////////
    private func setupBottomMenuBar(){
        
        menuBarWrapper = UIView(frame: CGRect(x: 0, y: self.view.bounds.size.height - removeSafeAreaAndNavBarHeight - menuBarHeight, width: self.view.bounds.size.width, height: menuBarHeight))
        
        menuBar.frame.size.height = menuBarWrapper.frame.size.height
        menuBar.frame.size.width = menuBarWrapper.frame.size.width
        
        menuBarWrapper.addSubview(menuBar)
        
        view.addSubview(menuBarWrapper)
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    UPDATE TOP NAV BAR
    ///////////////////////////////////////////////////////////////
    func updateNavBar(indexPath: IndexPath){
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.colorForText()]
        
        titleLabel = UILabel()
        titleLabel.textColor = UIColor.colorForText()
        titleLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
        titleLabel.sizeToFit()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: titleLabel)
        

        if(indexPath.item == 0){
            
            self.titleLabel.text = "AFFICHER"
            
            self.navigationItem.rightBarButtonItem = nil
            
        }
        if(indexPath.item == 1){
            
            titleLabel.text = "PARCOURIR"
            
            searchButton = UIButton(type: .custom)
            searchButton.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 40)
            searchButton.setImage(UIImage(named:"search_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
            searchButton.tintColor = UIColor.colorForText()
            searchButton.contentEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
            searchButton.addTarget(self, action: #selector(self.showSearchView), for: UIControl.Event.touchUpInside)
            
            let searchButtonBarItem = UIBarButtonItem(customView: searchButton)
            let searchButtonWidth = searchButtonBarItem.customView?.widthAnchor.constraint(equalToConstant: 20)
            let searchButtonHeight = searchButtonBarItem.customView?.heightAnchor.constraint(equalToConstant: 40)
            searchButtonWidth?.isActive = true
            searchButtonHeight?.isActive = true
            
            self.navigationItem.rightBarButtonItem = searchButtonBarItem
        }
        if(indexPath.item == 2){
            
            titleLabel.text = "PROFILE"
            
            settingsButton = UIButton(type: .custom)
            settingsButton.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 40)
            settingsButton.setImage(UIImage(named:"settings_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
            settingsButton.tintColor = UIColor.colorForText()
            settingsButton.contentEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
            settingsButton.addTarget(self, action: #selector(self.showSettingsView), for: UIControl.Event.touchUpInside)
            
            let settingsButtonBarItem = UIBarButtonItem(customView: settingsButton)
            let settingsButtonWidth = settingsButtonBarItem.customView?.widthAnchor.constraint(equalToConstant: 20)
            let settingsButtonHeight = settingsButtonBarItem.customView?.heightAnchor.constraint(equalToConstant: 40)
            settingsButtonHeight?.isActive = true
            settingsButtonWidth?.isActive = true
            
            self.navigationItem.rightBarButtonItem = settingsButtonBarItem
        }
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    UPDATE UI FOR LIGHT OR DARK MODE
    ///////////////////////////////////////////////////////////////
    @objc func updateUI(){
        collectionView?.backgroundColor = UIColor.isLightModeBackground()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.colorForText()]
        navigationController?.navigationBar.barTintColor = UIColor.isLightModeItem()
        UINavigationBar.appearance().barTintColor = UIColor.isLightModeItem()
        titleLabel.textColor = UIColor.colorForText()
        UIColor.colorForStatusBar()
        if(settingsButton != nil){ settingsButton.tintColor = UIColor.colorForText() }
        if(searchButton != nil){ searchButton.tintColor = UIColor.colorForText() }
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    SHOW SETTINGS SLIDE UP MENU
    ///////////////////////////////////////////////////////////////
    let slideUpMenuController = SlideUpMenuController(
        allCells: [
            //CustomMenuCell(title: .preferences, image: "settings_icon"),
            CustomMenuCell(title: .modifierProfile, image: "profile_menu_icon"),
            CustomMenuCell(title: .inboarding, image: "inBoarding_icon")
        ]
    )
    @objc func showSettingsView(){
            
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        slideUpMenuController.mainViewControllerInstance = self
        slideUpMenuController.showMenu()
    }
    
    @objc func showSearchView(){
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    SHOW SETTINGS SLIDE UP MENU
    ///////////////////////////////////////////////////////////////
    func showPostView(category: itemType){
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)

        let postViewController = PostViewController()
        postViewController.mainViewControllerInstance = self
        postViewController.chosenItemType = category
        
        present(postViewController, animated: true)
    }
    
    ///////////////////////////////////////////////////////////////
    //    SHOW ITEM VIEW
    ///////////////////////////////////////////////////////////////
    func showItemView(item: ItemModel, isMine: Bool){
        
        let peek = SystemSoundID(1519)
        AudioServicesPlaySystemSound(peek)
        
        let navController = UINavigationController(rootViewController: ItemViewController(currentItem: item, isMine: isMine))
        self.present(navController, animated: true, completion: nil)
    
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    PRESENT INBOARDING
    ///////////////////////////////////////////////////////////////
    @objc func showInBoarding(){
        self.navigationController?.present(InBoardingViewController(), animated: true)
    }
    
    
    ///////////////////////////////////////////////////////////////
    //    ALL ABOUT COLLECTION VIEW
    ///////////////////////////////////////////////////////////////
    func startAppOnFeedView(){
        // Start app at menu index 1
        let indexPath = IndexPath(item: 1, section: 0)
        
        collectionView?.reloadData()
        collectionView?.layoutIfNeeded()
        
        scrollToMenuIndex(menuIndex: indexPath.item)
        menuBar.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
    }
    
    func scrollToMenuIndex(menuIndex: Int) {
        let indexPath = IndexPath(item: menuIndex, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        updateNavBar(indexPath: indexPath)
        
        if(menuIndex == 1){
            //NotificationCenter.default.post(name: NSNotification.Name("updateFeedItems"), object: nil)
        }
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = Int(targetContentOffset.pointee.x / view.frame.width)
        let indexPath = IndexPath(item: index, section: 0)
        menuBar.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        updateNavBar(indexPath: indexPath)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        cell.addSubview(allViews[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

}


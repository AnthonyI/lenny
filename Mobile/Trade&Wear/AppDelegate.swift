//
//  AppDelegate.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-01-20.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var statusBarBackgroundView: UIView?
    
    let service = Service()
    
    // For Auth0 ----------------------------------------------------
    let authServer = AuthorizationServer()
    
    // TODO: store this using the keystore
    var tokens: auth0Tokens? = nil
    
    var loggedInUser: UserModel? = nil
    var isLightModeCurrently: Bool? = true
    var currentChosenColor: String? = "White"

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return authServer.parseAuthorizeRedirectUrl(url: url)
    }
    
    func isLoggedIn() -> Bool {
        return UserDefaults.standard.bool(forKey: "isLoggedIn")
    }
    func setLoggedIn(userId: String){
        UserDefaults.standard.set(true, forKey: "isLoggedIn")
        UserDefaults.standard.set(userId, forKey: "userId")
        UserDefaults.standard.synchronize()
    }
    func setLoggedOut(){
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        UserDefaults.standard.set(nil, forKey: "userId")
        UserDefaults.standard.set(false, forKey: "demoMode")
        UserDefaults.standard.synchronize()
    }
    func setDemoMode(){
        UserDefaults.standard.set(true, forKey: "demoMode")
        UserDefaults.standard.synchronize()
    }
    func setLoggedInUser(user: UserModel){
        self.loggedInUser = user
        isLightModeCurrently = user.userPreferences.lightMode
    }
    func logout() {
        tokens = nil
        loggedInUser = nil
        authServer.reset()
        
        setLoggedOut()
    }
    // --------------------------------------------------------------

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        var alert : UIAlertController? = nil

        if(UserDefaults.standard.bool(forKey: "demoMode")){
            _ = service.demoMode()
            service.updateEveryUI()
        }else{
            if(UserDefaults.standard.object(forKey: "userId") != nil){
                
                let user = service.getOneUser(userId: UserDefaults.standard.object(forKey: "userId") as! String)
                
                if(user.userProfile.userId != nil) {
                    setLoggedInUser(user: user)
                    loggedInUser!.setMyItems(items: service.getMyItems(userId: (loggedInUser?.userProfile.userId)!))
                }else{
                    alert = UIAlertController(title: "ERREUR", message: "Problème de connection.", preferredStyle: .alert)
                    alert!.addAction(UIAlertAction(title: "Fermer", style: .cancel, handler: nil))
                    
                    logout()
                }
            }else{
                logout()
            }
        }
        
        FirebaseApp.configure()
        
        window = UIWindow()
        window?.makeKeyAndVisible()
        
        let layout = UICollectionViewFlowLayout()
        window?.rootViewController = UINavigationController(rootViewController: MainViewController(collectionViewLayout: layout))
        
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().backgroundColor = .clear
        
        statusBarBackgroundView = UIView()
        
        window?.addSubview((statusBarBackgroundView)!)
        window?.addConstraintsWithFormat("H:|[v0]|", views: (statusBarBackgroundView)!)
        
        let height = application.statusBarFrame.height
        window?.addConstraintsWithFormat("V:|[v0(\(height))]", views: (statusBarBackgroundView)!)
        
        
        DispatchQueue.main.async {
            self.service.updateEveryUI()
        }

        if(alert != nil) {
            window?.rootViewController!.present(alert!, animated: true)
        }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


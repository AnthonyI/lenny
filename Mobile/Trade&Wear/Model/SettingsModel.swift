//
//  SettingsModel.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-03-16.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import Foundation
import UIKit

enum SettingName: RawRepresentable {
    case modifierProfile
    case preferences
    case inboarding
    
    // (TITLE, CONTROLLER, IS FOR LOGGED IN USER ONLY)
    var rawValue: (String, UIViewController, Bool) {
        switch self {
        case .modifierProfile: return ("MODIFIER PROFILE", ModifyProfileController(), true)
        case .preferences: return ("PRÉFÉRENCES", SettingsViewController(), false)
        case .inboarding: return ("INBOARDING", InBoardingViewController(), true)
        }
    }
        
    init?(rawValue: (String, UIViewController, Bool)) {
        switch rawValue {
        case ("MODIFIER PROFILE", ModifyProfileController(), true): self = .modifierProfile
        case ("PRÉFÉRENCES", SettingsViewController(), false): self = .preferences
        case ("INBOARDING", InBoardingViewController(), true): self = .inboarding
        default: return nil
        }
    }
}

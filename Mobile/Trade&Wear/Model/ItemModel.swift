//
//  ItemModel.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-03-16.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit

enum itemStyle: String {
    
    case Tendance
    case Streetwear
    case Classy
    case Casual
    case Vintage
    case Sporty
    case Rock
    case Punk
    
    var rawValue: String {
        switch self {
        case .Tendance:     return "Tendance"
        case .Streetwear:   return "Streetwear"
        case .Classy:       return "Classy"
        case .Casual:       return "Casual"
        case .Vintage:      return "Vintage"
        case .Sporty:       return "Sporty"
        case .Rock:         return "Rock"
        case .Punk:         return "Punk"
        }
    }
    
    init?(rawValue: String) {
        switch rawValue {
        case "Tendance":    self = .Tendance
        case "Streetwear":  self = .Streetwear
        case "Classy":      self = .Classy
        case "Casual":      self = .Casual
        case "Vintage":     self = .Vintage
        case "Sporty":      self = .Sporty
        case "Rock":        self = .Rock
        case "Punk":        self = .Punk
            
        default: return nil
        }
    }
}

enum itemType: RawRepresentable {
    case Chandail
    case TShirt
    case Manteau
    case Pantalon
    case Shorts
    case SousVetements
    case Chaussures
    case Casquette
    case Tuque
    case Accessoire
    
    var rawValue: (String, [String]) {
        switch self {
        case .Chandail:     return ("Chandail", topSize)
        case .TShirt:       return ("T-Shirt", topSize)
        case .Manteau:      return ("Manteau", topSize)
        case .Pantalon:     return ("Pantalon", bottomSize)
        case .Shorts:       return ("Shorts", bottomSize)
        case .SousVetements:return ("Sous-vêtement", topSize)
        case .Chaussures:   return ("Chaussures", shoeSize)
        case .Casquette:    return ("Casquette", noSize)
        case .Tuque:        return ("Tuque", noSize)
        case .Accessoire:   return ("Accessoire", noSize)
        }
    }
    
    init?(rawValue: (String, [String])) {
        switch rawValue {
        case ("Chandail", topSize):     self = .Chandail
        case ("T-Shirt", topSize):      self = .TShirt
        case ("Manteau", topSize):      self = .Manteau
        case ("Pantalon", bottomSize):  self = .Pantalon
        case ("Shorts", bottomSize):    self = .Shorts
        case ("Sous-vêtement", topSize):self = .SousVetements
        case ("Chaussures", shoeSize):  self = .Chaussures
        case ("Casquette", noSize):     self = .Casquette
        case ("Tuque", noSize):         self = .Tuque
        case ("Accessoire", noSize):    self = .Accessoire
            
        default: return nil
        }
    }
    
    init?(title: String?) {
        switch title {
        case "Chandail":        self = .Chandail
        case "T-Shirt":         self = .TShirt
        case "Manteau":         self = .Manteau
        case "Pantalon":        self = .Pantalon
        case "Shorts":          self = .Shorts
        case "Sous-vêtement":   self = .SousVetements
        case "Chaussures":      self = .Chaussures
        case "Casquette":       self = .Casquette
        case "Tuque":           self = .Tuque
        case "Accessoire":      self = .Accessoire
            
        default: return nil
        }
    }
}

let allItemType: [itemType] = [.Chandail, .TShirt, .Manteau, .Pantalon, .Shorts, .SousVetements, .Chaussures, .Casquette, .Tuque, .Accessoire]

let allItemStyle: [itemStyle] = [.Tendance, .Streetwear, .Classy, .Casual, .Vintage, .Sporty, .Rock, .Punk]

let topSize: [String] =     ["","XS","S","M","L","XL","XXL","XXXL"]
let bottomSize: [String] =  ["","28","29","30","31","32","33","34"]
let shoeSize: [String] =    ["","7","7.5","8","8.5","9","9.5","10","10.5","11","11.5","12","12.5","13"]
let noSize: [String] =      ["", "Taille unique"]

struct ItemProfile {
    var itemId: String?
    var title: String?
    var price: String?
    var type: itemType?
    var style: itemStyle?
    var photos: [String:String]?
}
struct ItemCharacteristics{
    var size: String?
    var couleur: String?
}


///////////////////////////////////////////////////////////////
//    ITEM MODEL
///////////////////////////////////////////////////////////////
class ItemModel: NSObject {
    
    var itemProfile = ItemProfile()
    var itemCharacteristics = ItemCharacteristics()
    
    override init() {
        self.itemProfile.itemId = nil
        self.itemProfile.title = nil
        self.itemProfile.price = nil
        self.itemProfile.type = nil
        self.itemProfile.style = nil
        self.itemProfile.photos = nil
        
        self.itemCharacteristics.size = nil
        self.itemCharacteristics.couleur = nil
    }
    
    init(title: String, price: String, type: itemType, style: itemStyle, photos: [String:String], characteristics: ItemCharacteristics) {
        self.itemProfile.title = title
        self.itemProfile.price = price
        self.itemProfile.type = type
        self.itemProfile.style = style
        self.itemProfile.photos = photos
        
        self.itemCharacteristics = characteristics
    }
    
}

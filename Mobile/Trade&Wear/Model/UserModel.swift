//
//  UserModel.swift
//  Trade&Wear
//
//  Created by Kevin Boisvert on 2019-01-28.
//  Copyright © 2019 Trade&Wear Co. All rights reserved.
//

import UIKit

struct UserProfile {
    var userId: String?
    var name: String?
    var surname: String?
    var email: String?
    var birthDate: String?
    var photo: String?
}

struct UserPreferences {
    var lightMode: Bool?
    var color: String?
    var style: [String]?
}

class UserModel: NSObject {
    
    var userProfile = UserProfile()
    var userPreferences = UserPreferences()
    var myItems = [ItemModel]()

    override init() {
        self.userProfile.userId = nil
        self.userProfile.name = nil
        self.userProfile.surname = nil
        self.userProfile.email = nil
        self.userProfile.birthDate = nil
        self.userProfile.photo = nil
    }
    
    init(userId: String, name: String, surname: String, email: String, birthDate: String, photo: String) {
        self.userProfile.userId = userId
        self.userProfile.name = name
        self.userProfile.surname = surname
        self.userProfile.email = email
        self.userProfile.birthDate = birthDate
        self.userProfile.photo = photo
        
        self.userPreferences.lightMode = true
        self.userPreferences.color = ""
        self.userPreferences.style = []
        
    }
    
    func setUserPreferences(userPreferences: UserPreferences){
        self.userPreferences = userPreferences
    }
    
    func addToMyItems(item: ItemModel){
        self.myItems.append(item)
    }
    func setMyItems(items: [ItemModel]){
        self.myItems = items
    }
    func updateOneItem(item: ItemModel){
        self.myItems.filter {$0.itemProfile.itemId == item.itemProfile.itemId}.first?.itemProfile = item.itemProfile
    }
    func deleteOneItem(item: ItemModel){
        if let index = self.myItems.index(of: item) {
            self.myItems.remove(at: index)
        }
    }
    func getOneItem(itemId: String) -> ItemModel?{
        for item in self.myItems{
            if(item.itemProfile.itemId == itemId){
                return item
            }
        }
        
        return nil
    }
}

# Lenny - Vente en ligne de particuliers à particuliers


## Projet fin de bac 2019-2020

Projet réalisé par une équipe de 5 étudiants sur 2 sessions différentes.


- 1ère session (3 étudiants): Backend + Mobile
- 2ème session (3 étudiants initiaux + 2 nouveaux) : Web + amélioration Mobile et Backend

L'équipe était composée de cette façon :

- Etudiant 1 : Chef de Projet
- Etudiant 2 : Backend
- Etudiant 3 : Mobile
- Etudiant 4 : Backend TS
- Etudiant 5 : Web (moi)

Le code n'est pas le plus à jour car cette version est un backup fait quelques semaines avant la remise officielle. (Par exemple, côté FE : la page home n'existe pas ici...). En effet, le projet était hosté sur le git de l'université de Sherbrooke, qui par la suite a été supprimé.

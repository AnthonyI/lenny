import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  uri = 'http://localhost:8080';
  //uri = 'http://34.70.8.24:8080';
  //localStorage.setItem('uri', uri); Faire un storage temporaire pour aller feeder onBoarding.component.ts, checkIfExist
  constructor(private httpClient: HttpClient) { }
  response;



  /* Requetes User - Début */
  addUser(userData) {
    console.log("addUser(userData)" + userData);
    this.httpClient.post<any>(this.uri + '/users/add_user', userData).subscribe(res => {
      this.response = res;
    });
  }

  deleteUser(userId) {
    this.httpClient.delete(this.uri + '/users/delete_user/' + userId).subscribe(res => {
      this.response = res;
    });
  }

  updateUser(userData) {
    console.log("updateUser" + userData);
    this.httpClient.put<any>(this.uri + '/users/update_user', userData).subscribe(res => {
      this.response = res;
    });
  }

  getUser(userId: string) {
    return this.httpClient.get<any>(this.uri + '/users/get_user/' + userId).pipe();
  }
  /* Requetes User - Fin */




  /* Requetes Item - Début */
  addItem(itemData) {
    console.log("addItem(itemData)" + itemData);
    return this.httpClient.post<any>(this.uri + '/items/add_item', itemData).pipe();
  }

  deleteItem(itemId) {
    this.httpClient.delete(this.uri + '/items/delete_item/' + itemId).subscribe(res => {
      this.response = res;
    });
  }

  updateItem(itemData) {
    this.httpClient.put<any>(this.uri + '/items/update_item', itemData).subscribe(res => {
      this.response = res;
    });
  }
  /* Requetes Item - Fin */




  /* Requetes MyItems - Début */

  getMyItems(userId) {
    console.log('getMyItems(userId)  ' + userId);
    return this.httpClient.get<any>(this.uri + '/items/get_my_items/' + userId).pipe();
  }
  /* Requetes MyItems - Fin */

  /* Requetes Item - Début */

  getItem(itemId) {
    console.log('getItem(itemId)  ' + itemId);
    return this.httpClient.get<any>(this.uri + '/items/get_item/' + itemId).pipe();
  }
  /* Requetes Item - Fin */


  /* Requetes AllItems - Début */

  getAllItems() {
    console.log('getMyItems(userId) ');
    return this.httpClient.get<any>(this.uri + '/items/get_all_items/').pipe();
  }
  /* Requetes AllItems - Fin */




  /* Requetes Photo - Début */

  addPhoto(photoData) {
    console.log('addPhoto(photoData) ');
    this.httpClient.post<any>(this.uri + '/items/add_photo', photoData).subscribe(res => {
      this.response = res;
    });
  }

  getPhoto(fileName) {
    console.log('getPhoto(fileName)  ' + fileName);
    return this.httpClient.get(this.uri + '/items/get_photo/' + fileName,
      { responseType: 'text' });
  }

  deletePhoto(fileName) {
    this.httpClient.delete(this.uri + '/items/delete_photo/' + fileName).subscribe(res => {
      this.response = res;
    });
  }
  /* Requetes Photo - Fin */


}




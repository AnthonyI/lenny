export  interface Profile {
    birthDate: Date,
    createdDate: Date,
    email: String,
    inboardingDone: Boolean,
    methodConnection: String,
    name: String,
    profile: Object,
    sexe: String,
    surname: String,
    userId: String,
}
import { Component, OnInit } from '@angular/core';
import { faUser, faPowerOff } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  isCollapsed = true;
  faUser = faUser;
  faPowerOff = faPowerOff;

  constructor(public auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  goSell = function () {
    this.router.navigate(['/sell']);
  };

  goFeed = function () {
    this.router.navigate(['/feed']);
  };

  goOnBoarding = function () {
    this.router.navigate(['/onBoarding']);
  };

  goMyArticles = function () {
    this.router.navigate(['/myArticles']);
  };
}

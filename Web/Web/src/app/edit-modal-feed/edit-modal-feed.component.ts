import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { ToastrService } from 'ngx-toastr';
import { Item } from '../models/item.model';
import { AuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-edit-modal-feed',
  templateUrl: './edit-modal-feed.component.html',
  styleUrls: ['./edit-modal-feed.component.css']
})
export class EditModalFeedComponent implements OnInit {

  @Input() idItem: string;
  public Item: any = [];
  photos = new Array();


  @Input() userId: string;
  public Profile: any = [];

  constructor(
    private auth: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private apiService: ApiService,
    private toastr: ToastrService,
    private domSanitizer: DomSanitizer,
  ) { }
  ngOnInit() {
    const data = '{"characteristic":{"color": " ","size": " "}}';
    this.Item = JSON.parse(data);
  }

  ngOnChanges() {

    if (this.idItem !== '') {
      this.apiService.getItem(this.idItem).subscribe((data: any[]) => {
        this.Item = data;
        // console.log(this.Item);

        this.apiService.getUser(this.Item.userId).subscribe((profil: any[]) => {
          this.Profile = profil;
          // console.log('ngOnChanges() ' + profil);

          const countPhotos: number = this.Item.photos.length;
          // console.log('ngOnChanges() countPhotos = ' + countPhotos);
          this.photos = [];

          for (let y = 0; y < countPhotos; y++) {

            this.apiService.getPhoto(this.Item.photos[y]).subscribe((photo: any) => {

              this.photos.push(photo);
            });
          }
        });
      });
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditModalFeedComponent } from './edit-modal-feed.component';

describe('EditModalFeedComponent', () => {
  let component: EditModalFeedComponent;
  let fixture: ComponentFixture<EditModalFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditModalFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditModalFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

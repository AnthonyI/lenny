import { Component, OnInit, OnDestroy, Input, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { ToastrService } from 'ngx-toastr';
import { Item } from '../models/item.model';
import { AuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';
import { MyArticlesComponent } from '../pages/my-articles/my-articles.component';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.css']
})
export class EditModalComponent implements OnInit {


  @Input() idItem: string;
  putItem: FormGroup;
  public Item: any = [];
  photos = new Array();
  fileNames = new Array();
  photosToAdd = new Array(5);

  constructor(
    private auth: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private apiService: ApiService,
    private toastr: ToastrService,
    private domSanitizer: DomSanitizer,
    private MyArticlesComponent: MyArticlesComponent,
  ) {

    this.putItem = this.fb.group({
      '_id': '',
      'price': '',
      'style': '',
      'color': '',
      'size': '',
      //'picture'
    })
  }

  myItem: Item[];

  ngOnInit() {
    const data = '{"photos":"","characteristic":{"color": " ","size": " "}}';
    this.Item = JSON.parse(data);
  }

  ngOnChanges() {
    // console.log('handleClick() ' + this.idItem)
    if (this.idItem !== '') {
      this.apiService.getItem(this.idItem).subscribe((data: any[]) => {
        this.Item = data;
        console.log('ngOnChanges() ' + data);

        const countPhotos: number = this.Item.photos.length;
        console.log('ngOnChanges() countPhotos = ' + countPhotos);
        this.photos = [];
        this.fileNames = [];
        for (let y = 0; y < countPhotos; y++) {

          this.apiService.getPhoto(this.Item.photos[y]).subscribe((photo: any) => {
            this.fileNames.push(this.Item.photos[y]);
            this.photos.push(photo);
          });

          this.photosToAdd.splice(4 - y, 1);
          console.log('photosToAdd.length = ' + y + ' ; ' + this.photosToAdd.length);
        }
      });
    }
  }

  deleteMyArticle(idItem) {
    //console.log("deleteMyArticle(idItem)" + idItem);
    this.apiService.deleteItem(idItem);
    //location.reload();
    this.toastr.success('Votre article a bien été supprimé.');
  }

  deletePhoto(fileName: string) {
    console.log('deletePhoto(fileName) ' + fileName);
    this.apiService.deletePhoto(fileName);
    setTimeout(() => {
      this.ngOnChanges();
    }, 100);
  }

  put = function (putItem) {
    console.log("putItem" + this.idItem);

    let item = {
      '_id': '' + this.idItem + '',
      'price': '' + putItem.price + '',
      'style': '' + putItem.style + '',
      'characteristic': {
        'color': '' + putItem.color + '',
        'size': '' + putItem.size + '',
        'picturePath': '' + putItem.picturePath + '',
      },
    };

    /* if(item.price == ''){
      item.price = this.Item.price
      this.apiService.updateItem(item);
    }
    if(item.style == ''){
      item.style = this.Item.style
      this.apiService.updateItem(item);
    }
    if(item.color == ''){
      item.color = this.Item.characteristic.color
      this.apiService.updateItem(item);
    }
    if(item.size == ''){
      item.size = this.Item.characteristic.size
      this.apiService.updateItem(item);
    } */

    this.apiService.updateItem(item);
    //console.log("item"+item);

    //Afficher validation
    this.toastr.success('Votre article a bien été modifié.');
    //}
  };

  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    console.log('myReader.onloadend ' + inputValue);
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      const image = myReader.result;

      const photo = {
        '_id': '' + this.idItem + '',
        'photo': '' + image + '',
      };
      this.apiService.addPhoto(photo);

      setTimeout(() => {
        this.ngOnChanges();
      }, 100);

    };
    myReader.readAsDataURL(file);
  }

  refreshMyArticles() {
    console.log("this.MyArticlesComponent.ngOnInit();");
    this.MyArticlesComponent.ngOnInit();

  }

}

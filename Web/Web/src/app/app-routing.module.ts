import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { CallbackComponent } from './pages/callback/callback.component';
import { AuthGuard } from './auth/auth.guard';
import { ProfileComponent } from './pages/profile/profile.component';
import { OnboardingComponent } from './pages/onboarding/onboarding.component';
import { FeedComponent } from './pages/feed/feed.component';
import { SellComponent } from './pages/sell/sell.component';
import { AccessoriesComponent } from './pages/sell/accessories/accessories.component';
import { BonnetComponent } from './pages/sell/bonnet/bonnet.component';
import { CoatComponent } from './pages/sell/coat/coat.component';
import { PantComponent } from './pages/sell/pant/pant.component';
import { ShoesComponent } from './pages/sell/shoes/shoes.component';
import { ShortComponent } from './pages/sell/short/short.component';
import { SweaterComponent } from './pages/sell/sweater/sweater.component';
import { TshirtComponent } from './pages/sell/tshirt/tshirt.component';
import { MyArticlesComponent } from './pages/my-articles/my-articles.component';


const routes: Routes = [
  {
    path: 'callback',
    component: CallbackComponent
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full'
  },
  {
    path: 'onBoarding',
    component: OnboardingComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'myArticles',
    component: MyArticlesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'feed',
    component: FeedComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'sell',
    component: SellComponent,
    //canActivate: [AuthGuard]
  },
  {
    path: 'sell/accessories',
    component: AccessoriesComponent,
    //canActivate: [AuthGuard]
  },
  {
    path: 'sell/bonnet',
    component: BonnetComponent,
    //canActivate: [AuthGuard]
  },
  {
    path: 'sell/coat',
    component: CoatComponent,
    //canActivate: [AuthGuard]
  },
  {
    path: 'sell/pant',
    component: PantComponent,
    //canActivate: [AuthGuard]
  },
  {
    path: 'sell/shoes',
    component: ShoesComponent,
    //canActivate: [AuthGuard]
  },
  {
    path: 'sell/short',
    component: ShortComponent,
    //canActivate: [AuthGuard]
  },
  {
    path: 'sell/sweater',
    component: SweaterComponent,
    //canActivate: [AuthGuard]
  },
  {
    path: 'sell/tshirt',
    component: TshirtComponent,
    //canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

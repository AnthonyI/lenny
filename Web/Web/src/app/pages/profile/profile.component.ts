import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';
import { ApiService } from '../../api.service'
import { FormGroup, FormBuilder, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Profile } from '../../models/user.model';
import { ToastrService } from 'ngx-toastr';
import { getDefaultService } from 'selenium-webdriver/chrome';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  //profileJson: string = null;
  putUser: FormGroup;
  userAuth0;
  userId = localStorage.getItem('userId');

  constructor(private auth: AuthService, private router: Router, private fb: FormBuilder, private apiService: ApiService, private toastr: ToastrService) {

    this.userAuth0 = this.auth.userProfile$;
    localStorage.setItem('userId', this.userAuth0.source._value.sub);

    this.putUser = this.fb.group({
      'userId': this.userId,
      'name': '',
      'surname': '',
      'email': '',
      'birthDate': '', /* 12/03/2019 */
      'tendance': false,
      'classy': false,
      'vintage': false,
      'rock': false,
      'streetwear': false,
      'casual': false,
      'sporty': false,
      'punk': false,
    })
  }


  myProfile: Profile[];

  ngOnInit() {
    const data = '{"name": "","surname": "","birthDate": "","profile":{"style": ""}}';
    this.myProfile = JSON.parse(data);
    this.apiService.getUser(this.userId).subscribe((data: Profile[]) => {
      this.myProfile = data;
      console.log("ngOnInit()" + data);
    });

  }

  put = function (putUser) {
    console.log("letuser" + putUser);

    // Mise au format de birthDate
    console.log("function (putUser){ " + putUser.birthDate);

    let tempBirthDate = ''

    if (putUser.birthDate === undefined) {
      tempBirthDate = this.myProfile.birthDate.substring(0, 10);
    } else {
      tempBirthDate = putUser.birthDate.month + '/' + putUser.birthDate.day + '/' + putUser.birthDate.year;      
    }

    putUser.birthDate = tempBirthDate;

    let profile = {
      "userId": '' + putUser.userId + '',
      "name": '' + putUser.name + '',
      "surname": '' + putUser.surname + '',
      "email": "" + putUser.email + "",
      "birthDate": '' + tempBirthDate + '', /* 12/03/2019 */
      "profile": {
        "style": [putUser.tendance,
        putUser.classy,
        putUser.vintage,
        putUser.rock,
        putUser.streetwear,
        putUser.casual,
        putUser.sporty,
        putUser.punk],
        //"darkmode": "true"
      }
    }



    if (profile.name == '') {
      profile.name = this.myProfile.name
      this.apiService.updateUser(profile);
    }
    if (profile.surname == '') {
      profile.surname = this.myProfile.surname;
      this.apiService.updateUser(profile);
    }
    if (profile.email === '') {
      profile.email = this.myProfile.email
      this.apiService.updateUser(profile);
    }
    if (profile.birthDate == '') {
      profile.birthDate = this.myProfile.birthDate
      this.apiService.updateUser(profile);
    }


    //Afficher validation
    this.toastr.success('Votre profil a bien été modifié.');
  }

  night_mode = function () {

    const input = (document.getElementById("checkbox")) as HTMLInputElement;
    if (input.checked) {
      document.body.style.setProperty('--main-bg', "#212529");
      document.body.style.setProperty('--main-color', "#fff");
    }
    else {
      document.body.style.setProperty('--main-bg', "#fff");
      document.body.style.setProperty('--main-color', "#555");
    }
  }

}

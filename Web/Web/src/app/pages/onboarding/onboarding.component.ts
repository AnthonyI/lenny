import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApiService } from '../../api.service';
import { AuthService } from 'src/app/auth/auth.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.css']
})
export class OnboardingComponent implements OnInit {

  postUser: FormGroup;
  userAuth0;

  constructor(private router: Router,
    private fb: FormBuilder,
    private apiService: ApiService,
    private auth: AuthService,
    private toastr: ToastrService) {

    this.userAuth0 = this.auth.userProfile$;
    localStorage.setItem('userId', this.userAuth0.source._value.sub);

    this.postUser = this.fb.group({
      'userId': this.userAuth0.source._value.sub,
      'name': '',
      'surname': '',
      'email': '',
      'birthDate': '',
      //'darkmode' : false,
      'methodConnection': '',
      'inboardingDone': true,
      'tendance': false,
      'classy': false,
      'vintage': false,
      'rock': false,
      'streetwear': false,
      'casual': false,
      'sporty': false,
      'punk': false,
    })
  }

ngOnInit() {
  this.userAuth0 = this.auth.userProfile$;
  this.checkIfExist();
}

checkIfExist() {
  this.apiService.getUser(this.userAuth0.source._value.sub).subscribe((data: object) => {
    if (data === null) {
      this.router.navigate(['/onBoarding']);
    } else {
      this.router.navigate(['/feed']);
    }
  });
}

post = function (postUser) {
  console.log(postUser);

  //Mise au format de birthDate
  let tempBirthDate = postUser.birthDate.month + '/' + postUser.birthDate.day + '/' + postUser.birthDate.year;
  postUser.birthDate = tempBirthDate;

  /* let tempUserId = postUser.userId;
  tempUserId = tempUserId.split('|', 2); */

  let onBoarding = {
    "userId": '' + postUser.userId + '',
    /* "userId": '' + tempUserId[1] + '', */
    "name": '' + postUser.name + '',
    "surname": '' + postUser.surname + '',
    "email": "" + postUser.email + "",
    "inboardingDone": true,
    "birthDate": "" + tempBirthDate + "",
    "profile": {
      "style": [postUser.tendance,
      postUser.classy,
      postUser.vintage,
      postUser.rock,
      postUser.streetwear,
      postUser.casual,
      postUser.sporty,
      postUser.punk],
      //"darkmode": postUser.darkmode
    }
  }

  //methodConnection
  let tempMethodConnection = this.auth.userProfile$.source._value.sub;
  postUser.methodConnection = tempMethodConnection.substring(0, 13);

  if (postUser.name == '' || postUser.surname == '' || postUser.email == '' || postUser.birthDate == '') {
    //Afficher erreur
    this.toastr.error('Veuillez compléter tous les champs obligatoires.');
  }
  else {
    //Requete post http
    this.apiService.addUser(onBoarding);

    //Navigation
    this.router.navigate(['/feed']);

    //Afficher validation
    this.toastr.success('Votre profil a bien été créé.');
  }
}

/* night_mode = function () {

  const input = (document.getElementById("checkbox")) as HTMLInputElement;
  if (input.checked) {
    document.body.style.setProperty('--main-bg', "#3b3b3b");
    document.body.style.setProperty('--main-color', "#fff");
  }
  else {
    document.body.style.setProperty('--main-bg', "#fff");
    document.body.style.setProperty('--main-color', "#555");
  } 
}*/



}



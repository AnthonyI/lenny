import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Item } from '../../models/item.model';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-my-articles',
  templateUrl: './my-articles.component.html',
  styleUrls: ['./my-articles.component.css']
})
export class MyArticlesComponent implements OnInit {

  public idItem: any = '';
  photos = new Array();
  userId = localStorage.getItem('userId');
  myItems: Item[];

  constructor(private apiService: ApiService,
    private domSanitizer: DomSanitizer,
    ) { }

  ngOnInit() {
    console.log('ngOnInit()' + this.userId);
    this.apiService.getMyItems(this.userId).subscribe((data: Item[]) => {
      this.myItems = data;    
      
      const countItems: number = this.myItems.length;
      //console.log('ngOnChanges() countPhotos = ' + countPhotos);
      this.photos = [];      
      for (let y = 0; y < countItems; y++) {

        this.apiService.getPhoto(this.myItems[y].photos[0]).subscribe((photo: any) => {          
          this.myItems[y].photos[0] = photo;
        });
        
      }
      
    });
  }

  handleClick(data: number): any {
    this.idItem = this.myItems[data]._id;    
  }

}

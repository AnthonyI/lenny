import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sell',
  templateUrl: './sell.component.html',
  styleUrls: ['./sell.component.css']
})
export class SellComponent implements OnInit {

  constructor(public auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  sellSweater = function () {
    this.router.navigate(['/sell/sweater']);
  };

  sellTshirt = function () {
    this.router.navigate(['/sell/tshirt']);
  };

  sellCoat = function () {
    this.router.navigate(['/sell/coat']);
  };

  sellPant = function () {
    this.router.navigate(['/sell/pant']);
  };

  sellShort = function () {
    this.router.navigate(['/sell/short']);
  };

  sellShoes = function () {
    this.router.navigate(['/sell/shoes']);
  };

  sellBonnet = function () {
    this.router.navigate(['/sell/bonnet']);
  };

  sellAccessories = function () {
    this.router.navigate(['/sell/accessories']);
  };

}

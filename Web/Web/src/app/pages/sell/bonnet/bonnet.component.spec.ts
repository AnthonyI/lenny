import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BonnetComponent } from './bonnet.component';

describe('BonnetComponent', () => {
  let component: BonnetComponent;
  let fixture: ComponentFixture<BonnetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BonnetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BonnetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

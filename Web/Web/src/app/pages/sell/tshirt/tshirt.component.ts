import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApiService } from '../../../api.service'
import { AuthService } from 'src/app/auth/auth.service';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-tshirt',
  templateUrl: './tshirt.component.html',
  styleUrls: ['./tshirt.component.css']
})
export class TshirtComponent implements OnInit {

  postItem: FormGroup;
  image: any;
  photos = new Array();

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private auth: AuthService,
    private domSanitizer: DomSanitizer,
  ) {

    this.postItem = this.fb.group({
      'userId': localStorage.getItem('userId'),
      'title': '',
      'price': '',
      'style': '',
      'type': 'tshirt',
      'color': '',
      'size': '',
      'picturePath': '',
    });
  }

  ngOnInit() {
  }

  post = function (postItem) {
    console.log(postItem);

    let item = {
      'userId': '' + postItem.userId + '',
      'title': '' + postItem.title + '',
      'price': '' + postItem.price + '',
      'style': '' + postItem.style + '',
      'type': 'tshirt',
      'characteristic': {
        'color': '' + postItem.color + '',
        'size': '' + postItem.size + '',
        'picturePath': '' + postItem.picturePath + '',
      },
    };

    if (postItem.title === '' || postItem.price === '' || postItem.style === '' || postItem.color === '' || postItem.size === '') {
      // Afficher erreur
      this.toastr.error('Veuillez compléter tous les champs obligatoires.');
    } else {
      // Requete post http + navigation
      console.log(postItem.picturePath);
      this.apiService.addItem(item).subscribe((data: any) => {
        console.log('picturePath ' + postItem.picturePath);

        console.log('user._id ' + data._id);
        console.log('photos qty ' + this.photos.length);

        const photosQty: number = this.photos.length;

        for (let i = 0; i < photosQty; i++) {
          const time: number = 200 * i;
          setTimeout(() => {
            const photo = {
              '_id': '' + data._id + '',
              'photo': '' + this.photos[i] + '',
            };
            this.apiService.addPhoto(photo);
          }, time);
        }


      });
      // Afficher validation
      this.toastr.success('Votre article a bien été créé.');

      this.router.navigate(['/feed']);
    }
  };

  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.image = myReader.result;
      // console.log('myReader.onloadend ' + this.image);
      this.photos.push(this.image);
    };
    myReader.readAsDataURL(file);
  }

  deletePhoto(photoNumber: number) {
    this.photos.splice(photoNumber, 1);
  }

}

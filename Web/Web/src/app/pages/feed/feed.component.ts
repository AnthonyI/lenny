import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service'
import { Item } from '../../models/item.model';
import { Profile } from '../../models/user.model';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {

  public idItem: any = '';
  photos = new Array();
  userId = localStorage.getItem('userId');
  /* public userId: any = ''; */

  //userId = localStorage.getItem('userId');
  allItems: Item[];
  /* profileItem: Profile[]; */

  constructor(
    private apiService: ApiService,
    private domSanitizer: DomSanitizer,
    ) { }

  ngOnInit() {
    this.apiService.getAllItems().subscribe((data: Item[]) => {
      this.allItems = data;
      
      const countItems: number = this.allItems.length;
      //console.log('ngOnChanges() countPhotos = ' + countPhotos);
      this.photos = [];
      for (let y = 0; y < countItems; y++) {

        this.apiService.getPhoto(this.allItems[y].photos[0]).subscribe((photo: any) => {          
          this.allItems[y].photos[0] = photo;
        });
        
      }

    });

    /* this.getUser(); */
  }

  handleClick(data: number): any {
    this.idItem = this.allItems[data]._id;    
    /* this.userId = this.profileItem[data].userId;  
    console.log("handleClick" + this.userId); */
  }

  

  /* getUser(){
    this.apiService.getUser(this.userId).subscribe((data: Profile[]) => {
      this.profileItem = data;
      console.log("getUser(userId)" + this.userId);
      console.log("getUser(data)" + data);
      console.log("getUser(this.profileItem)" + this.profileItem);
    });
  } */
 
}


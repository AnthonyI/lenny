import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HighlightModule } from 'ngx-highlightjs';
import json from 'highlight.js/lib/languages/json';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CallbackComponent } from './pages/callback/callback.component';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeroComponent } from './components/hero/hero.component';
import { HomeContentComponent } from './components/home-content/home-content.component';
import { LoadingComponent } from './components/loading/loading.component';
import { OnboardingComponent } from './pages//onboarding/onboarding.component';
import { FeedComponent } from './pages/feed/feed.component';
import { SellComponent } from './pages/sell/sell.component';
import { ApiService } from './api.service';
import { PantComponent } from './pages/sell/pant/pant.component';
import { SweaterComponent } from './pages/sell/sweater/sweater.component';
import { AccessoriesComponent } from './pages/sell/accessories/accessories.component';
import { BonnetComponent } from './pages/sell/bonnet/bonnet.component';
import { ShoesComponent } from './pages/sell/shoes/shoes.component';
import { TshirtComponent } from './pages/sell/tshirt/tshirt.component';
import { ShortComponent } from './pages/sell/short/short.component';
import { CoatComponent } from './pages/sell/coat/coat.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyArticlesComponent } from './pages/my-articles/my-articles.component';
import { EditModalComponent } from './edit-modal/edit-modal.component';
import { ToastrModule } from 'ngx-toastr';
import { EditModalFeedComponent } from './edit-modal-feed/edit-modal-feed.component';

export function hljsLanguages() {
  return [{ name: 'json', func: json }];
}

@NgModule({
  declarations: [
    AppComponent,
    CallbackComponent,
    HomeComponent,
    ProfileComponent,
    NavBarComponent,
    FooterComponent,
    HeroComponent,
    HomeContentComponent,
    LoadingComponent,
    OnboardingComponent,
    FeedComponent,
    SellComponent,
    PantComponent,
    SweaterComponent,
    AccessoriesComponent,
    BonnetComponent,
    ShoesComponent,
    TshirtComponent,
    ShortComponent,
    CoatComponent,
    MyArticlesComponent,
    EditModalComponent,
    EditModalFeedComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    NgbModule,
    HighlightModule.forRoot({
      languages: hljsLanguages
    }),
    FontAwesomeModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
